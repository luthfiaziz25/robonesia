<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBerita extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_berita');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$berita = $this->M_berita->getBerita();
		$this->template->load('admin/static','admin/adminBerita',compact('berita'));
	}
	public function addBerita()
	{
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$penulis = $this->input->post('penulis');
		$image = $this->input->post('image');

		if ($image=''){} else{
			$config['upload_path']='./assets/img';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;


			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Berita Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}

			$data =  array(
				'judul'=> $judul,
				'isi'	  => $isi,
				'penulis'     => $penulis,
				'image'		  => $image
			);
			$this->M_berita->insertBerita($data);
			redirect('adminBerita');
		}
	}
	public function hapusBerita($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('adminBerita');
		}else{
			$this->M_berita->hapusBerita($data,$id);
			redirect('adminBerita');
		}
	}

	public function editBerita()
     {
        $this->form_validation->set_rules('id', 'id', 'required');
        $this->form_validation->set_rules('judul', 'judul', 'required');

		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$penulis = $this->input->post('penulis');
		$image = $this->input->post('image');

        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('AdminBerita'); 
        }else{

	        
            $data=array(
                "judul"=>$judul,
                "isi"=>$isi,
                "penulis"=>$penulis,
            );

        	if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;


				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Menambahkan Berita Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['image'] = $image;
				}
	        }

            $this->M_berita->editBerita($data,$id);
            redirect('AdminBerita');
        }
    } 

}
