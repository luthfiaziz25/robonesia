<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminDownload extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_download');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$download = $this->M_download->getDownload();
		$this->template->load('admin/static','admin/adminDownload', compact('download'));
	}
	public function addDownload()
	{
		$namafile = $this->input->post('namafile');
		$file = $this->input->post('file');

		if ($file=''){} else{
			$config['upload_path']='./assets/document';
			$config['upload_url']  = base_url()."/assets/document";
			$config['allowed_types']='pdf|doc|docx';
			$config['encrypt_name'] = true;	

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('file')){
				echo "Menambahkan File Download Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}
			$data =  array(
				'namafile'	=> $namafile,
				'file'	=> $file
			);
			$this->M_download->insertGaleriFoto($data);
			redirect('adminDownload');
		}
	}
	public function hapusDownload($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"File Gagal Di Hapus");
			redirect('adminDownload');
		}else{
			$this->M_download->hapusDownload($data,$id);
			redirect('adminDownload');
		}
	}
	public function editDownload()
     {
        $this->form_validation->set_rules('id_file', 'id_file', 'required');
        $this->form_validation->set_rules('namafile', 'namafile', 'required');
        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('AdminDownload'); 
        }else{
            $data=array(
                "namafile"=>$_POST['namafile'],
            );
            $this->M_download->editDownload($data,$id);
            redirect('AdminDownload');
        }
    } 

}
