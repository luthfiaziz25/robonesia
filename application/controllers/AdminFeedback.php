<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminFeedback extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_feedback');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$feedback = $this->M_feedback->getfeedback();
		$this->template->load('admin/static','admin/adminFeedback', compact('feedback'));
	}
	public function hapusFeedback($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('adminFeedback');
		}else{
			$this->M_feedback->hapusFeedback($data,$id);
			redirect('adminFeedback');
		}
	}
}
