<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminGaleriFoto extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_galerifoto');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$galerifoto = $this->M_galerifoto->getGalerifoto();
		$this->template->load('admin/static','admin/adminGaleriFoto', compact('galerifoto'));
	}
	public function addGalerifoto()
	{
		$image = $this->input->post('filename');
		$title = $this->input->post('title');
		$deskripsi = $this->input->post('deskripsi');

		if ($image=''){} else{
			$config['upload_path']='./assets/img';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;	
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Foto Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
			}
			$data =  array(
				'filename'		  => $image,
				'title'	=>$title,
				'deskripsi'	  => $deskripsi
			);
			$this->M_galerifoto->insertGaleriFoto($data);
			redirect('adminGaleriFoto');
		}
	}

	public function hapusGalerifoto($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Galeri Foto Gagal Di Hapus");
			redirect('adminGaleriFoto');
		}else{
			$this->M_galerifoto->hapusGalerifoto($data,$id);
			redirect('adminGaleriFoto');
		}
	}

	public function editGalerifoto()
     {
        $this->form_validation->set_rules('id', 'id', 'required');
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');

        $deskripsi = $this->input->post('deskripsi');
        $title = $this->input->post('title');
        $image = $this->input->post('filename');

        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('AdminGaleriFoto'); 
        }else{

	        
            $data=array(
                "deskripsi"=>$deskripsi,
                "title"=>$title,
   
            );

        	if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Menambahkan Foto Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['filename'] = $image;
				}
	        }
            $this->M_galerifoto->editGalerifoto($data,$id);
            redirect('AdminGaleriFoto');
        }
    } 
}
