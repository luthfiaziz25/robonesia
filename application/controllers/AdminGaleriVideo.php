<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminGaleriVideo extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_galerivideo');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$galerivideo = $this->M_galerivideo->getGalerivideo();
		$this->template->load('admin/static','admin/adminGaleriVideo', compact('galerivideo'));
	}
	public function addGalerivideo(){

		$judul = $this->input->post('judul');
		$link= $this->input->post('link');
		$data =  array(
			'link'=> $link,
			'judul'=>$judul
		);
		$this->M_galerivideo->insertGaleriVideo($data);
		redirect('adminGaleriVideo');
	}

	public function hapusGalerivideo($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Video Gagal Di Hapus");
			redirect('adminGaleriVideo');
		}else{
			$this->M_galerivideo->hapusGalerivideo($data,$id);
			redirect('adminGaleriVideo');
		}
	}

	public function editGalerivideo()
     {
        $this->form_validation->set_rules('id_video', 'id_video', 'required');
        $this->form_validation->set_rules('judul', 'judul', 'required');
        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('AdminGaleriVideo'); 
        }else{
            $data=array(
                "judul"=>$_POST['judul'],
                "link"=>$_POST['link']
            );
            $this->M_galerivideo->editGalerivideo($data,$id);
            redirect('AdminGaleriVideo');
        }
    } 
}
