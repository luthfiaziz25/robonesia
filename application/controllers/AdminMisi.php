<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMisi extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_misi');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('index.php/loginadmin/login'));
		}
	}
	public function index()
	{
		$misi = $this->M_misi->getMisi();
		$this->template->load('admin/static','admin/adminMisi', compact('misi'));
	}
	public function addMisi(){

		$misi = $this->input->post('misi');
		
		$data =  array(
			'misi'=> $misi
		);
		$this->M_misi->insertMisi($data);
		redirect('adminMisi');
	}

	public function hapusMisi($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Misi Gagal Di Hapus");
			redirect('adminMisi');
		}else{
			$this->M_misi->hapusMisi($data,$id);
			redirect('adminMisi');
		}
	}

	public function editMisi()
     {
        $this->form_validation->set_rules('id_misi', 'id_misi', 'required');
        $this->form_validation->set_rules('misi', 'misi', 'required');
        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('adminMisi'); 
        }else{
            $data=array(
                "misi"=>$_POST['misi'],
           
            );
            $this->M_misi->editmisi($data,$id);
            redirect('adminMisi');
        }
    } 
}