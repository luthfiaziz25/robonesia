<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMustahik extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_mustahik');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
		if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$mustahik = $this->M_mustahik->getMustahik();
		$this->template->load('admin/static','admin/adminMustahik',compact('mustahik'));
	}
	public function insertMustahik($data){

		$this->db->insert('tabel_mustahik',$data);
		$this->session->set_flashdata('sukses',"Galeri Foto Ditambahkan");
		return TRUE;
	}
	public function addMustahik()
	{
		$image = $this->input->post('image');
		$nik_mustahik = $this->input->post('nik_mustahik');
		$nama_mustahik = $this->input->post('nama_mustahik');
		$jk_mustahik = $this->input->post('jk_mustahik');
		$alamat_mustahik = $this->input->post('alamat_mustahik');
		
		if ($image=''){} else{
			$config['upload_path']='./assets/img';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;


			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Mustahik Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}

			$data =  array(
				'image'=>	$image,
				'nik_mustahik'=> $nik_mustahik,
				'nama_mustahik'=> $nama_mustahik,
				'jk_mustahik' => $jk_mustahik,
				'alamat_mustahik' => $alamat_mustahik

			);
			$this->M_mustahik->insertMustahik($data);
			redirect('adminMustahik');
		}
	}
	public function hapusMustahik($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('adminMustahik');
		}else{
			$this->M_mustahik->hapusMustahik($data,$id);
			redirect('adminMustahik');
		}
	}
	public function editMustahik()
	{
		$this->form_validation->set_rules('id_mustahik', 'id_mustahik', 'required');
		$this->form_validation->set_rules('nama_mustahik', 'nama_mustahik', 'required');

		$image = $this->input->post('image');
		$nik_mustahik = $this->input->post('nik_mustahik');
		$nama_mustahik = $this->input->post('nama_mustahik');
		$jk_mustahik = $this->input->post('jk_mustahik');
		$alamat_mustahik = $this->input->post('alamat_mustahik');


		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('AdminMustahik'); 
		}else{
			$data=array(
				"nik_mustahik"=>$_POST['nik_mustahik'],
				"nama_mustahik"=>$_POST['nama_mustahik'],
				"jk_mustahik"=>$_POST['jk_mustahik'],
				"alamat_mustahik"=>$_POST['alamat']
			);

			if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Menambahkan Foto Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['image'] = $image;
				}

			}
			$this->M_mustahik->editMustahik($data,$id_mustahik);
				redirect('AdminMustahik');
		} 
	}
}