<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMuzakki extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_muzakki');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$muzakki = $this->M_muzakki->getMuzakki();
		$this->template->load('admin/static','admin/adminMuzakki',compact('muzakki'));
	}
	public function insertMuzakki($data){

    $this->db->insert('tabel_muzakki',$data);
    $this->session->set_flashdata('sukses',"Galeri Foto Ditambahkan");
    return TRUE;
  	}
  	public function addMuzakki()
	{
		$image = $this->input->post('image');
		$nama = $this->input->post('nama');
		$nik = $this->input->post('nik');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$status = $this->input->post('status');
		$pekerjaan = $this->input->post('pekerjaan');
		$no_hp = $this->input->post('no_hp');
		$alamat = $this->input->post('alamat');
		
		if ($image=''){} else{
			$config['upload_path']='./assets/img';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;


			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Muzakki Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}

			$data =  array(
				'image'=>	$image,
				'nama'=>	$nama,
				'nik'=>		$nik,
				'jenis_kelamin'=>	$jenis_kelamin,
				'status'=>	$status,
				'pekerjaan'=>	$pekerjaan,
				'no_hp'=>	$no_hp,
				'alamat'=>	$alamat
			);
			$this->M_muzakki->insertMuzakki($data);
			redirect('adminMuzakki');
		}
	}
	public function hapusMuzakki($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('adminMuzakki');
		}else{
			$this->M_muzakki->hapusMuzakki($data,$id);
			redirect('adminMuzakki');
		}
	}
	public function editMuzakki()
     {
        $this->form_validation->set_rules('id_muzakki', 'id_muzakki', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('AdminMuzakki'); 
        }else{
            $data=array(
                "nama"=>$_POST['nama'],
                "nik"=>$_POST['nik'],
                "jenis_kelamin"=>$_POST['jenis_kelamin'],
                "status"=>$_POST['status'],
                "pekerjaan"=>$_POST['pekerjaan'],
                "jenis_kelamin"=>$_POST['jenis_kelamin'],
                "no_hp"=>$_POST['no_hp'],
                "alamat"=>$_POST['alamat']
            );
            $this->M_muzakki->editMuzakki($data,$id_muzakki);
            redirect('AdminMuzakki');
        }
    } 
}
