<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminProker extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_proker');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
		if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{	
		$proker= $this->M_proker->getProker();
		$this->template->load('admin/static','admin/adminProker',compact('proker'));
	}
	public function addProker()
	{
		$namaproker = $this->input->post('namaproker');
		$image = $this->input->post('image');
		$waktu = $this->input->post('waktu');
		$desk_proker = $this->input->post('desk_proker');

		if ($image=''){} else{
			$config['upload_path']='./assets/img';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;	
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Foto Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}
			$data =  array(
				'namaproker'	=> $namaproker,
				'image'		  => $image,
				'waktu'	=> $waktu,
				'desk_proker'	  => $desk_proker
			);
			$this->M_proker->insertProker($data);
			redirect('adminProker');
		}
	}
	public function hapusProker($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Program Kerja Gagal Di Hapus");
			redirect('adminProker');
		}else{
			$this->M_proker->hapusProker($id);
			redirect('adminProker');
		}
	}
	public function editProker()
	{
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('namaproker', 'namaproker', 'required');
		
		$namaproker = $this->input->post('namaproker');
		$waktu = $this->input->post('waktu');
		$desk_proker = $this->input->post('desk_proker');
		$image = $this->input->post('image');

		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('AdminProker'); 
		}else{


			$data=array(
				"namaproker"=>$namaproker,
				"waktu"=>$waktu,
				"desk_proker"=>$desk_proker

			);

			if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;


				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Update Program Kerja Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['image'] = $image;
				}
			}
			$this->M_proker->editProker($data,$id);
			redirect('AdminProker');
		}
	} 
}
