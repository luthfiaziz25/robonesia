<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminRekening extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_rekening');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
		if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$rekening = $this->M_rekening->getRekening();
		$this->template->load('admin/static','admin/adminRekening',compact('rekening'));
	}
	public function insertRekening($data){

		$this->db->insert('tabel_norekening',$data);
		$this->session->set_flashdata('sukses',"Galeri Foto Ditambahkan");
		return TRUE;
	}
	public function addRekening()
	{
		$image = $this->input->post('images');
		$bank = $this->input->post('bank');
		$atasnama = $this->input->post('atasnama');
		$no_rekening = $this->input->post('no_rekening');
		
		if ($image=''){} else{
			$config['upload_path']='./assets/img';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;
			// var_dump($image);
			// exit();
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('images')){
				echo "Menambahkan Rekening Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}

			$data =  array(

				'image' => $image,
				'bank' => $bank ,
				'atasnama' => $atasnama,
				'no_rekening' => $no_rekening
				
			);
			$this->M_rekening->insertRekening($data);
			redirect('adminRekening');
		}
	}
	public function hapusRekening($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('adminRekening');
		}else{
			$this->M_rekening->hapusRekening($data,$id);
			redirect('adminRekening');
		}
	}
	public function editRekening()
	{
		$this->form_validation->set_rules('id_norek', 'id_norek', 'required');
		$this->form_validation->set_rules('bank', 'bank', 'required');

		$image = $this->input->post('image');
		$bank = $this->input->post('bank');
		$atasnama = $this->input->post('atasnama');
		$no_rekening = $this->input->post('no_rekening');


		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('AdminRekening'); 
		}else{
			$data=array(
				
				"bank"=>$_POST['bank'],
				"atasnama"=>$_POST['atasnama'],
				"no_rekening"=>$_POST['no_rekening']
			);

			if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Menambahkan Foto Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['image'] = $image;
				}

			}
			$this->M_rekening->editRekening($data,$id_norek);
			redirect('AdminRekening');
		} 
	}
}