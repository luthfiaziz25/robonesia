<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminTransaksi extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_transaksi');
		$this->load->model('M_register');
		$this->load->model('M_data');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
		if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}
	public function index()
	{
		$transaksi = $this->M_register->getZakat();
		$sum = $this->M_data->get_sum();
		$count = $this->M_data->get_count();
		$this->template->load('admin/static','admin/adminTransaksi',compact('transaksi','sum','count'));
	}
	/*public function addTransaksi()
	{
		$jenis_transaksi = $this->input->post('jenis_transaksi');
		$nominal = $this->input->post('nominal');
		$nama = $this->input->post('nama');
		$usrername = $this->input->post('usrername');
		$status_transaksi = $this->input->post('status_transaksi');

		$data =  array(
			'status_transaksi'=> $status_transaksi,
			'nominal'	  => $nominal,
			'nama'     => $nama,
			'usrername'		  => $usrername,
			'status_transaksi'	=> $status_transaksi
		);
		$this->M_transaksi->insertTransaksi($data);
		redirect('adminTransaksi');
	}*/

	function approve(){
		$id_transaksi = $this->input->post('id_transaksi');
		$getTransaksi = $this->M_data->getTransaksi($id_transaksi);
	
			$data = array(
				'status' => "SUKSES"
			);
			
			$this->M_data->edit($id_transaksi, $data);
		redirect('AdminTransaksi');
	}
	/*public function hapusTransaksi($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('adminTransaksi');
		}else{
			$this->M_transaksi->hapusTransaksi($data,$id);
			redirect('adminTransaksi');
		}
	}*/

}
