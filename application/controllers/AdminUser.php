<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminUser extends CI_Controller {
	function __construct(){
		parent::__construct();
		if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}

	
	public function index()
	{
		$this->template->load('admin/static','admin/adminUser');
	}
}
