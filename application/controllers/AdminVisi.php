<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminVisi extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_visi');
		$this->load->helper('url');
		$this->load->library('template');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
        if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('index.php/loginadmin/login'));
		}
	}
	public function index()
	{
		$visi = $this->M_visi->getVisi();

		$this->template->load('admin/static','admin/adminVisi', compact('visi'));
	}
	public function addVisi(){

		$visi = $this->input->post('visi');
		
		$data =  array(
			'visi'=> $visi
		);
		$this->M_visi->insertVisi($data);
		redirect('adminVisi');
	}

	public function hapusVisi($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Visi Gagal Di Hapus");
			redirect('adminVisi');
		}else{
			$this->M_visi->hapusVisi($data,$id);
			redirect('adminVisi');
		}
	}

	public function editVisi()
     {
        $this->form_validation->set_rules('id_visi', 'id_visi', 'required');
        $this->form_validation->set_rules('visi', 'visi', 'required');
        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('adminVisi'); 
        }else{
            $data=array(
                "visi"=>$_POST['visi'],
           
            );
            $this->M_visi->editVisi($data,$id);
            redirect('adminVisi');
        }
    } 
}