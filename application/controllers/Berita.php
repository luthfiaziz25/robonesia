<?php
defined('BASEPATH') OR exit('No direct script access allow');

class Berita extends CI_Controller {

	private $limit = 4;

	function __construct(){
		parent:: __construct();
		$this->load->model('Beritaa','berita');
	}
	
	
	public function index()
	{
		$berita = $this->berita->latest()->all($this->limit);
		$total_rows = $this->berita->count();
		$links = pagination($total_rows, $this->limit);
		$content = ('home.php');

		$this->load->view('user/interfaceuser', compact('berita','total_rows','links','content'));
	}
	public function latest()
	{
		$berita = $this->berita->latest()->all($this->limit);
		$total_rows = $this->berita->count();
		$links = pagination($total_rows, $this->limit);
		$content = ('home.php');

		$this->load->view('user/interfaceuser', compact('berita','total_rows','links','content'));
	}
	public function populer()
	{
		$berita = $this->berita->populer()->all($this->limit);
		$total_rows = $this->berita->count();
		$links = pagination($total_rows, $this->limit);
		$content = ('berita.php');

		$this->load->view('user/interfaceuser', compact('berita','total_rows','links','content'));
	}

	public function detailBerita($id)
	{

		$row = $this->berita->find($id);
		if (empty($row)) {
			show_404();
		}
		$content = ('detailBerita.php');
		$this->image->view_count($id);

		$this->load->view('user/interfaceuser', compact('row','content'));
	}
}