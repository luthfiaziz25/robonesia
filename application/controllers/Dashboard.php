<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_transaksi');
		$this->load->model('M_register');
		$this->load->model('M_data');
		$this->load->helper('url');
		$this->load->library('template');
		if ($this->session->userdata('status') != "login_admin") {
			redirect(base_url('loginadmin/login'));
		}
	}

	
	public function index()
	{	
		$sum = $this->M_data->get_sum();
		$count = $this->M_data->get_count();
		$countMuzakki = $this->M_data->get_countMuzakki();
		$countSaran = $this->M_data->get_countSaran();
		$countGaleri = $this->M_data->get_countGaleri();
		$this->template->load('admin/static','admin/dashboard',compact('sum','count','countMuzakki','countSaran','countGaleri'));
	}
}
