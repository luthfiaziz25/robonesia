<?php

class Dashboarduser extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		$this->load->model("M_login");
		$this->load->model("M_register");
		$this->load->model("M_data");
		if($this->session->userdata('status') != "login_user"){
			redirect(base_url("overview/login"));
		}
	}

	function index(){
		$data['content'] = ('profiluser.php');
		$this->load->view('user/interfaceuser', $data);
	}
	
	function profilku(){
		$id = $this->session->userdata('id');
		$data = array(
			'content' => 'profiluser.php',
			'message' => 'test',
			'profilku' => $this->M_register->getByIdUser($id)
		);
		$this->load->view('user/interfaceuser', $data);
		/*$content = ('dashboarduser.php');
		$profilku = $this->M_register->getByIdUser($id);
		$this->load->view('user/interfaceuser', compact('content','profilku'));*/
	}
	function editprofil(){
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('username', 'username', 'required');

        $data = $this->M_register->getByIdUser($id);
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$foto = $this->input->post('foto');

        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Edit");
            redirect('Dashboarduser'); 
        }else{

	        
            $data=array(
                "nama"=>$nama,
                "alamat"=>$alamat,
            );

        	if (!empty($_FILES["foto"]["name"])){
				$config['upload_path']='./assets/img';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;


				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('foto')){
					echo "Menambahkan Berita Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['foto'] = $foto;
				}
	        }

            $this->M_data->editProfil($data,$id);
            redirect('Dashboarduser');
        }
    } 

	function zakatku(){
		$data['content'] = ('zakatku.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function tampilzakatku(){
		$id = $this->session->userdata('id');
		$data = array(
			'content' => 'zakatku.php',
			'message' => 'test',
			'tampilzakatku' => $this->M_register->getZakatku($id)
		);
		$this->load->view('user/interfaceuser', $data);
	}
	function saran(){
		$data['content'] = ('feedback.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function setting(){
		$data['content'] = ('setting.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function gantipass(){
		$id = $this->session->userdata('id');
		$password = md5($this->input->post('password'));
		$newpassword = md5($this->input->post('newpassword'));
		$renewpassword = md5($this->input->post('renewpassword'));

		if ($newpassword != $renewpassword) {
				die("Kata sandi baru yang dimasukan tidak valid");
			}
		if ($newpassword == $password) {
				die("Kata sandi tidak ada perubahan");
			}
		if ($newpassword == $renewpassword) {
			$data = $this->M_register->getByIdUser($id);
			if($data[0]->password==$password){
				//update
				$data = ['password' => $newpassword];

				$this->M_data->editpassword($id, $data);
				redirect('overview/home');
			}
			else{
				die("Kata Sandi Salah, Gagal Merubah kata sandi");
			}
		}
		else{
			die("Isi Field");
		}
	}
	function kirimfeedback(){
		$tampil['content'] = ('feedback.php');
		$id = $this->session->userdata('id');
		$kategori 	= $this->input->post('kategori');
		$isifeedback 	= $this->input->post('isifeedback');
		$time 			= date('Y-m-d H:i:s');

		$data = array(
			'kategori_feedback' => $kategori,
			'isi_feedback' => $isifeedback,
			'tanggal_feedback' => $time,
			'id_user' => $id
		);

		$this->M_data->input_data($data, 'saran');
		$this->session->set_flashdata('message','<script>alert("Terimakasih Atas Saran/Kritik Anda"); </script>');
		echo $this->session->flashdata('message');
		$this->load->view('user/interfaceuser', $tampil);
	} 
}