<?php

class Donasi extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		$this->load->model("M_login");
		$this->load->model("M_register");
		$this->load->model("M_data");
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('status') != "login_user"){
			redirect(base_url("overview/login"));
		}
	}

	function donasi(){
		$id = $this->session->userdata('id');
		$data['content'] = ('donasi.php');
		$data['profilku'] = $this->M_register->getByIdUser($id);
		$this->load->view('user/interfaceuser', $data);
	}

	function insert_zakat(){
		$id 			= $this->input->post('id');
		$type_zakat 	= $this->input->post('type_zakat');
		$jumlahuang 	= $this->input->post('jumlahuang');
		$metode 		= $this->input->post('metode_pembayaran');
		$time 			= date('Y-m-d H:i:s');

		$data = [
			'jenis_zakat' => $type_zakat,
			'jumlah_zakat' => $jumlahuang,
			'metode_pembayaran' => $metode,
			'tanggal' => $time,
			'status_pembayaran' => "PENDING"
		];
		$this->session->set_userdata($data);
		$this->pembayaran();
	}

	function pembayaran(){
		$id = $this->session->userdata('id');
		$data = array(
			'content' => 'pembayaran.php',
			'profilku' => $this->M_register->getByIdUser($id),
			'rekening' => $this->M_data->ambil_data('tabel_norekening')->result()
		);
		$this->load->view('user/interfaceuser', $data);
	}

	function cancel(){
		$this->session->set_userdata('jenis_zakat');
		$this->session->set_userdata('jumlah_zakat');
		$this->session->set_userdata('metode_pembayaran');
		redirect(base_url('donasi/donasi'));
	}

	function bayar(){
		$id = $this->session->userdata('id');
		$data = array(
			'jenis_zakat' =>$this->session->userdata('jenis_zakat'),
			'jumlah_zakat' =>$this->session->userdata('jumlah_zakat'),
			'metode_pembayaran' =>$this->session->userdata('metode_pembayaran'),
			'tanggal' =>$this->session->userdata('tanggal'),
			'status' =>$this->session->userdata('status_pembayaran'),
			'id' => $id
		);

		$this->M_data->input_data($data, 'transaksi');
		redirect(base_url('dashboarduser/tampilzakatku'));
	} 

	function v_bukti($id_transaksi){
		$data = array(
			'data' => $this->M_data->getAllTransaksi($id_transaksi)
		);
		$this->load->view('v_bukti', $data);
	}

	function uploadbukti(){
		$time 			= date('Y-m-d H:i:s');
		$id_transaksi = $this->input->post('id_transaksi');
		$getTransaksi = $this->M_data->getTransaksi($id_transaksi);
		
		$config = [
			 'upload_path' => './assets/img/uploadbukti/',
			 'allowed_types' => 'gif|jpg|png',
			 'max_size' => 3000
		 ];
		 $this->load->library('upload', $config);

		 if (!$this->upload->do_upload('userfile')) {
		 	$error = array('error' => $this->upload->display_errors());
		 	$this->load->view('dashboarduser/tampilzakatku',$error);
		 }else{
			$file = $this->upload->data();
			
			if($getTransaksi->uploadbukti != NULL){
				unlink('assets/img/uploadbukti/'.$getTransaksi->uploadbukti);
			}

			$data = array(
				'upload_bukti'=> $file['file_name'],
				'tanggal_upload' => $time,
			);
			
			$this->M_data->edit($id_transaksi, $data);
		}
		redirect('dashboarduser/tampilzakatku');
	}

}