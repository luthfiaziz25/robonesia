<?php
defined('BASEPATH') OR exit('No direct script access allow');

class Galeri extends CI_Controller {

	private $limit = 16;

	function __construct(){
		parent:: __construct();
		$this->load->model('M_galeri','image');
	}
	
	
	public function index()
	{
		$query = $this->image->all($this->limit);
		$total_rows = $this->image->count();
		$links = pagination($total_rows, $this->limit);
		$content = ('galeri.php');

		$this->load->view('user/interfaceuser', compact('query','total_rows','links','content'));
	}
	public function latest()
	{
		$query = $this->image->latest()->all($this->limit);
		$total_rows = $this->image->count();
		$links = pagination($total_rows, $this->limit);
		$content = ('galeri.php');

		$this->load->view('user/interfaceuser', compact('query','total_rows','links','content'));
	}
	public function populer()
	{
		$query = $this->image->populer()->all($this->limit);
		$total_rows = $this->image->count();
		$links = pagination($total_rows, $this->limit);
		$content = ('galeri.php');

		$this->load->view('user/interfaceuser', compact('query','total_rows','links','content'));
	}

	public function detailGaleri($id)
	{

		$row = $this->image->find($id);
		if (empty($row)) {
			show_404();
		}
		$content = ('detailGaleri.php');
		$this->image->view_count($id);

		$this->load->view('user/interfaceuser', compact('row','content'));
	}
	public function download()
	{
		if (! empty($_POST['image_id'])) 
		{
			$id  = $this->input->post('image_id');
			$row = $this->image->find($id);
			$this->image->download_count($id);
			$this->load->helper('download');
			force_download("./assets/img/". $row->filename, NULL);
		}
	}

	/*public function generate()
	{
	for ($i = 9; $i <= 16; $i++)
	{
	 		$title = "Image" .$i;

			$this->db->insert("images", array(

	 			'title' => $title,
	 			'filename' => urldecode($title) . ".jpg",
	 			'created_at' => date("Y-m-d H:i:s"),
	 			'view_count' => rand(0,10),
	 			'download_count' => rand(0,10)

	 		));

	 		echo $this->db->last_query();
	 	}
	 }*/
}