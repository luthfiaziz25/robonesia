<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulator extends CI_Controller {
	
	// function __construct(){
	// 	$this->load->library('template');
	// 	$this->load->helper('url');
	// }
	public function __construct()
    {
		parent::__construct();
		/*$this->load->model('');*/
		$this->load->helper('url');
	}

	function kemas(){
		$data['content'] = ('kemas.php');
		$this->load->view('kalkulator', $data);
	}

	function kfitrah(){
		$data['content'] = ('kfitrah.php');
		$this->load->view('kalkulator', $data);
	}
	function kmaal(){
		$data['content'] = ('kmaal.php');
		$this->load->view('kalkulator', $data);
	}
	function kperusahaan(){
		$data['content'] = ('kperusahaan.php');
		$this->load->view('kalkulator', $data);
	}
	function kprofesi(){
		$data['content'] = ('kprofesi.php');
		$this->load->view('kalkulator', $data);
	}


}
