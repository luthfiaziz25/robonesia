<?php
Class Laporanpdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }
    
    function index(){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'UNIT PENGUMPUL ZAKAT',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'Laporan Transaksi Zakat',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(40,6,'Jenis Zakat',1,0);
        $pdf->Cell(35,6,'Jumlah Zakat',1,0);
        $pdf->Cell(40,6,'Tanggal',1,0);
        $pdf->Cell(30,6,'Status',1,1);
        $pdf->SetFont('Arial','',10);
        $transaksi = $this->db->get('transaksi')->result();
        foreach ($transaksi as $row){
            $pdf->Cell(40,6,$row->jenis_zakat,1,0);
            $pdf->Cell(35,6,$row->jumlah_zakat,1,0);
            $pdf->Cell(40,6,$row->tanggal,1,0);
            $pdf->Cell(30,6,$row->status,1,1); 
        }
        $pdf->Output();
    }
}