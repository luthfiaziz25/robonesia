<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginAdmin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_loginadmin');
	}

	
	public function login()
	{
		$this->load->view('admin/loginadmin');
	}
	function aksiLoginadmin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'role' => 1,
			'password' => md5($password)
		);
		
		$cek = $this->M_loginadmin->cek_login("tabel_loginadmin",$where)->num_rows();
		if($cek > 0){
			$data_session = array (
				'username' => $username,
				'status' => "login_admin"
			);

			$this->session->set_userdata($data_session);
			/*foreach ($cek->result() as $session) {
				$sess_data['id'] = $session->id;
				$sess_data['username'] = $session->username;
				$sess_data['status'] = 'loginadmin';
				$this->session->set_userdata($sess_data);
			}*/

		redirect(base_url("admin"));
		}
		else{
		echo "Username dan password salah !";
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('loginadmin/login'));
	}
}
