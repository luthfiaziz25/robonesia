<?php

class Overview extends CI_Controller {

	private $limit = 3;
	private $limit2 = 4;
    public function __construct()
    {
		parent::__construct();
		$this->load->model("M_login");
		$this->load->model("M_data");
		$this->load->model("M_register");
		$this->load->model('M_rekening');
		$this->load->model("M_video",'video');
		$this->load->model("Beritaa",'berita');
		$this->load->model("Prokerr",'proker');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}


	function index()
	{
        // load view user/overview.php
        $content = ('home.php');
        $sum = $this->M_data->get_sum();
		$count = $this->M_data->get_count();
		$countMuzakki = $this->M_data->get_countMuzakki();
		$countSaran = $this->M_data->get_countSaran();
		$countGaleri = $this->M_data->get_countGaleri();
        $berita =$this->db->query("SELECT * FROM berita ORDER BY views DESC LIMIT 3");
        $proker =$this->db->query("SELECT * FROM tabel_proker ORDER BY waktu DESC LIMIT 4");
        $proker = $this->proker->all($this->limit2);
        $this->load->view('user/interfaceuser',compact('content','berita','proker','sum','count','countGaleri','countMuzakki','countSaran'));
	}

	function home(){
		$content = ('home.php');
        $proker = $this->proker->all($this->limit2);
        $berita =$this->db->query("SELECT * FROM berita ORDER BY views DESC LIMIT 3");
        $this->load->view('user/interfaceuser',compact('content','berita','proker'));
	}
	function visimisi(){
		$berita =$this->db->query("SELECT * FROM berita ORDER BY views DESC LIMIT 4");
		$content = ('visimisi.php');
		$this->load->view('user/interfaceuser', compact('berita','content'));
	}
	function struktur(){
		$data['content'] = ('struktur.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function panduan(){
		$data['content'] = ('zakat.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function rekening(){
		$content = ('rekening.php');
		$rekening = $this->M_rekening->getRekening();
		$this->load->view('user/interfaceuser',compact('content','rekening'));
	}
	/*function minggu(){
		$data['content'] = ('minggu.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function bulan(){
		$data['content'] = ('bulan.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function tahun(){
		$data['content'] = ('tahun.php');
		$this->load->view('user/interfaceuser', $data);
	}*/
	function galeri(){
		$data['content'] = ('galeri.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function detailgaleri(){
		$data['content'] = ('detailGaleri.php');
		$this->load->view('user/interfaceuser', $data);
	}

	/*Bagian Register dan Authentification*/

	function signup(){
		/*$data['register'] =$this->m_data->register('register');*/
		$data['content'] = ('dashboarduser.php');
		$this->load->view('user/interfaceuser', $data);
	}

	function add()
	{
		$nama =$this->input->post('nama');
		$password =$this->input->post('password');
		$username =$this->input->post('username');
		$email =$this->input->post('email');
		$nomor_hp =$this->input->post('nomor_hp');
		$pekerjaan =$this->input->post('pekerjaan');
		$alamat =$this->input->post('alamat');

		$config = [
			 'upload_path' => './assets/img/',
			 'allowed_types' => 'gif|jpg|png',
			 'max_size' => 1000, 'max_width' => 1000,
			 'max_height' => 1000
		 ];
		 $this->load->library('upload',$config);
		 if (!$this->upload->do_upload()) 
		 {
		 	$error = array('error' => $this->upload->display_errors());
		 	$this->load->view('dashboarduser',$error);
		 }
		 else{
		 	$file = $this->upload->data();
		 	$data = array(
		 		'foto' => $file['file_name'],
		 		'nama' => $nama,
		 		'password' => md5($password),
		 		'username' => $username,
		 		'email' => $email,
		 		'nomor_hp' => $nomor_hp,
		 		'pekerjaan' => $pekerjaan,
		 		'alamat' => $alamat,
		 		'role' => 0
		 	);
		 	$this->M_data->input_data($data,'muzakki');
		 	redirect('overview/login');
		 }
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('overview/home'));
	}
	function login(){
		$this->load->view('loginuser.php');
	}
	function aksi_login_user(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'role' => 0,
			'password' => md5($password)
			);
		$cek = $this->M_login->cek_login("muzakki",$where);
		if($cek->num_rows() == 1){

			foreach ($cek->result() as $session) {
				$sess_data['id'] = $session->id;
				$sess_data['username'] = $session->username;
				$sess_data['status'] = 'login_user';
				$this->session->set_userdata($sess_data);
			}

			redirect(base_url("overview/home"));

		}else{
			echo "Username dan password salah !";
		}
	}
	

	/*Akhir Auth*/

	function detailberita($id){

		$row = $this->berita->find($id);
		if (empty($row)) {
			show_404();
		}
		$this->berita->view_count($id);
		$proker =$this->db->query("SELECT * FROM tabel_proker ORDER BY waktu DESC LIMIT 4");
		$beritatop =$this->db->query("SELECT * FROM berita ORDER BY views DESC LIMIT 4");
		$id = $this->uri->segment(3);
		$berita= $this->berita->getSingle($id);
		$video=$this->video->getVideo();
		$content = ('detailberita.php');
		$this->load->view('user/interfaceuser', compact('id','berita','content','proker','video','beritatop'));
	}
	function lebihbanyakberita()
	{
        // load view user/overview.php
        $content = ('lebihbanyakberita.php');
        $berita = $this->berita->all();
        $this->load->view('user/interfaceuser',compact('content','berita'));
	}
	function detailpengabdian($id){
		$row = $this->proker->find($id);
		if (empty($row)) {
			show_404();
		}
		$berita =$this->db->query("SELECT * FROM berita ORDER BY views DESC LIMIT 4");
		$prokernew =$this->db->query("SELECT * FROM tabel_proker ORDER BY waktu DESC LIMIT 4");
		$id = $this->uri->segment(3);
		$proker= $this->proker->getSingle($id);
		$video=$this->video->getVideo();
		$content = ('detailpengabdian.php');
		$this->load->view('user/interfaceuser', compact('id','berita','content','proker','video','prokernew'));
	}
	function lebihbanyakprogram()
	{
        // load view user/overview.php
        $content = ('lebihbanyakprogram.php');
        $proker = $this->proker->all();
        $this->load->view('user/interfaceuser',compact('content','proker'));
	}
	function moreberita(){
		$data['content'] = ('artikel.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function morepengabdian(){
		$data['content'] = ('artikel.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function asnaf(){
		$data['content'] = ('asnaf.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function fitrah(){
		$data['content'] = ('fitrah.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function maal(){
		$data['content'] = ('maal.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function profesi(){
		$data['content'] = ('propesi.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function perusahaan(){
		$data['content'] = ('perusahaan.php');
		$this->load->view('user/interfaceuser', $data);
	}
	function surat(){
		$data['content'] = ('surat.php');
		$this->load->view('user/interfaceuser', $data);
	}

	public function cari(){
    $keyword = $this->input->get('cari', TRUE); //mengambil nilai dari form input cari
    $berita = $this->berita->cari($keyword);
    $proker = $this->proker->cari($keyword); //mencari data karyawan berdasarkan inputan
    $content = ('result.php'); 
    $this->load->view('user/interfaceuser',compact('berita','content','proker')); //menampilkan data yang sudah dicari
  }

}