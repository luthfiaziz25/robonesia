<?php
defined('BASEPATH') OR exit('No direct script access allow');

class Proker extends CI_Controller {

	private $limit2 = 4;

	function __construct(){
		parent:: __construct();
		$this->load->model('Prokerr','proker');
	}
	
	
	public function index()
	{
		$proker = $this->proker->latest()->all($this->limit2);
		$total_rows = $this->proker->count();
		$links = pagination($total_rows, $this->limit2);
		$content = ('home.php');

		$this->load->view('user/interfaceuser', compact('proker','total_rows','links','content'));
	}
	public function latest()
	{
		$proker = $this->proker->latest()->all($this->limit2);
		$total_rows = $this->proker->count();
		$links = pagination($total_rows, $this->limit2);
		$content = ('home.php');

		$this->load->view('user/interfaceuser', compact('proker','total_rows','links','content'));
	}
	public function populer()
	{
		$proker = $this->proker->populer()->all($this->limit2);
		$total_rows = $this->proker->count();
		$links = pagination($total_rows, $this->limit2);
		$content = ('proker.php');

		$this->load->view('user/interfaceuser', compact('proker','total_rows','links','content'));
	}

	public function detailproker($id)
	{

		$row = $this->proker->find($id);
		if (empty($row)) {
			show_404();
		}
		$content = ('detailproker.php');
		$this->image->view_count($id);

		$this->load->view('user/interfaceuser', compact('row','content'));
	}
}