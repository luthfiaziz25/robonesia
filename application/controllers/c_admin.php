<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_admin extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('m_admin');
		// $this->load->helper('url');
       // $this->load->library('upload');
        //$this->load->model('M_login','login');
		// if ($this->session->userdata('status') != "login_admin") {
		// 	redirect(base_url('loginadmin/login'));
		// }
	}

	public function index()
	{
		$this->load->view('admin/static');
		$this->load->view('admin/dashboard');
	}

	public function artikel(){
		$data['artikel'] = $this->m_admin->getartikel();
		$this->load->view('admin/static');
		$this->load->view('admin/artikel',$data);
	}

}