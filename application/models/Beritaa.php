<?php
defined('BASEPATH') OR exit('No direct script access allow');

class Beritaa extends CI_Model {

  private $table ="berita";

  public function all($limit = 8)
  {
    $this->db->limit($limit);
    $this->db->offset( $this->uri->segment(3) );
    return $this->db->get($this->table);
  }

  public function GetSingle($id){
    $singel = $this->db->select('*')
              ->from('berita')
              ->where('id',$id) 
              ->get();
    return $singel;
  }

  public function count()
  {
    return $this->db->count_all_results($this->table);
  }
  public function latest()
  {
    $this->db->order_by("posted", "desc");

    return $this;
  }
  public function populer()
  {
    $this->db->order_by("views", "desc");
    
    return $this;
  }
  public function find($id)
  {
    $this->db->where("id",$id);
    $query = $this->db->get($this->table);
    return $query->num_rows() ? $query->row() : null;
  }
  public function view_count($id)
  {
    $this->db->set("views", "views+1",false);
    $this->db->where("id",$id);
    $this->db->update($this->table);
  }

  public function data(){
    return $this->db->get('berita')->result();
  }

  public function cari($keyword){
    $this->db->like('judul', $keyword)->or_like('isi', $keyword)->or_like('penulis', $keyword); //mencari data yang serupa dengan keyword
    return $this->db->get('berita')->result();
  }
}