<?php  

class M_berita extends CI_Model{

	public function getBerita(){
    $data = $this->db->get('berita');
    return $data;
  }
  public function insertBerita($data){
    $this->db->insert('berita',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }
  function editBerita($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('berita',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function hapusBerita($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('berita');
    $this->session->set_flashdata('sukses',"Berita Berhasil Dihapus");
    return TRUE;
  }
  

/*Memanggil Data Untuk Tampilan User*/
  
}