<?php

class M_data extends CI_Model{

  function ambil_data($table){
		return $this->db->get($table);
	}

  function ambil_data_kondisi($table, $where){
    $this->db->where($where);
    return $this->db->get($table);
  }

  function input_data($data,$table){
    $this->db->insert($table,$data);
  }

  function hapus_data($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  }

  function editProfil($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('muzakki',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

  function edit_data($where,$table){
  	return $this->db->get_where($table,$where);
  }

  function update_data($where,$data,$table){
		$this->db->where('id', $where);
		$this->db->update($table,$data);
	}

  function edit($id, $data){
    try{
      $this->db->where('id_transaksi', $id)->update('transaksi', $data);
      return true;
    }catch(Exception $e){
      echo "System Error". $e->getMessage();
    }
  }
  function editpassword($id, $data){
    try{
      $this->db->where('id', $id)->update('muzakki', $data);
      return true;
    }catch(Exception $e){
      echo "System Error". $e->getMessage();
    }
  }

  function getTransaksi($id_transaksi){
    $this->db->where('id_transaksi', $id_transaksi);
    return $this->db->get('transaksi')->row();
  }

  function getAllTransaksi($id_transaksi){
    $query = $this->db->query("SELECT * FROM transaksi WHERE id_transaksi = '$id_transaksi'");
    return $query->result();
  }
  public function get_sum()
  {
    $sql = "SELECT sum(jumlah_zakat) as jumlah_zakat FROM transaksi";
    $result = $this->db->query($sql);
    return $result->row()->jumlah_zakat;
  }
  public function get_count()
  {
    $sql = "SELECT count(jumlah_zakat) as jumlah_zakat FROM transaksi";
    $result = $this->db->query($sql);
    return $result->row()->jumlah_zakat;
  }

  public function get_countMuzakki()
  {
    $sql = "SELECT count(nama) as nama FROM muzakki";
    $result = $this->db->query($sql);
    return $result->row()->nama;
  }

  public function get_countSaran()
  {
    $sql = "SELECT count(kategori_feedback) as kategori_feedback FROM saran";
    $result = $this->db->query($sql);
    return $result->row()->kategori_feedback;
  }

  public function get_countGaleri()
  {
    $sql = "SELECT count(filename) as filename FROM images";
    $result = $this->db->query($sql);
    return $result->row()->filename;
  }

}
