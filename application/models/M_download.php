<?php  

class M_download extends CI_Model{


	public function getDownload(){
    $data = $this->db->get('tabel_download');
    return $data;
  }
  public function insertDownload($data){
    $this->db->insert('tabel_download',$data);
    $this->session->set_flashdata('sukses',"File Download Berhasil Ditambahkan");
    return TRUE;
  }
  function editDownload($data,$id){
    $this->db->where('id_file', $_POST['id_file']);
    $this->db->update('tabel_download',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function hapusDownload($data, $id){
    $this->db->where('id_file', $id);
    $this->db->delete('tabel_download');
    $this->session->set_flashdata('sukses',"File Download Berhasil Dihapus");
    return TRUE;
  }

}