<?php
defined('BASEPATH') OR exit('No direct script access allow');

class M_galeri extends CI_Model {

	private $table ="images";

	public function all($limit = 8)
	{
		$this->db->limit($limit);
		$this->db->offset( $this->uri->segment(3) );
		return $this->db->get($this->table);
	}

	public function count()
	{
		return $this->db->count_all_results($this->table);
	}
	public function latest()
	{
		$this->db->order_by("created_at", "desc");

		return $this;
	}
	public function populer()
	{
		$this->db->order_by("download_count", "desc");
		
		return $this;
	}
	public function find($id)
	{
		$this->db->where("id",$id);
		$query = $this->db->get($this->table);
		return $query->num_rows() ? $query->row() : null;
	}
	public function view_count($id)
	{
		$this->db->set("view_count", "view_count+1",false);
		$this->db->where("id",$id);
		$this->db->update($this->table);
	}
	public function download_count($id)
	{
		$this->db->set("download_count", "download_count+1",false);
		$this->db->where("id",$id);
		$this->db->update($this->table);
	}
	 public function insertFoto($data)
    {
        $this->db->insert('images',$data);
		$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
   	    return TRUE;
    }
    public  function hapusFoto($data, $id){
        $this->db->where('id', $id);
        $this->db->delete('images');
        $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
        return TRUE;
    }
}