<?php  

class M_galerifoto extends CI_Model{


	public function getGalerifoto(){

    $data = $this->db->get('images');
    return $data;
  }
  public function insertGalerifoto($data){

    $this->db->insert('images',$data);
    $this->session->set_flashdata('sukses',"Galeri Foto Ditambahkan");
    return TRUE;
  }
  function editGalerifoto($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('images',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function hapusGalerifoto($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('images');
    $this->session->set_flashdata('sukses',"Galeri Foto Berhasil Dihapus");
    return TRUE;
  }


}