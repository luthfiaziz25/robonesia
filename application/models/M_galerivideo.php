<?php  

class M_galerivideo extends CI_Model{


	public function getGalerivideo(){

    $data = $this->db->get('video');
    return $data;
  }

  public function insertGalerivideo($data){

    $this->db->insert('video',$data);
    $this->session->set_flashdata('sukses',"Video Berhasil Ditambahkan");
    return TRUE;
  }

  public  function hapusGalerivideo($data, $id){
    $this->db->where('id_video', $id);
    $this->db->delete('video');
    $this->session->set_flashdata('sukses',"Video Berhasil Dihapus");
    return TRUE;
  }

  
  function editGalerivideo($data,$id){
    $this->db->where('id_video', $_POST['id_video']);
    $this->db->update('video',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

}