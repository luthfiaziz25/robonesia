<?php  

class M_misi extends CI_Model{


	public function getMisi(){

    $data = $this->db->get('tabel_misi');
    return $data;
  }

  public function insertMisi($data){

    $this->db->insert('tabel_misi',$data);
    $this->session->set_flashdata('sukses',"Misi Berhasil Ditambahkan");
    return TRUE;
  }

  public  function hapusMisi($data, $id){
    $this->db->where('id_misi', $id);
    $this->db->delete('tabel_misi');
    $this->session->set_flashdata('sukses',"Misi Berhasil Dihapus");
    return TRUE;
  }

  
  function editMisi($data,$id){
    $this->db->where('id_misi', $_POST['id_misi']);
    $this->db->update('tabel_misi',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

}