<?php  

class M_mustahik extends CI_Model{


	public function getMustahik(){
    $data = $this->db->get('tabel_mustahik');
    return $data;
  }
  public function insertMustahik($data){
    $this->db->insert('tabel_mustahik',$data);
    $this->session->set_flashdata('sukses',"Data Mustahik Berhasil Ditambahkan");
    return TRUE;
  }
   function editMustahik($data,$id_mustahik){
    $this->db->where('id_mustahik', $_POST['id_mustahik']);
    $this->db->update('tabel_mustahik',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function hapusMustahik($data, $id){
    $this->db->where('id_mustahik', $id);
    $this->db->delete('tabel_mustahik');
    $this->session->set_flashdata('sukses',"Data Mustahik Berhasil Dihapus");
    return TRUE;
  }

}