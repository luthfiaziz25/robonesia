<?php  

class M_muzakki extends CI_Model{


	public function getMuzakki(){
    $data = $this->db->get('muzakki');
    return $data;
  }
  public function insertMuzakki($data){
    $this->db->insert('muzakki',$data);
    $this->session->set_flashdata('sukses',"Data Muzakki Berhasil Ditambahkan");
    return TRUE;
  }
   function editMuzakki($data,$id_muzakki){
    $this->db->where('id', $_POST['id']);
    $this->db->update('muzakki',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function hapusMuzakki($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('muzakki');
    $this->session->set_flashdata('sukses',"Data Muzakki Berhasil Dihapus");
    return TRUE;
  }

}