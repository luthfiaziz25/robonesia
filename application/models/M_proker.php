<?php  

class M_proker extends CI_Model{


	public function getProker(){
		$data = $this->db->get('tabel_proker');
		return $data;
	}
	public function insertProker($data){

		$this->db->insert('tabel_proker',$data);
		$this->session->set_flashdata('sukses',"Program Kerja Berhasil Ditambahkan");
		return TRUE;
	}
	function editProker($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('tabel_proker',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
	public  function hapusProker($id){
		$this->db->where('id', $id);
		$this->db->delete('tabel_proker');
		$this->session->set_flashdata('sukses',"Program Kerja Berhasil Dihapus");
		return TRUE;
	}
}