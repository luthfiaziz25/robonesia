<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_register extends CI_Model{

    private $table = "muzakki";
    public $user_id;
    public $username;
    public $password;
    public $email;
    public $nama;
    public $nomor_hp;
    public $foto = "default.jpg";
    public $pekerjaan;
    public $alamat;

    public function rules(){

        return[
            ['field' => 'username','label'=>'Username','rules'=> 'required'],
            ['field' => 'password','label'=>'Password','rules'=> 'password'],
            ['field' => 'email','label'=>'Email','rules'=> 'email'],
            ['field' => 'nama','label'=>'Nama','rules'=> 'nama'],
            ['field' => 'nomor_hp','label'=>'Nomor_hp','rules'=> 'nomor_hp'],
            ['field' => 'pekerjaan','label'=>'Pekerjaan','rules'=> 'pekerjaan'],
            ['field' => 'alamat','label'=>'Alamat','rules'=> 'alamat']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->table)->result();
    }

    public function getByIdUser($id){
        $query = $this->db->query("SELECT * FROM muzakki WHERE id='$id'");
        return $query->result(); 
    }
    
    public function getZakatku($id){
        $query = $this->db->query("SELECT * FROM transaksi WHERE id='$id'");
        return $query->result();
    }
    public function getZakat(){
        $data = $this->db->get('transaksi');
        return $data;
    }
}