<?php  

class M_rekening extends CI_Model{


	public function getRekening(){
    $data = $this->db->get('tabel_norekening');
    return $data;
  }
  public function insertRekening($data){
    $this->db->insert('tabel_norekening',$data);
    $this->session->set_flashdata('sukses',"Data Mustahik Berhasil Ditambahkan");
    return TRUE;
  }
   function editRekening($data,$id_mustahik){
    $this->db->where('id_norek', $_POST['id_norek']);
    $this->db->update('tabel_norekening',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function hapusRekening($data, $id){
    $this->db->where('id_norek', $id);
    $this->db->delete('tabel_norekening');
    $this->session->set_flashdata('sukses',"Data Mustahik Berhasil Dihapus");
    return TRUE;
  }

}