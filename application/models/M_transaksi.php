<?php  

class M_transaksi extends CI_Model{


	public function getTransaksi(){
    $data = $this->db->get('tabel_transaksi');
    return $data;
  }
  public function insertTransaksi($data){
    $this->db->insert('tabel_transaksi',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }
  public  function hapusTransaksi($data, $id){
    $this->db->where('id_transaksi', $id);
    $this->db->delete('tabel_transaksi');
    $this->session->set_flashdata('sukses',"Berita Berhasil Dihapus");
    return TRUE;
  }
}