<?php  

class M_visi extends CI_Model{


	public function getVisi(){

    $data = $this->db->get('tabel_visi');
    return $data;
  }

  
  public function insertVisi($data){

    $this->db->insert('tabel_visi',$data);
    $this->session->set_flashdata('sukses',"Video Berhasil Ditambahkan");
    return TRUE;
  }

  public  function hapusVisi($data, $id){
    $this->db->where('id_visi', $id);
    $this->db->delete('tabel_visi');
    $this->session->set_flashdata('sukses',"Video Berhasil Dihapus");
    return TRUE;
  }

  
  function editVisi($data,$id){
    $this->db->where('id_visi', $_POST['id_visi']);
    $this->db->update('tabel_visi',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

}