<?php  

class m_admin extends CI_Model{

	public function getartikel(){
    $data = $this->db->get('artikel');
    return $data;
  }
  public function insertartikel($data){
    $this->db->insert('artikel',$data);
    $this->session->set_flashdata('sukses',"Artikel Berhasil Ditambahkan");
    return TRUE;
  }
  function editartikel($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('artikel',$data);
    $this->session->set_flashdata('sukses',"Artikel Berhasil Diedit");
    return TRUE;
  }
  public  function deleteartikel($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('artikel');
    $this->session->set_flashdata('sukses',"Artikel Berhasil Dihapus");
    return TRUE;
  }
  
}