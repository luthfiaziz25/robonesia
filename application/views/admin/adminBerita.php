<div id="page-wrapper">
	<div class="page-title" style="margin-top: -20px;">
		<div class="title_left">
		 	<h3>Berita</h3>
		 	<h3><small>Klik untuk menambahkan/mengedit</small></h3>
	 	</div>
 	</div>

	<?php 
		$data=$this->session->flashdata('sukses');
		if($data!=""){ ?>
			<div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
		<?php } ?>

		<?php 
		$data2=$this->session->flashdata('error');
		if($data2!=""){ ?>
			<div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
	<?php } ?>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="line-height:70px;">
						<a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-tambahberita"><span class="fa fa-plus"></span> Tambah Berita</a>
						<div class="box-body" style="overflow: auto;">
							<table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
								<thead>
									<tr style="background: #fff;">
										<th width="5"><center>No</center></th>
										<th width="200px"><center>Judul</center></th>
										<th width="100"><center>Gambar</center></th>
										<th width="80"><center>Posted</center></th>
										<th width="120"><center>Penulis</center></th>
										<th width="400"><center>Isi</center></th>
										<th width="80"><center>Aksi</center></th>
									</tr>
								</thead>

								<tbody>
									<?php $no=1; foreach($berita->result() as $row): ?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $row->judul; ?></td>
										<td>
											<img width="200" src="<?php echo base_url(). 'assets/img/'.$row->image; ?>" class="img-thumbnail" >
										</td>
										<td><?php echo date('d F Y', strtotime($row->posted)); ?></td>
										<td><?php echo $row->penulis; ?></td>
										<td><?php echo $row->isi; ?></td>
										<td align="center">
											<a href="" data-toggle="modal" data-target="#modal-editberita<?=$row->id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
											<a href="<?php echo site_url('adminBerita/hapusBerita/'.$row->id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modal-tambahberita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
					<h4 class="modal-title" id="myModalLabel"> Tambah</h4>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('adminBerita/addBerita'); ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="modal-body">
						<input name="id" type="hidden" value="">
							<div class="form-group">
								<label>Judul</label>
								<input class="form-control" name="judul" type="text" placeholder="Input judul Berita" value="" required>
							</div>
							<div class="form-group">
								<label>Gambar</label>
								<input class="form-control" type="file" name="image">
							</div>
							<div class="form-group">
								<label>Penulis</label>
								<input class="form-control" name="penulis" type="text" placeholder="Penulis berita" value="" required>
								</div>
							<div class="form-group">
								<label>Isi berita</label><br>
								<textarea  name="isi" style="width: 100%" placeholder="Tuliskan berita terbaru"></textarea>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>  

	<?php $no=0; foreach($berita->result() as $row): $no++; ?>
	<div class="row">
		<div class="modal fade" id="modal-editberita<?=$row->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header bg-primary">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
		        <h4 class="modal-title" id="myModalLabel"> EDIT</h4>
		      </div>
		      <form class="form-horizontal" action="<?php echo site_url('adminBerita/editBerita '); ?>" method="post" enctype="multipart/form-data">
		        <div class="modal-body">

		          <input type="hidden" readonly value="<?=$row->id;?>" name="id" class="form-control" >
		          
		            <div class="form-group">
		              <label class="col-sm-2">Judul</label>
		              <div class="col-sm-8">
		              <input class="form-control" name="judul" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->judul;?>" required>
		       		  </div>
		            </div>
		            <div class="form-group">
						<label class="col-sm-2"	>Gambar</label>
						<div class="col-sm-8">
						<input class="form-control" type="file" name="image">
						</div>
					</div>
		            <div class="form-group">
		              <label class="col-sm-2">Isi</label>
		              <div class="col-sm-8">
		              <textarea  class="form-control" autocomplete="off" placeholder="Masukan Konten Berita" name="isi"><?php echo $row->isi;?></textarea>
		          	</div>
		            </div>
		            <div class="form-group">
		              <label class="col-sm-2">Penulis</label>
		              <div class="col-sm-8">
		              <input class="form-control" name="penulis" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->penulis;?>" required>
		              </div>
		            </div>
		      

		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
		          <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
		        </div>
		      </form>
		    </div>
		  </div>
		</div>  
	</div>
	<?php endforeach; ?>''

</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
    tinymce.init({
            selector: "textarea",
            plugins: [
                    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
            ],
    
            toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
    
            menubar: false,
            toolbar_items_size: 'small',
            image_advtab: true,
            style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
    
            templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
            ]
    });
    </script>
    <script>
      $(document).ready(function(){
        $("#myInput").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
    </script>
