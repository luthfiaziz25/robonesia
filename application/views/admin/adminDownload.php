<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
      <h3>Layanan Download File</h3>
      <h3><small>Klik untuk menambahkan/mengedit</small></h3>
    </div>
  </div>

  <?php 
    $data=$this->session->flashdata('sukses');
    if($data!=""){ ?>
      <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
    <?php } ?>

    <?php 
    $data2=$this->session->flashdata('error');
    if($data2!=""){ ?>
      <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
    <?php } ?>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header" style="line-height:70px;">
            <a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahdownload"><span class="fa fa-plus"></span> Tambah File</a>
            <div class="box-body" style="overflow: auto;">
              <table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
                <thead>
                  <tr style="background: #fff;">
                    <th width="5"><center>No</th>
                   <th ><center>Judul</th>
                    <th width="400"><center> File</th>
                    <th width="110"><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                  <tbody>
                  <?php $no=1; foreach($download->result() as $row): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row->namafile; ?></td>
                    <td>
                      <element width="200" height="180" src="<?php echo base_url(). 'assets/document/'.$row->link_file; ?>" type="pdf"></element>
                    </td>
                    <td align="center">
                      <a href="" data-toggle="modal" data-target="#modal-editdownload<?=$row->id_file;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                      <a href="<?php echo site_url('adminDownload/hapusDownload/'.$row->id_file); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <div class="modal fade" id="modal-tambahdownload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary"> 
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
          <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
        </div>

        <form class="form-horizontal" action="<?php echo site_url('adminDownload/addDownload'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="modal-body">
              <div class="form-group">
                <label>Nama File</label>
                <input type="text" name="namafile" class="form-control" placeholder="Tuliskan nama file" value="" required></input>
              </div>
              <div class="form-group">
                <label>File</label>
                <input class="form-control" type="file" name="file">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php $no=0; foreach($download->result() as $row): $no++; ?>
  <div class="row">
    <div class="modal fade" id="modal-editdownload<?=$row->id_file;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel"> EDIT</h4>
          </div>
          <form class="form-horizontal" action="<?php echo site_url('adminDownload/editDownload'); ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

              <input type="hidden" readonly value="<?=$row->id_file;?>" name="id_file" class="form-control" >
              
                <div class="form-group">
                  <label class="col-sm-2">Nama File</label>
                  <div class="col-sm-8">
                  <input class="form-control" name="namafile" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->namafile;?>" required>
                </div>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>  
  </div>
  <?php endforeach; ?>

</div>