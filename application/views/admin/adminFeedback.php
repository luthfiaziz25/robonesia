<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
          <div class="title_left">
             <h3>Feedback</h3>
             <h3><small>Pengaduan, kritik dan saran user</small></h3>
          </div>
    </div>

    <?php 
    $data=$this->session->flashdata('sukses');
    if($data!=""){ ?>
      <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
    <?php } ?>

    <?php 
    $data2=$this->session->flashdata('error');
    if($data2!=""){ ?>
      <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
    <?php } ?>
          
 
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header" style="line-height:70px;">
            <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
              <thead>
                <tr style="background: #fff;">
                    <th width="5"><center>No</th>
                    <th width="220"><center>Id User</th>
                    <th width="120"><center>Kategori</center></th>
                    <th><center>Isi</th>
                    <th><center>Tanggal</th>
                    <th width="110"><center>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($feedback->result() as $row): ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->id_user; ?></td>
                  <td><?php echo $row->kategori_feedback; ?></td>
                  <td><?php echo $row->isi_feedback; ?></td>
                  <td><?php echo $row->tanggal_feedback; ?></td>
                  <td align="center">
                    <a href="<?php echo site_url('adminFeedback/hapusFeedback/'.$row->id_saran); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>