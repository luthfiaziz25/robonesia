<div id="page-wrapper">
	<div class="page-title" style="margin-top: -20px;">
		<div class="title_left">
			<h3>Galeri Foto</h3>
			<h3><small>Klik untuk menambahkan/mengedit</small></h3>
		</div>
	</div>

	<?php 
		$data=$this->session->flashdata('sukses');
		if($data!=""){ ?>
			<div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
		<?php } ?>

		<?php 
		$data2=$this->session->flashdata('error');
		if($data2!=""){ ?>
			<div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
		<?php } ?>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="line-height:70px;">
						<a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahgalerifoto"><span class="fa fa-plus"></span> Tambah Foto</a>
						<div class="box-body" style="overflow: auto;">
							<table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
								<thead>
									<tr>
										<th width="10"><center>No</center></th>
										<th width="200"><center>Foto</center></th>
										<th width="200"><center>Title</center></th>
										<th width="150"><center>Posted</center></th>
										<th><center>Deskripsi Foto</center></th>
										<th width="90"><center>Views</center></th>
										<th width="90"><center>Download</center></th>
										<th width="110"><center>Aksi</center></th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; foreach($galerifoto->result() as $row): ?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td>
											<img width="200" src="<?php echo base_url(). 'assets/img/'.$row->filename; ?>" class="img-thumbnail">
										</td>
										<td><?php echo $row->title; ?></td>
										<td><?php echo date('d F Y', strtotime($row->created_at)); ?></td>
										<td><?php echo $row->deskripsi; ?></td>
										<td><?php echo $row->view_count; ?></td>
										<td><?php echo $row->download_count; ?></td>
										<td align="center">
											<a href="" data-toggle="modal" data-target="#modal-editgalerifoto<?=$row->id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
											<a href="<?php echo site_url('adminGaleriFoto/hapusGalerifoto/'.$row->id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modal-tambahgalerifoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
					<h4 class="modal-title" id="myModalLabel"> Tambah</h4>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('adminGaleriFoto/addGalerifoto'); ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="modal-body">
							<div class="form-group">
								<label>Foto</label>
								<input class="form-control" type="file" name="image">
							</div>
							<div class="form-group">
								<label>Title</label>
								<input class="form-control" name="title" type="text" placeholder="Title" value="" required>
							</div> 
							<input name="deskripsi" type="hidden" value="">
							<div class="form-group">
								<label>Deskripsi Foto</label>
								<input class="form-control" name="deskripsi" type="text" placeholder="Deskripsi foto" value="" required>
							</div>     
							<div class="modal-footer">
								<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div> 

	<?php $no=0; foreach($galerifoto->result() as $row): $no++; ?>
	<div class="row">
		<div class="modal fade" id="modal-editgalerifoto<?=$row->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header bg-primary">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
		        <h4 class="modal-title" id="myModalLabel"> EDIT</h4>
		      </div>
		      <form class="form-horizontal" action="<?php echo site_url('adminGaleriFoto/editGalerifoto'); ?>" method="post" enctype="multipart/form-data">
		        <div class="modal-body">

		          <input type="hidden" readonly value="<?=$row->id;?>" name="id" class="form-control" >
		          	<div class="form-group">
						<label class="col-sm-2"	>Foto</label>
						<div class="col-sm-8">
						<input class="form-control" type="file" name="image">
						</div>
					</div>
					<div class="form-group">
								<label class="col-sm-2">Title</label>
								<div class="col-sm-8">
								<input class="form-control" name="title" type="text" placeholder="Title" value="<?=$row->title;?>" required>
							</div>
							</div> 
		            <div class="form-group">
		              <label class="col-sm-2">Deskripsi Foto</label>
		              <div class="col-sm-8">
		              <input class="form-control" name="deskripsi" type="text" autocomplete="off" placeholder="Input Deskripsi" value="<?=$row->deskripsi;?>" required>
		       		  </div>
		            </div>
		            
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
		          <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
		        </div>
		      </form>
		    </div>
		  </div>
		</div>  
	</div>
	<?php endforeach; ?>
</div>