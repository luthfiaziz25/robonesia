<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
      <h3>Galeri Video</h3>
     <h3><small>Klik untuk menambahkan/mengedit</small></h3>
   </div>
  </div>

  <?php 
    $data=$this->session->flashdata('sukses');
    if($data!=""){ ?>
      <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
    <?php } ?>

    <?php 
    $data2=$this->session->flashdata('error');
    if($data2!=""){ ?>
      <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
    <?php } ?>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header" style="line-height:70px;">
            <a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahGalerivideo"><span class="fa fa-plus"></span> Tambah Video</a>
            <div class="box-body" style="overflow: auto;">
              <table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
                <thead>
                  <tr>
                    <th width="10"><center>No</th>
                    <th ><center>Judul</th>
                    <th width="400"><center> Link</th>
                    <th width="110"><center>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($galerivideo->result() as $row): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row->judul; ?></td>
                    <td><?php echo $row->link; ?></td>
                    <td align="center">
                      <a href="" data-toggle="modal" data-target="#modal-editgalerivideo<?=$row->id_video;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                      <a href="<?php echo site_url('adminGaleriVideo/hapusGalerivideo/'.$row->id_video); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>    
  </section>

  <div class="modal fade" id="modal-tambahGalerivideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary"> 
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
        </div>

        <form class="form-horizontal" action="<?php echo site_url('adminGaleriVideo/addGalerivideo'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body" style="margin: 15px">
            <div class="modal-body">
              <div class="form-group">
                <label>Judul</label>
                <input class="form-control" name="judul" type="text" placeholder="Berikan Judul Pada Video" value="" required>
              </div>    
              <div class="form-group">
                <label>Link</label>
                <input class="form-control" name="link" type="text" placeholder="http://. . . . ." value="" required>
              </div>    
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php $no=0; foreach($galerivideo->result() as $row): $no++; ?>
  <div class="row">
    <div class="modal fade" id="modal-editgalerivideo<?=$row->id_video;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel"> EDIT</h4>
          </div>
          <form class="form-horizontal" action="<?php echo site_url('adminGaleriVideo/editGalerivideo'); ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

              <input type="hidden" readonly value="<?=$row->id_video;?>" name="id_video" class="form-control" >
                <div class="form-group">
                  <label class="col-sm-2">Judul</label>
                  <div class="col-sm-8">
                  <input class="form-control" name="judul" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->judul;?>" required>
                </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2">Link</label>
                  <div class="col-sm-8">
                  <input class="form-control" name="link" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->link;?>" required>
                </div>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>  
  </div>
  <?php endforeach; ?>

</div>  