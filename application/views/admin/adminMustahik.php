<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
     <h3>Kelola Data Mustahik</h3>
     <h3><small>Klik untuk menambahkan/mengedit</small></h3>
   </div>
 </div>  

 <?php 
 $data=$this->session->flashdata('sukses');
 if($data!=""){ ?>
   <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
 <?php } ?>

 <?php 
 $data2=$this->session->flashdata('error');
 if($data2!=""){ ?>
  <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header" style="line-height:50px;">
          <a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahmustahik"><span class="fa fa-plus"></span> Tambah Mustahik</a>
          <div class="box-body" style="overflow: auto;">
            <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
              <thead>
                <tr style="background: #fff;">
                  <th width="10px"><center>No</center></th>
                  <th width="120px"><center>Foto</center></th>
                  <th width="180px"><center>NIK</center></th>
                  <th width="150px"><center>Nama</center></th>
                  <th width="80px"><center>Kelamin</center></th>
                  <th ><center>Alamat</center></th>
                  <th width="110px"><center>Aksi</center></th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($mustahik->result() as $row): ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td>
                    <img width="200" src="<?php echo base_url(). 'assets/img/'.$row->image; ?>" class="img-thumbnail" >
                  </td>
                  <td><?php echo $row->nik_mustahik; ?></td>
                  <td><?php echo $row->nama_mustahik; ?></td>
                  <td><?php echo $row->jk_mustahik; ?></td>
                  <td><?php echo $row->alamat_mustahik; ?></td>
                  <td align="center">
                    <a href="" data-toggle="modal" data-target="#modal-editmustahik<?=$row->id_mustahik;?>"" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="<?php echo site_url('adminMustahik/hapusMustahik/'.$row->id_mustahik); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div> 
</div>   
</section>

<div class="modal fade" id="modal-tambahmustahik" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
        <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
      </div>

      <form class="form-horizontal" action="<?php echo site_url('adminMustahik/addMustahik'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="modal-body">
            <div class="form-group">
              <label>Foto</label>
              <input class="form-control" type="file" name="image">
            </div>
            <div class="form-group">
              <label>NIK (Nomor Induk Kependudukan)</label>
              <input class="form-control" name="nik_mustahik" type="text" placeholder="Masukkan NIK (Nomor Induk Kependudukan)" value="" required>
            </div>
            <div class="form-group">
              <label>Nama</label>
              <input class="form-control" name="nama_mustahik" type="text" placeholder="Masukan nama lengkap" value="" required>
            </div>
            <div class="form-group">
              <label>Jenis Kelamin</label>
              <fieldset>
                <input type="radio" class="radiobtn1" name="jk_mustahik" id="rd1" value="Laki-laki"> <label for="rd1">Laki-laki</label>
                <input type="radio" class="radiobtn2" name="jk_mustahik" id="rd2" value="Perempuan"><label for="rd2">Perempuan</label>          
              </fieldset>
            </div>
            <div class="form-group">
              <label>Alamat</label>
              <input class="form-control" name="alamat_mustahik" type="text" placeholder="Masukkan alamat lengkap" value="" required>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $no=0; foreach($mustahik->result() as $row): $no++; ?>
<div class="row">
  <div class="modal fade" id="modal-editmustahik<?=$row->id_mustahik;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
          <h4 class="modal-title" id="myModalLabel"> EDIT</h4>
        </div>
        <form class="form-horizontal" action="<?php echo site_url('adminMustahik/editMustahik'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <input type="hidden" readonly value="<?=$row->id_mustahik;?>" name="id_mustahik" class="form-control" >
            <div class="form-group">
              <label class="col-sm-2" >Foto</label>
              <div class="col-sm-8">
                <input class="form-control" type="file" name="image">
              </div>
            </div>/
            <div class="form-group">
              <label class="col-sm-2">Nik</label>
              <div class="col-sm-8">
                <input class="form-control" name="nik_mustahik" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->nik_mustahik;?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">Nama</label>
              <div class="col-sm-8">
                <input class="form-control" name="nama_mustahik" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->nama_mustahik;?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">Jenis Kelamin</label>
              <div class="col-sm-8">
                <input class="form-control" name="jk_mustahik" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->jk_mustahik;?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">Alamat</label>
              <div class="col-sm-8">
                <input class="form-control" name="alamat_mustahik" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->alamat_mustahik;?>" >
              </div>
            </div>


            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>  
</div>
<?php endforeach; ?>
</div>