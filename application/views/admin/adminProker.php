<div id="page-wrapper">   
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
      <h3>Program Kerja</h3>
      <h3><small>Kelola program kerja UPZ MD</small></h3>
    </div>
  </div>

  <?php 
  $data=$this->session->flashdata('sukses');
  if($data!=""){ ?>
    <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
  <?php } ?>

  <?php 
  $data2=$this->session->flashdata('error');
  if($data2!=""){ ?>
    <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
  <?php } ?>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header" style="line-height:70px;">
            <a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahproker"><span class="fa fa-plus"></span> Tambah Progam Kerja</a>
            <div class="box-body" style="overflow: auto;">
              <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
                <thead>
                  <tr style="background: #fff;">
                    <th width="5"><center>No</center></th>
                    <th width="300"><center>Program Kerja</center></th>
                    <th width="200"><center>Dokumentasi Gambar</center></th>
                    <th width="150"><center>Waktu</center></th>
                    <th width="400"><center>Deskripsi Proker</center></th>
                    <th width="110"><center>Aksi</center></th>
                  </tr>
                </thead>

                <tbody>
                  <?php $no=1; foreach($proker->result() as $row): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row->namaproker; ?></td>
                    <td>
                      <img width="200" src="<?php echo base_url(). 'assets/img/'.$row->image; ?>" class="img-thumbnail">
                    </td>
                    <td><?php echo $row->waktu; ?></td>
                    <td><?php echo $row->desk_proker; ?></td>
                    <td align="center">
                      <a href="" data-toggle="modal" data-target="#modal-editproker<?=$row->id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                      <a href="<?php echo site_url('adminProker/hapusProker/'.$row->id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-tambahproker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
        <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
      </div>

      <form class="form-horizontal" action="<?php echo site_url('adminProker/addProker'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="modal-body">
            <input name="id" type="hidden" value="">
            <div class="form-group">
              <label>Program Kerja</label>
              <input class="form-control" name="namaproker" type="text" placeholder="Input Program Kerja" value="" required>
            </div>
            <div class="form-group">
              <label>Dokumentasi Gambar</label>
              <input class="form-control" type="file" name="image">
            </div>
            <div class="form-group">
              <label>Waktu</label>
              <input type="text" name="waktu" placeholder="Input waktu program kerja" value="" required>
            </div>
            <div class="form-group">
              <label>Deskripsi Program Kerja</label><br>
              <textarea  name="desk_proker" style="width: 100%" required placeholder="Deskripsikan program kerja"></textarea>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $no=0; foreach($proker->result() as $row): $no++; ?>
<div class="row">
  <div class="modal fade" id="modal-editproker<?=$row->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
          <h4 class="modal-title" id="myModalLabel"> EDIT</h4>
        </div>
        <form class="form-horizontal" action="<?php echo site_url('adminProker/editProker'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <input type="hidden" readonly value="<?=$row->id;?>" name="id" class="form-control" >

            <div class="form-group">
              <label class="col-sm-2">Program Kerja</label>
              <div class="col-sm-8">
                <input class="form-control" name="namaproker" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->namaproker;?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2" >Dokumentasi Gambar</label>
              <div class="col-sm-8">
                <input class="form-control" type="file" name="image">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">Waktu</label>
              <div class="col-sm-8">
                <input class="form-control" name="waktu" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->waktu;?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">Deskripsi Program Kerja</label>
              <div class="col-sm-8">
                <input class="form-control" name="desk_proker" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->desk_proker;?>" required>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>  
</div>
<?php endforeach; ?>

</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
    tinymce.init({
            selector: "textarea",
            plugins: [
                    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
            ],
    
            toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
    
            menubar: false,
            toolbar_items_size: 'small',
            image_advtab: true,
            style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
    
            templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
            ]
    });
    </script>
    <script>
      $(document).ready(function(){
        $("#myInput").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
    </script>