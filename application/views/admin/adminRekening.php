<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
     <h3>Kelola Data Rekening</h3>
     <h3><small>Klik untuk menambahkan/mengedit</small></h3>
   </div>
 </div>  

 <?php 
 $data=$this->session->flashdata('sukses');
 if($data!=""){ ?>
   <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
 <?php } ?>

 <?php 
 $data2=$this->session->flashdata('error');
 if($data2!=""){ ?>
  <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header" style="line-height:50px;">
          <a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahrekening"><span class="fa fa-plus"></span> Tambah Rekening</a>
          <div class="box-body" style="overflow: auto;">
            <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
              <thead>
                <tr style="background: #fff;">
                  <th width="10px"><center>No</center></th>
                  <th width="120px"><center>Logo Bank</center></th>
                  <th width="150px"><center>Bank</center></th>
                  <th width="150px"><center>Atas Nama Rekening</center></th>
                  <th width="80px"><center>No Rekening</center></th>
                  <th width="110px"><center>Aksi</center></th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($rekening->result() as $row): ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td>
                    <img width="200" src="<?php echo base_url(). 'assets/img/'.$row->image; ?>" class="img-thumbnail" >
                  </td>
                  <td><?php echo $row->bank; ?></td>
                  <td><?php echo $row->atasnama; ?></td>
                  <td><?php echo $row->no_rekening; ?></td>
                  <td align="center">
                    <a href="" data-toggle="modal" data-target="#modal-editrekening<?=$row->id_norek;?>"" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="<?php echo site_url('adminRekening/hapusRekening/'.$row->id_norek); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div> 
</div>   
</section>

<div class="modal fade" id="modal-tambahrekening" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
        <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
      </div>

      <form class="form-horizontal" action="<?php echo base_url().'adminRekening/addRekening'; ?>" method="post"  enctype="multipart/form-data">
        <div class="modal-body">
          <div class="modal-body">
            <div class="form-group">
              <label>Logo Bank</label>
              <input class="form-control" type="file" name="images">
            </div>
            <div class="form-group">
              <label>Bank</label>
              <input class="form-control" name="bank" type="text" placeholder="Masukan Nama Bank" value="" required>
            </div>
            <div class="form-group">
              <label>Atas Nama Rekening</label>
              <input class="form-control" name="atasnama" type="text" placeholder="Masukan nama lengkap" value="" required>
            </div>
            <div class="form-group">
              <label>No Rekening</label>
              <input class="form-control" name="no_rekening" type="text" placeholder="Masukan Nomor Rekening" value="" required>
            </div>
          
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $no=0; foreach($rekening->result() as $row): $no++; ?>
<div class="row">
  <div class="modal fade" id="modal-editrekening<?=$row->id_norek;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
          <h4 class="modal-title" id="myModalLabel"> EDIT</h4>
        </div>
        <form class="form-horizontal" action="<?php echo site_url('adminRekening/editRekening'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">

            <input type="hidden" readonly value="<?=$row->id_norek;?>" name="id_norek" class="form-control" >
            <div class="form-group">
              <label class="col-sm-2" >Foto</label>
              <div class="col-sm-8">
                <input class="form-control" type="file" name="image">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">Bank</label>
              <div class="col-sm-8">
                <input class="form-control" name="bank" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->bank;?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">Atas Nama Rekening</label>
              <div class="col-sm-8">
                <input class="form-control" name="atasnama" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->atasnama;?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2">No Rekening</label>
              <div class="col-sm-8">
                <input class="form-control" name="no_rekening" type="text" autocomplete="off" placeholder="Input judul Berita" value="<?=$row->no_rekening;?>">
              </div>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>  
</div>
<?php endforeach; ?>
</div>