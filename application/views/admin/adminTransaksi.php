<div id="page-wrapper">
	<div class="page-title" style="margin-top: -20px;">
		<div class="title_left">
			<br>
		 	<center><h3>Data Transaksi</h3></center>
		 	<br>
		 	<!-- <h3><small>Klik untuk menambahkan/mengedit</small></h3> -->
	 	</div>
 	</div>

	<!-- <?php 
		$data=$this->session->flashdata('sukses');
		if($data!=""){ ?>
			<div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
		<?php } ?>

		<?php 
		$data2=$this->session->flashdata('error');
		if($data2!=""){ ?>
			<div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
	<?php } ?> -->

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="line-height:70px;">
						<!-- <a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahtransaksi"><span class="fa fa-plus"></span> Tambah Transaksi</a> -->
						
						<div class="box-body" style="overflow: auto;">
							<table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
								<thead>
									<tr style="background: #fff;">
										<th width="5"><center>No</center></th>
										<th width="5"><center>Id Transaksi</center></th>
										<th width="150"><center>Id Muzakki</center></th>
										<th width="120px"><center>Jenis Zakat</center></th>
										<th width="110"><center>Nominal</center></th>
										<th width="180"><center>Metode Pembayaran</center></th>
										<th width="180"><center>Tanggal Transaksi</center></th>
										<th width="80"><center>Tanggal Bayar</center></th>
										<th width="80"><center>Status</center></th>
										<th width="80"><center>Bukti</center></th>
										<th width="80"><center>Action</center></th>
									</tr>
								</thead>

								<tbody>
									<?php $no=1; foreach($transaksi->result() as $row): ?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $row->id_transaksi; ?></td>
										<td><?php echo $row->id; ?></td>
										<td><?php echo $row->jenis_zakat; ?></td>
										<td><?php echo $row->jumlah_zakat; ?></td>
										<td><?php echo $row->metode_pembayaran; ?></td>
										<td><?php echo $row->tanggal; ?></td>
										<td><?php echo $row->tanggal_upload; ?></td>
										<td><?php echo $row->status; ?></td>
										<td><img height="100px" width="100px" src="<?=base_url().'assets/img/uploadbukti/'.$row->upload_bukti; ?>"></td>
										<form action="<?=base_url('AdminTransaksi/approve'); ?>" method="POST" enctype="multipart/form-data">
										<input type="hidden" name="id_transaksi" value="<?php echo $row->id_transaksi; ?>">
										<td ><button class="btn btn-primary btn-flat" type="submit">Approve</button></td>
										</form>
										<!-- <td align="center">
											<a href="" data-toggle="modal" data-target="#modal-edittransaksi<?=$row->id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-check"></i></a>
											<a href="<?php echo site_url('adminBerita/hapusTransaksi/'.$row->id_transaksi); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
										</td> -->
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>

							<a href="<?php echo site_url()?>laporanpdf"><button class="btn btn-info btn-flat">Cetak Laporan</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modal-tambahtransaksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
					<h4 class="modal-title" id="myModalLabel"> Tambah</h4>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('adminBerita/addBerita'); ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="modal-body">
							<input name="id" type="hidden" value="">
								<div class="form-group">
									<label>Jenis Transaksi</label>
									<fieldset>
                    <input type="radio" class="radiobtn1" name="jenis_transaksi" id="rd1" value="Zakat"> <label for="rd1">Laki-laki</label>
                    <input type="radio" class="radiobtn2" name="jenis_transaksi" id="rd2" value="Perempuan"><label for="rd2">Perempuan</label>          
                </fieldset>
								</div>
								<div class="form-group">
									<label>Nominal</label>
									<input class="form-control" type="text" name="nominal" placeholder="Masukan jumlah transaksi" value="" required>
								</div>
								<div class="form-group">
									<label>Nama Muzakki</label>
									<input class="form-control" name="nama" type="text" placeholder="Penulis berita" value="" required>
									</div>
								<div class="form-group">
									<label>Email</label><br>
									<textarea  name="email" style="width: 100%" required placeholder="Tuliskan berita terbaru"></textarea>
								</div>
								<div class="form-group">
									<label>Username</label><br>
									<textarea  name="usrername" style="width: 100%" required placeholder="Tuliskan berita terbaru"></textarea>
								</div>
								<div class="form-group">
									<label>Status Transaksi</label><br>
									<textarea  name="status_transaksi" style="width: 100%" required placeholder="Tuliskan berita terbaru"></textarea>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
								</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>  
</div>
