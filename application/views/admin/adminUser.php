<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
          <div class="title_left">
             <h3>Pengguna</h3>
             <h3><small>Kelola data acount user</small></h3>
           </div>
      </div>
        <div class="box-header" style="margin-bottom: 5px;">
              <a class="btn btn-warning  btn-flat" data-toggle="modal" data-target="#modal-tambahuser"><span class="fa fa-plus"></span> Tambah User</a>
             </div>
          <div class="box-body">
              <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
                <thead>
                <tr>
                    <th width="5">No</th>
                    <th width="300"><center>Nama</th>
                    <th><center>Username</th>
                    <th><center>Password</th>
                    <th width="120"><center>Aksi</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
              </table>
            </div>

            <div class="modal fade" id="modal-tambahuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary"> 
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo site_url('adminUser/insertUser'); ?>" method="post" enctype="multipart/form-data">
                    
                    <div class="modal-body" style="margin: 15px">
                             <div class="form-group">
                                <label    class="col-sm-2 control-label">Nama</label>
                                <div class="col-sm-8">
                                  <input type="text" name="nama" class="form-control" autocomplete="off" placeholder="Masukan Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label    class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-8">
                                  <input type="text" name="username" class="form-control" autocomplete="off" placeholder="Masukan Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label    class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-8">
                                  <input type="text" name="password" class="form-control" autocomplete="off" placeholder="Masukan Nomer Telepon">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
</div>