<!DOCTYPE html>
<html>
<head>
	<title>login</title>
	<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
	<link href="<?php echo base_url() ?>assets/img/upz.png" rel="icon" type="image/png">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js">

	<style type="text/css">

		body, html {
			height: 100%;
			font-family: futura md bt;
			margin: 0;
			padding: 0;
			background: url('../../assets/img/upz1.png'); 
			/*background-color: black;*/
			border: 2px;
		}
		*{
			box-sizing: border-box;
			-webkit-box-sizing:border-box;
			-moz-box-sizing:border-box;
		}
		h2{
			font-style: ;
			font-size: 25px;
			margin: 0;
			padding: 20px;
		}
		h1{
			text-align: center;
		}
		.wrap{
			background: url('../../assets/img/upz.png');
			min-height: 80%;
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
		}
		.container{
			position: fixed;
			max-width: 350px;
			top: 50%;
			left: 50%;
			margin-top: -150px;
			margin-left: -150px;
			padding: 15px;
			background:	#ffbf00;
			border-radius: 15px;
			opacity: 0.9;

		}
		.container img{
			width: 100px;
			height: 100px;
			margin-left: 30%;
		}
		input[type=text], input[type=Password]{
			width: 100%;
			padding:13px;
			margin: 5px 0 0 0;
			border: none;
			background: #f1f1f1;
			font-size: 15px;
		}
		label{
			margin-bottom: 0;
			margin-top: 5px;
			font-style: bold;
		}
		.titlelogin{
			margin-top: 100px;
			font-size: 30px;
			color: white;
		}
		/*.btn{
			background: #4caf50;
			color: #fff;
			padding: 13px 20px;
			font-size: 16px;
			font-weight: bold;
			width: 100%
			border-radius:10px;
		}
		.btn:hover {
			background: salmon;

		}*/

		a{
			text-decoration: none;
		}
		a:hover {
			text-decoration: underline;
		}	
	</style>
</head>
<br>
<center><h2 class="titlelogin"style="color:green";>Masuk Administrator UPZ Manajemen Dakwah</h2></center>
<body>
	<div class="">  
		
		<form method="POST" action="<?php echo base_url() ?>loginadmin/aksiLoginadmin">
			<div class="container">
				<?php if(isset($error)) { echo $error; }; ?>
				<img src="<?php echo base_url();?>assets/img/upz.png" class="custom-logo" alt="El-Haqq Tour &amp; Travel">
				<!-- <h5 align="center">Masuk Administrator <br> PPM Assuruur</h5>
 -->			<br>	
 				<div class="form-group">
 				<label><b>Username</b></label>
				<input type="text" name="username" placeholder="Masukan Username" class="form-control">
				<?php echo form_error('username'); ?>
				</div>
				<br>
				<div class="form-group">
				<label><b>Password</b></label>
				<input type="password" name="password" placeholder="Masukan Password" class="form-control">
				<?php echo form_error('password'); ?>
				</div>
				<br>
				<br>
				<br>
				<button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
			</div>
		</form>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		$('.form-checkbox').click(function(){
			if ($(this).is(':checked')) {
				$('.from-password').attr('type','text');
			}else{
				$('.from-password').attr('type','password');
			}
	});
});		
</script>
</html>