<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Robonesia</title>
    <!-- <link href="<?php echo base_url() ?>assets/img/upz.png" rel="icon" type="image/png"> -->

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url() ?>assets/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url() ?>assets/admin/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/admin/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url() ?>assets/admin/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url() ?>assets/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper" >
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color:rgba(255,255,255,1);  ">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url()?>Dashboard">Admin UPZ MD</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <!-- /.dropdown -->
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw" style="color: #a67c00"></i> <i class="fa fa-caret-down" style="color: #a67c00"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" style="color: #a67c00"><i class="fa fa-user fa-fw" style="color: #a67c00"></i> Admin Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url() ?>loginadmin/logout" style="color: red"><i class="fa fa-sign-out fa-fw" style="color: red"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation"  style=" background-color:rgba(255,255,255,1);">
                <div class="sidebar-nav navbar-collapse">   
                    <ul class="nav sidemenu" id="side-menu">
                        <!-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" style="color: #a67c00">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </li> -->
                        <li>
                            <a href="<?php echo base_url('c_admin');?>" style="color:    #a67c00"><i style="color:   #a67c00" class="fa fa-home fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="#" style="color:   #a67c00"><i class="fa fa-edit" style="color:   #a67c00"></i> Tentang Robonesia<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('c_admin/profile');?>" style="color:   #a67c00"><i class=" fa fa-user-secret" aria-hidden="true" style="color:   #a67c00"> Profile Perusahaan</i></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('c_admin/daftarkaryawan');?>" style="color:   #a67c00"><i class=" fa fa-group" aria-hidden="true" style="color:   #a67c00"> Daftar Karyawan</i></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('c_admin/daftarmember');?>" style="color:   #a67c00"><i class=" fa fa-group" aria-hidden="true" style="color:   #a67c00"> Daftar Member</i></a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#" style="color:   #a67c00"><i class="fa fa-folder-open" style="color:   #a67c00"></i> Konten<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('c_admin/artikel');?>" style="color:    #a67c00"><i class="fa fa-file" style="color:   #a67c00"></i> Artikel</a>
                                </li>
                                <li>
                                    <a href="#" style="color:   #a67c00"><i class="fa fa-picture" style="color:  #a67c00"></i> Galery<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_url()?>adminGaleriFoto" style="color:    #a67c00"><i class=" fa fa-photo" aria-hidden="true" style="color:   #a67c00"> Foto</i></a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url()?>adminGaleriVideo" style="color:   #a67c00"><i class=" fa fa-file-video-o" aria-hidden="true" style="color:   #a67c00"> Video</i></a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?php echo base_url('adminFeedback');?>" style="color:     #a67c00"><i class="fa fa-envelope fa-fw" style="color:   #a67c00"></i> Saran</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>adminUser" style="color:  #a67c00"><i class="fa fa-user" style="color:  #a67c00"></i> User Acount</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
        </nav>

    </div>

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/admin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/dist/js/sb-admin-2.js"></script>


<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<script src="<?php echo base_url() ?>assets/css/jasny-bootstrap.min.js"></script>
<!-- Page script -->

</body>

</html>
