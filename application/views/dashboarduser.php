<!DOCTYPE html>
<html>
<head>
  <title> Dashboard User</title>
  <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">

  <div class="container">
  <div class="row">
    <div class="col-sm-3">
    </div>
    
    <div class="col-sm-6">
       
      <div class="bguser">
        <div class="dbuser"><br>
        <h4 class="titledbuser">Profil Muzakki</h4>
        <?php echo form_open_multipart('overview/add'); ?>
        <img class="imgprofil"><br>
        <div class="form-group">
        <input class="btn btn-default btnmargin" type="file" name="userfile"/>
        </div>
        <input class="inputdbuser " type="text"  name="nama" placeholder="Nama" >
        <br>
        <input class="inputdbuser " type="password"  name="password" placeholder="Password" >
        <br>
        <input class="inputdbuser " type="text"  name="username"  placeholder="Username" >
        <input class="inputdbuser " type="email" name="email"  placeholder="email" >
        <br>
        <input class="inputdbuser " type="number"  name="nomor_hp" placeholder="Nomor Hp" >
        <br>
        <small class="mutetext">*Boleh diisi Dosen/Mahasiswa</small><br>
        <input class="inputdbuser " type="text"  name="pekerjaan" placeholder="pekerjaan" >
        <br>
        <textarea class="txtprofile " name="alamat"  placeholder="Alamat"></textarea>
        <br>
        <button class="btnprofil" type="submit"  name="submit">Save</button>
        <br>
      <?php echo form_close() ?>
        </div>  
      </div>
      </div>
      <br>
    <div class="col-sm-3">
    </div>
  </div>
</div>
</body>
</html>