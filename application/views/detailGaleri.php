<!DOCTYPE html>
<html>
<head>
	<title>Detail Galeri</title>
	<?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
	<br>
	<br>
	<br>
	<div class="container">
	<h2><?php  echo $row->title; ?></h2>
    	<div class="row">
    		<div class="col-md-8 col-xs-12">
			      <img src="<?php echo base_url() ?>assets/img/<?php echo $row->filename; ?>" alt="Image 1" class="img-thumbnail">			  
    		</div>
    		<div class="col-md-4 col-xs-12">
    			<ul class="list-group">
				  <li class="list-group-item">
				    <i class="glyphicon glyphicon-eye-open"></i> <?php echo $row->view_count + 1?> View
				  </li>
				  <li class="list-group-item">
				    <i class="glyphicon glyphicon-download"></i> <?php echo $row->download_count ?> Download
				  </li>
				</ul>

				<?php echo form_open("galeri/download"); ?>
					<input type="hidden" name="image_id" id="image_id" value="<?php echo $row->id ?>">
					<button type="submit" href='#' class="btn btn-lg btn-block btn-primary">Download</button>
				<?php echo form_close();?>
    		</div>
    	</div>
    </div>
    <br>
	<br>
	<br>
</body>
</html>
