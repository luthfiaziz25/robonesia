
<!DOCTYPE html>
<html>
<head>
  <title>Berita</title>
  <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">
  <br>
  <div class="container bgartikel">
    <div class="article-dual-coloum">
      <div class="row">
        <div class="row alignartikel">
          <div class="col-md-8 col-lg-8 marginartikel">
            <div class="row">
               <?php foreach ($proker->result() as $key): ?>
              <div class="col-md-12 itemcenter">
                <img class="bannerartikel" src="<?php echo base_url()?>assets/img/<?php echo $key->image; ?>">
              </div>
            </div>
            <hr>
          
            <div class="col-sm-12 item">
                <h3><a href="#"><?php echo $key->namaproker;?></a></h3>
                 <small>Pelaksanaan : <?php echo $key->waktu; ?></small>
                <p class="description"><?php echo $key->desk_proker;?></p></div>
            <?php endforeach ?>
            <hr>
            <br>
            <br>
             <div class="col-md-12">
          <?php 
                            function youtube($url){
                              $link=str_replace('http://www.youtube.com/watch?v=', '', $url);
                              $link=str_replace('https://www.youtube.com/watch?v=', '', $link);
                              $data='<object width="700px" height="500px" data="http://www.youtube.com/v/'.$link.'" type="application/x-shockwave-flash">
                              <param name="src" value="http://www.youtube.com/v/'.$link.'" />
                              </object>';
                              return $data;
                            }
                            ?>      
      
            <div id="youtube" class="card" style=" margin-top: 10px; width: 700px; height: 500px; text-align: center; background-color:rgba(255,255,255,0.9) ;">
              <strong><?php foreach ($video->result() as $key): ?></strong>
              <?php echo $key->judul;?>
              <?php echo youtube("$key->link") ?>
              <?php endforeach ?>
            </div>
            <hr>
            <br>
        </div>
        <div class="projects-horizontal">
        <div class="container bgartikelsmall">
          <center><h4>Berita Terpopuler</h4></center>
            <div class="intro">
                <p class="text-center"></p>
            </div>
            <div class="row projects">
               <?php foreach ($berita->result() as $key): ?>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'overview/detailpengabdian'; ?>"><img class="img-fluid imghoverpost thumbproker" src="<?php echo base_url()?>assets/img/<?php echo $key->image; ?>"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url('overview/detailberita/'. $key->id)?>"><?php echo $key->judul;?></a></h3>
                            <small>Publish : <?php echo $key->posted; ?></small>
                            <p class="description"><?php echo $key->views;?></p>
                        </div>
                    </div>
                </div>
                  <?php endforeach ?>
            </div>
        </div>
    </div>
        </div>
        <div class="col-md-9 col-lg-3 marginside">
          <div class="toc borderside">
            <aside>
              <div class="cardfansartikel" style="background-color:rgba(255,255,255,0.9) ;">
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

                <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
              </div>

               <p  class="borderleftartikelred">Program Kerja Terbaru</p>
              <?php foreach ($prokernew->result() as $key): ?>
              <li class="borderleftgambar">
                <a class="sidenews" href=""><img class="sideimg" src="<?php echo base_url()?>assets/img/<?php echo $key->image; ?>"></a><a class="sidetitle" href="<?php echo base_url('overview/detailberita/'. $key->id)?>"><?php echo $key->namaproker;?></a></li>
              <?php endforeach ?>

                      <p class="borderleftorange">Tentang Zakat</p>                      
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>
                    </aside>
                  </div>
                  <br>
                  <br>
                  <br>
                </div>
              </div>
              <!-- end sidebar -->
            </div>
          </div>
        </div>
        <br>
        <!-- end wrapper -->

      </body>
      </html>
<!--     <div class="article-clean">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">
                    <div class="intro">
                        <h1 class="text-center">Judul Artikel</h1>
                        <p class="text-center"><span class="by">by</span> <a href="#">Author Name</a><span class="date">Sept 8th, 2016 </span></p><img class="img-fluid" src="<?php echo base_url()?>assets/img/desk.jpg"></div>
                    <div class="text">
                        <p>Sed lobortis mi. Suspendisse vel placerat ligula. <span style="text-decoration: underline;">Vivamus</span> ac sem lac. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in
                            justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
                        <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac lacus. <strong>Ut vehicula rhoncus</strong> elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit <em>pulvinar dict</em> vel in justo. Vestibulum
                            ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
                        <h2>Aliquam In Arcu </h2>
                        <p>Suspendisse vel placerat ligula. Vivamus ac sem lac. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                            posuere cubilia Curae.</p>
                        <figure><img class="figure-img" src="<?php echo base_url()?>assets/img/beach.jpg">
                            <figcaption>Caption</figcaption>
                        </figure>
                        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Suspendisse vel placerat ligula. Vivamus ac sem lac. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit
                            pulvinar dictum vel in justo.</p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->