<!DOCTYPE html>
<html>
<head>
  <title>Zakat/Donasi</title>
    <?php $this->load->view("user/_partials/head.php")?>
    <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>

<!--Container-->
      <br>
      <main role="main" class="container">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <form action="<?=base_url('Donasi/insert_zakat'); ?>" method="POST">
          <?php foreach($profilku as $user) { ?>
          <h2 class="border-bottom border-gray pb-2 mb-0" align="center" >Bayar Zakat</h2>
          <div class="row">
            <input type="hidden" name="id" value="<?php echo $user->id; ?>">
            <div class="col-md-5 mb-3">
                <label for="country">Pilih Type Transaksi</label>
                <select class="custom-select d-block w-100" id="typezakat" required name="type_zakat">
                  <option value="">Pilih Transaksi</option>
                  <optgroup label="Zakat">
                    <option value="Zakat Profesi">Zakat Profesi</option>
                    <option value="Zakat Maal">Zakat Maal</option>
                    <option value="Zakat Surat Berharga">Zakat Surat Berharga</option>
                    <option value="Zakat Perusahaan">Zakat Perusahaan</option>
                    <option value="Zakat Fitrah">Zakat Fitrah</option>
                  </optgroup>
                  <optgroup label="--------------------">
                    <option value="Infaq/Shodaqoh">Infaq/Shodaqoh</option>
                  </optgroup>
                </select>
                <div class="invalid-feedback">
                  Mohon Pilih Salah Satu Type Zakat
                </div>
              </div>
            </div>
            <hr class="mb-4">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="Nama">Nama Donatur/Muzakki</label>
                <input value="<?php echo $user->nama; ?>" type="text" class="form-control" name="nama_donatur" placeholder="Nama" required readonly>
                <div class="invalid-feedback">
                  Penghasilan Tidak Boleh Kosong
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="uangzakat">Jumlah Uang Zakat/Shodaqoh</label>
                <input type="number-format" id="rupiah" class="form-control" name="jumlahuang" placeholder="Rp." required>
                <div class="invalid-feedback">
                  Penghasilan Tidak Boleh Kosong
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="email">Email</label>
                <input type="text" class="form-control" value="<?php echo $user->email; ?>" name="email" placeholder="Email" required readonly>
                <div class="invalid-feedback">
                  Penghasilan Tidak Boleh Kosong
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="pilihbank">Metode Pembayaran</label>
                <select class="custom-select d-block w-100" id="pilihbank" required name="metode_pembayaran">
                  <option value="">Choose...</option>
                  <optgroup label="Transfer Bank">
                    <option value="BRI">Bank BRI</option>
                    <option value="BRI Syariah">Bank BRI Syariah</option>
                    <option value="BNI">Bank BNI</option>
                    <option value="BNI Syariah">Bank BNI Syariah</option>
                    <option value="BCA">BCA</option>
                    <option value="BTN">BTN</option>
                    <option value="Mandiri">Mandiri</option>
                    <option value="Mandiri Syariah">Mandiri Syariah</option>
                    <option value="Mega">Bank Mega</option>
                    <option value="Danamon">Danamon</option>
                  </optgroup>
                </select>
                <div class="invalid-feedback">
                  Silahkan Pilih Bulan Terlebih Dahulu
                </div>
              </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Selanjutnya</button>
            <?php } ?>
          </form>
        </div>
    </main>
</body>
</html>