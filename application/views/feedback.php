<!DOCTYPE html>
<html>
<head>
  <title>Saran dan Masukan</title>
  <?php $this->load->view("user/_partials/head.php")?>
    <?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">
  <div class="container">
  <div class="row">
    <div class="col-sm-3">
      <ul class="dashleftuser">
        <br>
      <div class="cardfb">
        <li><p class="ahrefjudul"><strong>Menu</strong></p></li>
        <hr>
        <li><a class="ahref activepage" href="<?php echo base_url()?>dashboarduser/profilku">Profil Muzzaki</a></li>
            <!-- <li><a class="ahref" href="<?php echo base_url()?>dashboarduser/setting">Setting</a></li> -->
            <li><a class="ahref" href="" data-toggle="modal" data-target='#modal-setting'>Setting</a></li>
            <li><a class="ahref" href="<?php echo base_url()?>dashboarduser/tampilzakatku">Zakatku</a></li>
            <li><a class="ahref" href="<?php echo base_url()?>dashboarduser/saran">Feedback</a></li>
        <br>
      </div>
    </ul>
    <aside>
                    <div class="col-md-4" >
                  <div class="cardfansuser" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
    </div>
    <div class="col-sm-6">
      <form action="<?php echo base_url() ?>dashboarduser/kirimfeedback" method="POST">
      <select class="slct" name="kategori">
        <option class="slct" value="">Kategori</option>
        <option class="slct" value="pengaduan">Pengaduan</option>
        <option class="slct" value="kritik">Kritik</option>
        <option class="slct" value="saran">Saran</option>
      </select>
      <textarea class="formtext" name="isifeedback">
        
      </textarea>
      <button type="submit" class="btnfeed"><strong>Submit</strong></button>
    </form>
    </div>
    <div class="col-sm-3">
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-setting" tabindex="-1" role="doalog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="head-shape bg-head">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Ganti Password</h4>
          </div>

          <form class="form-horizontal" action="<?php echo site_url('dashboarduser/gantipass'); ?>" method="POST">
          <div class="modal-body">
            <div class="modal-body">
                <div class="form-group">
                  <input class="form-control" type="password" name="password" placeholder="password lama" required>
                </div>
                <div class="form-group">
                  <input class="form-control" type="password" name="newpassword" placeholder="New Password" required>
                </div>
                <div class="form-group">
                  <input class="form-control" type="password" name="renewpassword" placeholder="Re-New Password" required>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary btn-flat" id="simpan">Save</button>
                </div>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>

</body>
<script type="text/javascript" src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
    tinymce.init({
            selector: "textarea",
            plugins: [
                    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
            ],
    
            toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
    
            menubar: false,
            toolbar_items_size: 'small',
            image_advtab: true,
            style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
    
            templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
            ]
    });
    </script>
    <script>
      $(document).ready(function(){
        $("#myInput").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
    </script>

</html>