<!DOCTYPE html>
 <html>
 <head>
   <title>Galeri</title>
   <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
 </head>
 <body>
  <nav class="navbar navbar header-blue">
      <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" style="text-align: center; color: white;"> KUMPULAN FOTO KEGIATAN</a>
        </div>  
          <div  id="bs-example-navbar-collapse-1">          
            <ul class="nav navbar-nav navbar-right"">
              <li><a style="text-decoration: none; color: white; hover:red;" href="<?php echo site_url("galeri/latest")?>">Latest Update</a></li>
              <li><a style="text-decoration: none; color: white; hover:red;" href="<?php echo site_url("galeri/populer")?>">Popular</a></li>              
          </ul>
        </div>
              
      </div>
    </nav>
  <div class="container">
    <div class="row" style="padding: 10px;">
      <?php foreach ($query->result() as $row): ?>
         <div class="col-lg-3 col-md-4 col-sm-6" style="padding: 5px;">
          <a href="<?php echo site_url("galeri/detailGaleri/" . $row->id) ?>">

            <?php $thumbnail = empty($row->thumbnail) ? $row->filename : $row->thumbnail; ?>

            <img src="<?php echo base_url() ?>assets/img/<?php echo $thumbnail; ?>" alt="" class="img-thumbnail imgthumbnail">
            <div class="thumbnail-caption">
           <h8><center><a href="<?php echo site_url("galeri/detailGaleri/" . $row->id) ?>"><?php echo $row->title; ?></a></center></h8>
         </div>
     </div>

    <?php endforeach;?>
    
    </div>
</div>


    <nav class="text-center" >
      <?php echo $links; ?>
    </nav>
 </body>
 </html>

