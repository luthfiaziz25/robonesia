<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
</head>
<?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
<body>
    <!--Crausel-->
    <div class="carousel slide" data-ride="carousel" id="carousel-1 margin">
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active"><img class="slide" src="<?php echo base_url()?>assets/img/banner1.jpg" alt="Slide Image"></div>
            <div class="carousel-item"><img class="slide" src="<?php echo base_url()?>assets/img/banner2.jpg" alt="Slide Image"></div>
            <div class="carousel-item"><img class="slide" src="<?php echo base_url()?>assets/img/banner3.png" alt="Slide Image"></div>
        </div>
        <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
        <ol
        class="carousel-indicators">
        <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-1" data-slide-to="1"></li>
        <li data-target="#carousel-1" data-slide-to="2"></li>
    </ol>
</div>
<!-- <div class="carousel slide" data-ride="carousel" id="carousel-1">
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <div class="jumbotron hero-nature carousel-hero" style="background-image:url('../../assets/img/banner1.jpg'); ">
                    
                </div>
            </div>
            <div class="carousel-item">
                <div class="jumbotron hero-photography carousel-hero" style="background-image:url('../../assets/img/banner2.jpg'); ">
                </div>
            </div>
            <div class="carousel-item">
                <div class="jumbotron hero-technology carousel-hero" style="background-image:url('../../assets/img/banner3.jpg');">
                </div>
            </div>
        </div>
        <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><i class="fa fa-chevron-left"></i><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next"><i class="fa fa-chevron-right"></i><span class="sr-only">Next</span></a></div>
        <ol
            class="carousel-indicators">
            <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-1" data-slide-to="1"></li>
            <li data-target="#carousel-1" data-slide-to="2"></li>
            <li data-target="#carousel-1" data-slide-to="3"></li>
            <li data-target="#carousel-1" data-slide-to="4"></li>
            </ol>
        </div> -->
<!-- Maps -->
<div id="map" style="width: 100%; height: 500px;"></div> 
<script type="text/javascript">
  
//              menentukan koordinat titik tengah peta
var myLatlng = new google.maps.LatLng(-6.9271274,107.7245824);

//              pengaturan zoom dan titik tengah peta
var myOptions = {
  zoom: 13,
  center: myLatlng
};

//              menampilkan output pada element
var map = new google.maps.Map(document.getElementById("map"), myOptions);

//              menambahkan marker
var marker = new google.maps.Marker({
 position: myLatlng,
 map: map,
 title:"UIN SUNAN GUNUNG DJATI"
});
</script>
<!-- Berita -->
<div class="article-list bgproker">
    <div class="container">
        <div class="intro"><br>
            <h2 class="text-center">Berita Terbaru</h2>
        </div>
        <hr>
        <div class="row articles">
            <?php foreach ($berita->result() as $key): ?>
            <div class="col-sm-6 col-md-4 item"><a href="<?php echo base_url('overview/detailberita/'. $key->id)?>"><img class="img-fluid imghoverpost imgthumbnail" src="<?php echo base_url() ?>assets/img/<?php echo $key->image; ?>"</a>
                <h3 class="name"><a class="judulartikel judulpostkecil" href="<?php echo base_url('overview/detailberita/'. $key->id)?>"><?php echo $key->judul;?></a></h3>
                <i class="fa fa-user" aria-hidden="true" > : <?php echo $key->penulis; ?></i><br>
                <small>Posted : <?php echo $key->posted; ?></small>
                <p class="description"></p></div>
            <?php endforeach ?>
            </div>
        </div>
        <div class="container">
            <br>
            <p class="more"><a class="more" href="<?php echo base_url().'overview/lebihbanyakberita'; ?>"><strong>Tampilkan Lebih Banyak</strong></a></p>
        </div>
        <br>
    </div>
    <!-- Dashboard -->
    <!-- <div class="imgartikel">
        <br>
    <div class="container">
    <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $countMuzakki?></div>
                                    <div>Muzakki</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url() ?>adminMuzakki">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-envelope fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $countSaran?></div>
                                    <div>Saran dan Masukan</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url() ?>adminFeedback">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $count?></div>
                                    <div>Transaksi Zakat</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url() ?>adminTransaksi">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-photo fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $countGaleri?></div>
                                    <div>Foto Galeri</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url() ?>adminGaleriFoto">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div> -->
        </div>
    <!-- Ahir Berita -->
    <div class="projects-horizontal">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Bentuk Pengabdian</h2>
                <p class="text-center"></p>
            </div>
            <div class="row projects">
            <?php foreach ($proker->result() as $key): ?>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url('overview/detailpengabdian/'. $key->id)?>"><img class="img-fluid imghoverpost thumbproker" src="<?php echo base_url()?>assets/img/<?php echo $key->image; ?>"></a></div>
                        <div class="col-sm-6">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url('overview/detailpengabdian/'. $key->id)?>"><?php echo $key->namaproker;?></a></h3>
                            <small>Pelaksanaan : <?php echo $key->waktu; ?></small>
                            <p class="description"><?php echo $key->desk_proker;?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
            <div class="container">
                <p class="more"><a class="more" href="<?php echo base_url().'overview/lebihbanyakprogram'; ?>"><strong>Tampilkan Lebih Banyak</strong></a></p>
            </div>
        </div>
    </div>
    <div class="team-boxed">
        <div class="container">
            <div class="intro"></div>
            <h2 class="text-center">Struktur Lembaga</h2>
            <div class="row people">
                <div class="col-md-6 col-lg-4 item">
                    <div class="box"><img class="rounded-circle" src="<?php echo base_url()?>assets/img/user.jpg">
                        <h3 class="nama">H. ARIF RAHMAN,S.Ag. M.Pd.</h3>
                        <p class="title">Penasehat</p>
                        <p class="description"></p>
                        <div class="social">
                            <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="fa fa-facebook-official"></i></a>
                            <a href="http://www.twitter.com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="http://www.instagram.com" rel="nofollow" target="_blank"><i class="fa fa-instagram"></i></a></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 item">
                        <div class="box"><img class="rounded-circle" src="<?php echo base_url()?>assets/img/user.jpg">
                            <h3 class="nama">MUKTI AHMAD RAHARJA</h3>
                            <p class="title">Ketua</p>
                            <p class="description"></p>
                            <div class="social">
                                <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="fa fa-facebook-official"></i></a>
                                <a href="http://www.twitter.com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="http://www.instagram.com" rel="nofollow" target="_blank"><i class="fa fa-instagram"></i></a></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 item">
                            <div class="box"><img class="rounded-circle" src="<?php echo base_url()?>assets/img/user.jpg">
                                <h3 class="nama">H. ASEP IWAN SETIAWAN, S.Sos. M.Ag.</h3>
                                <p class="title">Pembina</p>
                                <p class="description"></p>
                                <div class="social">
                                    <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="fa fa-facebook-official"></i></a>
                                    <a href="http://www.twitter.com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="http://www.instagram.com" rel="nofollow" target="_blank"><i class="fa fa-instagram"></i></a></div>
                                </div>
                            </div>
                            <div class="container">
                                <p class="morestruktur"><strong><a class="morestruktur" href="<?php echo base_url().'overview/struktur'; ?>">Struktur Lembaga</a></strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
            </html>