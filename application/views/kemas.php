    
<!DOCTYPE html>
<html>
<head>
  <title>Kalkulator Zakat Emas</title>
  <?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
 <?php 
 if(isset($_POST['hitung'])){
  $bil1 = $_POST['bil1'];
  $bil2 = $_POST['bil2'];

  $hasil=(($bil1*$bil2)*(2.5/100));
  $hasil1=$hasil/12;
}
?>


<div class="container bgartikel">
  <form method="post" action="<?php echo base_url().'kalkulator/kemas'; ?>">
    <center><h3>Kalkulator Zakat Emas</h3></center>
    <div class="row">
      <div class="col-md-6 mb-3">
        <label for="penghasilanbulanan">Harga Emas</label>
        <input type="text" name="bil1" class="form-control" autocomplete="off" placeholder="Harga Emas Saat Ini">
        <div class="invalid-feedback">
          Penghasilan Tidak Boleh Kosong
        </div>
      </div>
      <div class="col-md-6 mb-3">
        <label for="penghasilantambahan">Jumlah Emas</label>
        <input type="text" name="bil2" class="form-control" autocomplete="off" placeholder="Jumlah Emas (gram)">
        <small class="text-muted">*jika tidak ada boleh dikosongkan</small> 
      </div>
    </div>
    <button class="btn btn-primary btn-lg btn-block" value="Hitung" name="hitung" type="submit">Hitung</button>
  </form>
  <div class="row">
    <div class="col-md-6">
      <h5>Zakat Per Tahun</h5>
      <?php if(isset($_POST['hitung'])){ ?>
        <input type="text" value="<?php echo $hasil;?>" class="form-control">
      <?php }else{ ?>
        <input type="text" value="0" class="form-control">
      <?php } ?>
    </div>
    <div class="col-md-6">
      <h5>Zakat Per Bulan</h5>
      <?php if(isset($_POST['hitung'])){ ?>
        <input type="text" value="<?php echo $hasil1;?>" class="form-control">
      <?php }else{ ?>
        <input type="text" value="0" class="form-control">
      <?php } ?>
    </div>
  </div>
  <hr>
</div>
</body>
</html>