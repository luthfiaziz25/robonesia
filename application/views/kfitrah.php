    
<!DOCTYPE html>
<html>
<head>
  <title>Kalkulator Zakat Fitrah</title>
</head>
<body>
 <?php 
 if(isset($_POST['hitung'])){
  $bil1 = $_POST['bil1'];
  $bil2 = $_POST['bil2'];

  $hasil=(($bil1*$bil2)*2.5);
}
?>


<div class="container">
  <form method="post" action="<?php echo base_url().'kalkulator/kfitrah'; ?>">
    <center><h3>Kalkulator Zakat Fitrah</h3></center>
    <div class="row">
      <div class="col-md-6 mb-3">
        <label for="penghasilanbulanan">Harga Beras Saat Ini</label>
        <input type="text" name="bil1" value="0" class="form-control" autocomplete="off" placeholder="Harga Beras">
        <small class="text-muted">*Harga Beras Per Kilo</small>
        <div class="invalid-feedback">
          Penghasilan Tidak Boleh Kosong
        </div>
      </div>
      <div class="col-md-6 mb-3">
        <label for="penghasilantambahan">Jumlah Orang</label>
        <input type="text" name="bil2" value="0" class="form-control" autocomplete="off" placeholder="Banyaknya Orang">
        <small class="text-muted">*Ex: Bisa Menghitung Untuk 1 Keluarga/per orangan</small> 
      </div>
    </div>
    <button class="btn btn-primary btn-lg btn-block" value="Hitung" name="hitung" type="submit">Hitung</button>
  </form>
  <div class="row">
    <div class="col-md-12">
      <h5>Zakat Fitrah Yang Harus Dikeluarkan</h5>
      <?php if(isset($_POST['hitung'])){ ?>
        <input type="text" value="<?php echo $hasil;?>" class="form-control">
      <?php }else{ ?>
        <input type="text" value="0" class="form-control">
      <?php } ?>
    </div>
  </div>
  <hr>
</div>
</body>
</html>