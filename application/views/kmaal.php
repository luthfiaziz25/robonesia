    
<!DOCTYPE html>
<html>
<head>
  <title>Kalkulator Zakat Maal</title>
</head>
<body>
 <?php 
 if(isset($_POST['hitung'])){
  $bil1 = $_POST['bil1'];
  $bil2 = $_POST['bil2'];

  $hasil=(($bil1+$bil2)*(2.5/100));
  $hasil1=$hasil/12;
}
?>


<div class="container">
  <form method="post" action="<?php echo base_url().'kalkulator/kmaal'; ?>">
    <center><h3>Kalkulator Zakat Maal</h3></center>
    <div class="row">
      <div class="col-md-6 mb-3">
        <label for="penghasilanbulanan">Jumlah Tabungan</label>
        <input type="text" name="bil1" value="0" class="form-control" autocomplete="off" placeholder="Tabungan">
        <div class="invalid-feedback">
          Penghasilan Tidak Boleh Kosong
        </div>
      </div>
      <div class="col-md-6 mb-3">
        <label for="penghasilantambahan">Harta Simpanan Lain</label>
        <input type="text" name="bil2" value="0" class="form-control" autocomplete="off" placeholder="Harta Simpanan Lain">
        <small class="text-muted">*jika tidak ada boleh dikosongkan</small> 
      </div>
    </div>
    <button class="btn btn-primary btn-lg btn-block" value="Hitung" name="hitung" type="submit">Hitung</button>
  </form>
  <div class="row">
    <div class="col-md-6">
      <h5>Zakat Per Tahun</h5>
      <?php if(isset($_POST['hitung'])){ ?>
        <input type="text" value="<?php echo $hasil;?>" class="form-control">
      <?php }else{ ?>
        <input type="text" value="0" class="form-control">
      <?php } ?>
    </div>
    <div class="col-md-6">
      <h5>Zakat Per Bulan</h5>
      <?php if(isset($_POST['hitung'])){ ?>
        <input type="text" value="<?php echo $hasil1;?>" class="form-control">
      <?php }else{ ?>
        <input type="text" value="0" class="form-control">
      <?php } ?>
    </div>
  </div>
  <hr>
</div>
</body>
</html>