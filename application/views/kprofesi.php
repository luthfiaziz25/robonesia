    
<!DOCTYPE html>
<html>
<head>
  <title>Kalkulator Zakat Profesi</title>
</head>
<body>
  <?php 
  if(isset($_POST['hitung'])){
    $bil1 = $_POST['bil1'];
    $bil2 = $_POST['bil2'];
    $bil3 = $_POST['bil3'];
    $bil4 = $_POST['bil4'];

    $hasil=((($bil1*12)+($bil2*12)-($bil3*12)-($bil4*12))*(2.5/100));
    $hasil1=$hasil/12;
  }
  ?>
  

  <div class="container">
    <form method="post" action="<?php echo base_url().'kalkulator/kprofesi'; ?>">
      <center><h3>Kalkulator Zakat Profesi</h3></center>
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="penghasilanbulanan">Penghasilan Per Bulan</label>
          <input type="text" name="bil1" value="0" class="form-control" autocomplete="off" placeholder="Penghasilan Bulanan">
          <div class="invalid-feedback">
            Penghasilan Tidak Boleh Kosong
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="penghasilantambahan">Penghasilan Lain Per Bulan</label>
          <input type="text" name="bil2" value="0" class="form-control" autocomplete="off" placeholder="Penghasilan Lain">
          <small class="text-muted">*jika tidak ada boleh dikosongkan</small> 
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="penghasilanbulanan">Angsuran Bulanan</label>
          <input type="text" name="bil3" value="0" class="form-control" autocomplete="off" placeholder="Angsuran Bulanan">
          <div class="invalid-feedback">
            Penghasilan Tidak Boleh Kosong
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="penghasilantambahan">Kebutuhan Pokok</label>
          <input type="text" name="bil4" value="0" class="form-control" autocomplete="off" placeholder="Kebutuhan Pokok Bulanan">
          <small class="text-muted">*jika tidak ada boleh dikosongkan</small> 
        </div>
      </div>
      <button class="btn btn-primary btn-lg btn-block" value="Hitung" name="hitung" type="submit">Hitung</button>
    </form>
    <div class="row">
      <div class="col-md-6">
        <h5>Zakat Per Tahun</h5>
        <?php if(isset($_POST['hitung'])){ ?>
          <input type="text" value="<?php echo $hasil;?>" class="form-control">
        <?php }else{ ?>
          <input type="text" value="0" class="form-control">
        <?php } ?>
      </div>
      <div class="col-md-6">
        <h5>Zakat Per Bulan</h5>
        <?php if(isset($_POST['hitung'])){ ?>
          <input type="text" value="<?php echo $hasil1;?>" class="form-control">
        <?php }else{ ?>
          <input type="text" value="0" class="form-control">
        <?php } ?>
      </div>
    </div>
    <hr>
  </div>
</body>
</html>