<!DOCTYPE html>
<html>
<head>
	<title>Hasil Pencarian</title>
	 <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
	<br>
	<br>
  <div class="intro">
            <h2 class="text-center">Kumpulan Program Kerja</h2>
        </div>
	<hr>
  <!-- Awal Konten -->
   <div class="article-list">
    <div class="container">
        <br>
        <div class="row articles">
            <?php foreach ($proker->result() as $key): ?>
            <div class="col-sm-6 col-md-4 item"><a href="<?php echo base_url('overview/detailpengabdian/'. $key->id)?>"><img class="img-fluid imghoverpost imgthumbnail" src="<?php echo base_url() ?>assets/img/<?php echo $key->image; ?>"</a>
                <h3 class="name"><a class="judulartikel judulpostkecil" href="<?php echo base_url('overview/detailpengabdian/'. $key->id)?>"><?php echo $key->namaproker;?></a></h3>
                <i class="fa fa-time" aria-hidden="true" >Pelaksanaan : <?php echo $key->waktu; ?></i><br>
                <p class="description"></p>
              <br>
              <br>
              <br>
              </div>
            <?php endforeach ?>
            <hr>
            </div>
        </div>
    </div>
    <!-- Ahir Konten -->
           <br>
           <br>
  
</body>
</html>