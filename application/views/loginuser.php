<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
  <?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">
      <form class="containerlog" method="post" action="<?php echo base_url('overview/aksi_login_user'); ?>">
        <center><label class="labellogin"><strong>Login Muzakki</strong></label></center><br>
        <input type="text" name="username" class="form-control" autocomplete="off" placeholder="Username"><br>
        <input type="password" name="password" class="form-control" autocomplete="off" placeholder="Password"><br> 
        <button class="btnlogin"  name="submit" type="submit"><strong>Login</strong></button> 
        <p class="textlogin">Belum Memiliki Account? <a class="textlink" href="<?php echo base_url('overview/add'); ?>"><strong>Sign Up</strong></a></p>
      </form>
</body>
</html>