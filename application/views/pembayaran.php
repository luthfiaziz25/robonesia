<!DOCTYPE html>
<html>
<head>
	<title>Pembayaran</title>
	<?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
</head>
<body>

	<div class="container">
		<br>
		<center><h3>Detail Transaksi</h3></center>		
		<?php foreach($profilku as $value) { ?>
		<table class="table">
			<tr>
				<td>Nama Muzakki</td>
				<td>:</td>
				<td><?php echo $value->nama; ?></td>
			</tr>

			<tr>
				<td>Jenis Zakat</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('jenis_zakat'); ?></td>
			</tr>

			<tr>
				<td>Jumlah Zakat</td>
				<td>:</td>
				<td><?php echo "Rp. ", number_format($this->session->userdata('jumlah_zakat')); ?></td>
			</tr>

			<tr>
				<td>Metode Pembayaran</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('metode_pembayaran'); ?></td>
			</tr>

			<tr>
				<td>No Rekening</td>
				<td>:</td>
				<td><?php foreach($rekening as $x_rekening) { ?>
					<?php if($x_rekening->bank == $this->session->userdata('metode_pembayaran')) {
						echo $x_rekening->no_rekening;
					}else{ 
						echo "";
					?>	
				<?php }} ?></td>
			</tr>
			<tr>
				<td>Atas Nama</td>
				<td>:</td>
				<td><?php foreach($rekening as $x_rekening) { ?>
					<?php if($x_rekening->bank == $this->session->userdata('metode_pembayaran')) {
						echo $x_rekening->atasnama;
					}else{ 
						echo "";
					?>	
				<?php }} ?></td>
			</tr>

			<tr>
				<td>Batas Waktu</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('tanggal'); ?></td>
			</tr>

			<tr>
				<td>Status Pembayaran</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('status_pembayaran'); ?></td>
			</tr>
		</table>
		<?php } ?>
		<div class="row">
		 <form method="POST" action="<?php echo base_url() ?>donasi/cancel">
		 <button class="btnprofil" type="submit"  name="submit">Cancel</button>
		 </form>
		 <form method="POST" action="<?php echo base_url() ?>donasi/bayar">
		 <button class="btnprofil" type="submit"  name="submit">Bayar</button>
		 </form>
		</div>
		 <!-- <a href="<?php echo base_url() ?>donasi/bayar" type="submit">Bayar</a> -->
		 
	</div>
</body>
</html>