<!DOCTYPE html>
<html>
<head>
  <title> Dashboard User</title>
  <?php $this->load->view("user/_partials/head.php")?>
</head>
<body class="imgartikel">
  <div class="container">
  <div class="row">
    <div class="col-sm-3">
        <br>
        <ul class="dashleftuser">
        <br>
      <div class="cardfb">
        <li><p class="ahrefjudul"><strong>Menu</strong></p></li>
        <hr>
        <li><a class="ahref activepage" href="<?php echo base_url()?>dashboarduser/profilku">Profil Muzzaki</a></li>
            <li><a class="ahref" href="" data-toggle="modal" data-target='#modal-setting'>Setting</a></li>
            <li><a class="ahref" href="<?php echo base_url()?>dashboarduser/tampilzakatku">Zakatku</a></li>
            <li><a class="ahref" href="<?php echo base_url()?>dashboarduser/saran">Feedback</a></li>
        <br>
      </div>
    </ul>   
    <aside>
                    <div class="col-md-4" >
                  <div class="cardfansuser" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
    </div>
    
    <div class="col-sm-6">
       
      <div class="bguser">
        <?php foreach ($profilku as $row) {?>
        <div class="dbuser"><br>
        <h4 class="titledbuser">Profil Muzaki</h4>
        <img class="imgprofil" disabled="" src="<?php echo base_url(). 'assets/img/'.$row->foto; ?>"><br>
        <div class="form-group">
        <input class="btn btn-default btnmargin" type="file" name="userfile"/>
        </div>
        <input class="inputdbuser disabled" type="text" disabled="" name="nama" placeholder="Nama" value="<?php echo $row->nama; ?>">
        <br>
        <input class="inputdbuser disabled" type="password" disabled="" name="password" placeholder="Password" value="<?php echo $row->password; ?>">
        <br>
        <input class="inputdbuser disabled" type="text" disabled="" name="username"  placeholder="Username" value="<?php echo $row->username; ?>">
        <input class="inputdbuser disabled" type="email" name="email" disabled="" placeholder="email" value="<?php echo $row->email; ?>">
        <br>
        <input class="inputdbuser disabled" type="number" disabled="" name="nomor_hp" placeholder="Nomor Hp" value="<?php echo $row->nomor_hp; ?>">
        <br>
        <small class="mutetext">*Boleh diisi Dosen/Mahasiswa</small><br>
        <input class="inputdbuser disabled" type="text" disabled="" name="pekerjaan" placeholder="pekerjaan" value="<?php echo $row->pekerjaan; ?>">
        <br>
        <textarea class="txtprofile disabled" name="alamat" disabled="" placeholder="Alamat"><?php echo $row->alamat;?></textarea>
        <br>
        <!-- <button class="btnprofil" type="submit"  name="submit">Edit</button> -->
      <?php }?>
       
      </div>  
      </div>
      </div>
      <br>
    <div class="col-sm-3">
    </div>
  </div>
</div>
<!-- Modal Setting -->
<div class="modal fade" id="modal-setting" tabindex="-1" role="doalog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="head-shape bg-head">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Ganti Password</h4>
          </div>

          <form class="form-horizontal" action="<?php echo site_url('dashboarduser/gantipass'); ?>" method="POST">
          <div class="modal-body">
            <div class="modal-body">
                <div class="form-group">
                  <input class="form-control" type="password" name="password" placeholder="password lama" required>
                </div>
                <div class="form-group">
                  <input class="form-control" type="password" name="newpassword" placeholder="New Password" required>
                </div>
                <div class="form-group">
                  <input class="form-control" type="password" name="renewpassword" placeholder="Re-New Password" required>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary btn-flat" id="simpan">Save</button>
                </div>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>

<?php $this->load->view("user/_partials/js.php")?>

</body>
</html>