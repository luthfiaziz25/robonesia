<!DOCTYPE html>
<html>
<head>
  <title>Register</title>
  <?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">
  <?php $this->load->view("admin/_partials/breadcrumb.php") ?>

        <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
  <?php endif; ?>

  <div class="bgsignup">
    <div class="tengah">
      <form action="<?php echo site_url('overview/add '); ?>" method="post">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-3 mb-1">
                <input type="text" name="username"  class="form-control <?php echo form_error('username') ? 'is-invalid':'' ?>" autocomplete="off" placeholder="Username">
                <div class="invalid-feedback">
                  <?php echo form_error('username') ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 mb-1">
                <input type="email" name="email"  class="form-control <?php echo form_error('email') ? 'is-invalid':'' ?>" autocomplete="off" placeholder="Email"> 
                <div class="invalid-feedback">
                  <?php echo form_error('email') ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 mb-1">
                <input type="password" name="password"  class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?>" autocomplete="off" placeholder="Password"> 
                 <div class="invalid-feedback">
                  <?php echo form_error('password') ?>
                </div>
              </div>
            </div>
            <!-- <div class="row">
              <div class="col-md-3 mb-3">
                <input type="password" name="repassword" class="form-control" autocomplete="off" placeholder="Re-password"> 
              </div>
            </div> -->
            <button class="btnlogin" value="Save" name="submit" type="submit"><strong>SignUp</strong></button>
          </div>
        </div>
        <p class="textlogin">Sudah Memiliki Account? <a class="textlink" href="<?php echo site_url('loginuser')?>"><strong>Login</strong></a></p>
      </form>
    </div>
  </div>
  <?php $this->load->view("user/_partials/modal.php")?>

</body>
</html>