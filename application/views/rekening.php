<!DOCTYPE html>
<html>
<head>
  <title>Rekening</title>
  <?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
  <br>
     <main role="main" class="container bgartikel">  
          <h2 class="border-bottom border-gray pb-2 mb-0" align="center" >DAFTAR REKENING BANK</h2>
        <?php $no=1; foreach($rekening->result() as $row): ?>
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="media text-muted pt-3">
            <h5 style="margin-right: 30px;"><?php echo $no++; ?></h5>
            <img class="ic_bank" src="<?php echo base_url(). 'assets/img/'.$row->image; ?>" class="mr-2 rounded">
            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
              <strong class="d-block text-gray-dark"><?php echo $row->bank; ?></strong>
              <strong class="d-block text-gray-dark"><?php echo $row->no_rekening; ?></strong>
              <?php echo $row->atasnama; ?>
            </p>
          </div>
          </div>
          <?php endforeach ?>
    </main>
</body>
</html>