<!DOCTYPE html>
<html>
<head>
	<title>Hasil Pencarian</title>
	 <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
	<br>
	<br>
	<center><h2 class="text-center">Hasil Pencarian</h2></center>
	<hr>
   <div class="container">
    	<div class="row">
    <?php
      if(count($berita)>0){
        $no = 1;
        foreach($berita as $k){?>
    		<div style="padding-left: 40px"><a href="<?php echo base_url('overview/detailberita/'. $k->id)?>"><img class="img-fluid imghoverpost imgthumbnailsearch" src="<?php echo base_url() ?>assets/img/<?php echo $k->image; ?>"></a>
                <h3 class="name"><a class="judulartikel judulpostkecil" href="<?php echo base_url('overview/detailberita/'. $k->id)?>"><?php echo $k->judul;?></a></h3>
                <i class="fa fa-user" aria-hidden="true" > : <?php echo $k->penulis; ?></i><br>
                <small>Posted : <?php echo $k->posted; ?></small>
                <p class="description"></p></div>
                <?php }}else{ ?>
                <div class="col-sm-12">
                <br>
                <br>
                <br>
      			<center><h1 style="color: grey">No Result</h1></center>
      			<br>
      			<br>
      			<br>
  				</div>
  <?php } ?>
            </div>
           </div>
           <br>
           <br>
  
</body>
</html>