<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Header-Blue.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-Clean.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-Menu.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-with-Search.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Team-Boxed.css">
    <link rel="icon" href="<?php echo base_url()?>assets/img/logouin.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</head>

<body>
<header>
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-search fixed">
        <div class="container">
            <div class="row">
                <ul>
                    <a href="#"><img class="logouin" src="<?php echo base_url()?>assets/img/LogoUPZ.png"></a>
                </ul>
            </div>
            <div class="col-md-6 col-md-11 item">
                <h3 class="UPZHeader">UPZ MD</h3>
                <a class="detailupzheader" href="<?php echo base_url()?>/home">UNIT PENGUMPUL ZAKAT MANAJEMEN DAKWAH</a>
            </div>
        </div>
    </nav>
</header>
<div></div>
<div>
    <!-- Navbar Lama -->
    <nav class="navbar navbar-light navbar-expand-md navigation-clean fixednav">
        <div class="container"><a class="navbar-brand" href="#"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"
            id="navcol-1">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item" role="presentation"><a class="nav-link" href="<?php echo base_url()?>overview/home">Home</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Profil&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url()?>/visimisi">Visi dan Misi</a><a class="dropdown-item" role="presentation" href="<?php echo base_url()?>/struktur">Struktur Lembaga</a></div>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Zakat&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url()?>/zakat">Panduan Zakat</a><a class="dropdown-item" role="presentation" href="<?php echo base_url()?>/kalkulator">Kalkulator Zakat</a></div>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="<?php echo base_url()?>/rekening">Rekening Donasi</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="<?php echo base_url()?>/download">Download</a></li>
                <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Program&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Program Mingguan</a><a class="dropdown-item" role="presentation" href="#">Program Bulanan</a><a class="dropdown-item" role="presentation" href="#">Program Tahunan</a></div>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="<?php echo base_url()?>/donasi">Zakat/Donasi</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Muzzaki&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url()?>/loginuser">Login</a><a class="dropdown-item" role="presentation" href="<?php echo base_url()?>/register">SignUp</a></div>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>
<nav class="blok">

</nav>

<!-- content -->
<?php echo $content; ?>
<!-- // -->
<div class="footer-dark">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 item">
                    <h3>Layanan</h3>
                    <ul>
                        <li><a href="https://mail.google.com/mail/u/0/#inbox?compose=GTvVlcSMVJTRxtvmpjBpNkxnWxzXJvktXxJnkwNBcznnzSBjPpqvxNzRmNwHPbwzWTqCXtTrHNhpl" rel="nofollow" target="_blank">Email : fidkomzakat@gmail.com</a></li>
                        <li><a href="#">Telp : 0227 800 525</a></li>
                    </ul>
                    <ul style="margin-top: 20px">
                        <a href="https://uinsgd.ac.id"><img class="logouin" src="<?php echo base_url()?>assets/img/logouin.png"></a>
                        <a href="#"><img class="logouin" src="<?php echo base_url()?>assets/img/LogoUPZ.png"></a>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-4 item">
                    <h3>Kantor Pusat</h3>
                    <ul>
                        <li><a href="https://www.google.com/maps/place/UIN+Sunan+Gunung+Djati+Bandung/@-6.9271274,107.7245824,15.26z/data=!4m5!3m4!1s0x2e68dd42c254a55d:0xee52343f78dc2e32!8m2!3d-6.9316482!4d107.7171922" rel="nofollow" target="_blank">Jl.A.H. Nasution No.105, Cipadung, Cibiru,Kota Bandung, Jawa Barat 40614</a></li>
                    </ul>
                </div>
                <div class="col-md-4 item text">
                    <h3>UPZ MD | Unit Pengumpul Zakat Manajemen Dakwah</h3>
                    <p>UPZ MD Adalah sebuah lembaga atau wadah resmi untuk mengelola mengenal mengenai Zakat, Infak dan Sedekah. UPZ Manajemen Dakwah didirikan tahun 2016 berdasarkan SK BAZNAS Nomor Un.05/III.4/PP.009/20/2016 tanggal 05 Februari 2016</p>
                </div>
                <div class="col item social">
                    <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="icon ion-social-facebook"></i></a>
                    <a href="http://www.twitter.com"  rel="nofollow" target="_blank"><i class="icon ion-social-twitter"></i></a>
                    <a href="http://www.youtube.com" rel="nofollow" target="_blank"><i class="icon ion-social-youtube"></i></a>
                    <a href="http://www.instagram.com" rel="nofollow" target="_blank""><i class="icon ion-social-instagram"></i></a>
                    <a href="https://api.whatsapp.com/send?phone=6289683834288&text=Halo%20Admin%20Saya%20Mau%20Zakat" rel="nofollow" target="_blank""><i class="icon ion-social-whatsapp"></i></a></div>
                </div>
                <p class="copyright">UPZ | MD © 2019</p>
            </div>
        </footer>
    </div>
    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>