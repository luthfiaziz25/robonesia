<!DOCTYPE html>
<html>
<head>
    <title>Zakat Surat Berharga</title>
    <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
    <br>
<div class="article-dual-column">
    <div class="container bgartikel">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="intro">
                    <h1 class="text-center">PANDUAN ZAKAT</h1>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3 marginside">
                <div class="toc borderside">
                    <aside>
                        <p class="borderleftorange">Tentang Zakat</p>                      
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/surat'; ?>"><button class="btnactiveorange">Zakat Surat Berharga</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>

                    </aside>
                    <aside>
                                <div class="col-md-4" >
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
                </div>
            </div>
            <div class="col-md-9">
                <div class="text">
                    <div class="text-justify">
                        <center><h3>Zakat Surat Berharga</h3></center>
                        <hr>
                        <h4>Zakat Saham</h4>
                        <p>Salah satu bentuk harta yang berkaitan dengan perusahaan dan bahkan berkaitan dengan kepemilikannya adalah saham. Pemegang saham adalah pemilik perusahaan yang mewakilkan kepada manajemen untuk menjalankan operasional perusahaan. Pada setiap akhir tahun, yang biasanya pada waktu Rapat Umum Pemegang Saham (RUPS) dapatlah diketahui keuntungan (deviden) perusahaan, termasuk juga kerugiannya. Pada saat itulah ditentukan kewajiban zakat terhadap saham tersebut</p>

                        <p>Dalam pembahasan forum-forum ulama dunia, keputusan fatwa baik secara ijtihad al-fardiyah (perorangan) ataupun ijtihad al-jamaai (lembaga fatwa), juga dari buku-buku karya ulama mu’tabar, telah disepakati bahwa hukum mengeluarkan zakat atas kepemilikan saham adalah wajib bagi yang telah mencapai haul dan nishab.</p>

                        <p>BAZNAS memberikan kemudahan kepda investor dalam menunaikan zakat melalui sahamnya. Saat ini investor tidak perlu menjual saham yang dimiliki untuk menunaikan zakat atas saham yang dimiliki. Zakat dapat ditunaikan ke BAZNAS dengan memindahbukukan saham sejumlah kadar zakatnya (2,5%). berikut simulasinya</p>
                        <p>
                            <img src="assets/img/saham.jpg">
                        </p>
                        <h4>Zakat Obligasi Syariah/Sukuk</h4>
                        <p>
                            Yusuf al-Qaradhawi menyatakan bahwa obligasi adalah perjanjian tertulis dari bank, perusahaan, atau pemerintah kepada pemegangnya untuk melunasi sejumlah pinjaman dalam masa tertentu dengan bunga tertentu pula. Selanjutnya, Yusuf al-Qaradhawi104 mengemukakan perbedaan antara saham dan obligasi sebagai berikut:
                        </p>
                        <p>
                            Pertama, saham merupakan bagian dari harta bank atau perusahaan, sedangkan obligasi merupakan pinjaman kepada perusahaan, bank atau pemerintah.
                        </p>
                        <p>
                            Kedua, saham memberikan keuntungan sesuai dengan keuntungan perusahaan atau bank, yang besarnya tergantung pada keberhasilan perusahaan atau bank itu, tetapi juga menanggung kerugiannya. Sedangkan obligasi memberikan keuntungan tertentu (bunga) atas pinjaman tanpa bertambah atau berkurang.
                        </p>
                        <p>
                            Ketiga, pemilik saham berarti pemilik sebagian perusahaan dan bank itu sebesar nilai sahamnya. Sedangkan pemilik obligasi berarti pemberi utang atau pinjaman kepada perusahaan, bank atau pemerintah.
                        </p>
                        <p>
                            Keempat, deviden saham hanya dibayar dari ke-untungan bersih perusahaan, sedangkan bunga obligasi dibayar setelah waktu tertentu yang ditetapkan.
                        </p>
                        <p>
                            Selama perusahaan tersebut tidak memproduksi barang-barang atau komoditas-komoditas yang dilarang, maka saham menjadi salah satu obyek atau sumber zakat. Sedangkan obligasi sangat tergantung kepada bunga yang termasuk kategori riba yang dilarang secara tegas oleh ajaran Islam. Meskipun demikian, yang menarik adalah bahwa sebagian ulama, walaupun sepakat akan haramnya bunga, tetapi mereka tetap menyatakan bahwa obligasi adalah satu obyek atau sumber zakat dalam perekonomian modern ini. Muhammad Abu Zahrah menyatakan bahwa jika obligasi itu kita bebaskan dari zakat, maka akibatnya orang lebih suka memanfaatkan obligasi daripada saham. Dengan demikian, orang akan terdorong untuk meninggalkan yang halal dan melakukan yang haram. Dan juga bila ada harta haram, sedangkan pemiliknya tidak diketahui, maka ia disalurkan kepada sedekah.
                        </p>
                        <p>
                            Karena obligasi hanya tergantung pada bunga, maka bukan merupakan obyek atau sumber zakat. Karena zakat hanyalah diambil dari harta yang jelas baik dan halal. Sementara bunga termasuk kategori riba, dan riba itu sangat jelas keharamannya, baik dalam jumlah yang sedikit maupun yang berlipat ganda.
                        </p>
                        <p>
                            Keharaman riba (bunga) di samping berlandaskan kepada ayat-ayat tersebut di atas, beberapa buah hadis Nabi yang shahih, juga hampir seluruh ulama berpendapat hal yang sama, bahkan peserta Sidang Organisasi Konferensi Islam (OKI) kedua yang berlangsung di Karachi Pakistan pada bulan Desember 1970 menyatakan hal yang sama pula, yaitu bahwa praktik bank dengan sistem bunga adalah tidak sesuai dengan syariat Islam. Karena itu, hemat penulis, diskusi tentang bunga bank itu haram ataukah tidak, harus dianggap sudah selesai dan yang paling kuat dari sudut dalil dan kemaslahatan umat bahwa bunga itu adalah termasuk riba, dan karena itu termasuk yang diharamkan. Tugas kaum muslimin sekarang menguatkan alternatif dan jalan keluar dari sistem ekonomi ribawi. Seperti jual beli yang halal dan bersih, menguatkan lembaga keuangan syariah (LKS) yang bebas dari bunga dan menguatkan peran dari zakat, infak, sedekah, dan wakaf (ZISWAF).
                        </p>
                        <p>
                            Selanjutnya alternatif dari obligasi konvensional adalah sukuk. Sukuk merupakan surat berharga atau obligasi yang diterbitkan berdasarkan prinsip syariah, baik diterbitkan oleh negara maupun korporasi, di mana basis akad yang digunakan adalah akad-akad yang sesuai syariah, seperti mudharabah, musyarakah, ijarah, dan lain-lain. Sehingga, karakteristik return atau yield yang diterima investor sukuk bergantung pada akad yang digunakan. Ketika sukuk diterbitkan dengan menggunakan akad ijarah, maka investor akan mendapatkan imbal hasil yang tetap. Demikian pula dengan akad lainnya. Oleh karena sukuk merupakan instrumen investasi yang sesuai syariah, maka para investor pun terkena kewajiban zakat jika telah memenuhi ketentuan syariah.
                        </p>
                        <p>
                            Zakat pada sukuk ini dianalogikan dengan zakat perdagangan atau zakat emas perak, baik dari sisi nishab maupun kadarnya. Nishabnya adalah sebesar 85 gram emas, dikeluarkan dari pokok dan return yang diterima, dengan kadar 2,5 persen.
                        </p>
                        <a href="http://baznas.go.id/zakatsuratberharga">Source :BAZNAS</a>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
</body>
</html>