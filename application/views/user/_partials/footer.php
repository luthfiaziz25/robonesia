    <div class="footer-dark">
    <div class="container">
    <div class="row">
                <div class="col-sm-6 col-md-4 item">
                    <h3>Layanan</h3>
                    <ul>
                        <li><a href="https://mail.google.com/mail/u/0/#inbox?compose=GTvVlcSMVJTRxtvmpjBpNkxnWxzXJvktXxJnkwNBcznnzSBjPpqvxNzRmNwHPbwzWTqCXtTrHNhpl" rel="nofollow" target="_blank">Email : fidkomzakat@gmail.com</a></li>
                        <li><a href="#">Telp : 0227 800 525</a></li>
                    </ul>
                    <ul style="margin-top: 20px">
                        <a href="https://uinsgd.ac.id"><img class="logouin" src="<?php echo base_url()?>assets/img/logouin.png"></a>
                        <a href="#"><img class="logouin" src="<?php echo base_url()?>assets/img/LogoUPZ.png"></a>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-4 item">
                    <h3>Kantor Pusat</h3>
                    <ul>
                        <li><a href="https://www.google.com/maps/place/UIN+Sunan+Gunung+Djati+Bandung/@-6.9271274,107.7245824,15.26z/data=!4m5!3m4!1s0x2e68dd42c254a55d:0xee52343f78dc2e32!8m2!3d-6.9316482!4d107.7171922" rel="nofollow" target="_blank">Jl.A.H. Nasution No.105, Cipadung, Cibiru,Kota Bandung, Jawa Barat 40614</a></li>
                    </ul>
                </div>
                <div class="col-md-4 item text">
                    <h3>UPZ MD | Unit Pengumpul Zakat Manajemen Dakwah</h3>
                    <p>UPZ MD Adalah sebuah lembaga atau wadah resmi untuk mengelola mengenal mengenai Zakat, Infak dan Sedekah. UPZ Manajemen Dakwah didirikan tahun 2016 berdasarkan SK BAZNAS Nomor Un.05/III.4/PP.009/20/2016 tanggal 05 Februari 2016</p>
                </div>
                <!-- <div class="col item social col-md-12 col-md-6">
                    <a class="itemicon" href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="icon ion-social-facebook"></i></a>
                    <a class="itemicon"  href="http://www.twitter.com"  rel="nofollow" target="_blank"><i class="icon ion-social-twitter"></i></a>
                    <a class="itemicon"  href="http://www.youtube.com" rel="nofollow" target="_blank"><i class="icon ion-social-youtube"></i></a>
                    <a class="itemicon"  href="http://www.instagram.com" rel="nofollow" target="_blank""><i class="icon ion-social-instagram"></i></a>
                    <a class="itemicon"  href="https://api.whatsapp.com/send?phone=6289683834288&text=Halo%20Admin%20Saya%20Mau%20Zakat" rel="nofollow" target="_blank""><i class="icon ion-social-whatsapp"></i></a></div> -->
                <div class="col item social">
                    <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="icon ion-social-facebook"></i></a>
                    <a href="http://www.twitter.com"  rel="nofollow" target="_blank"><i class="icon ion-social-twitter"></i></a>
                    <a href="http://www.youtube.com" rel="nofollow" target="_blank"><i class="icon ion-social-youtube"></i></a>
                    <a href="http://www.instagram.com" rel="nofollow" target="_blank""><i class="icon ion-social-instagram"></i></a>
                    <a href="https://api.whatsapp.com/send?phone=6289683834288&text=Halo%20Admin%20Saya%20Mau%20Zakat" rel="nofollow" target="_blank""><i class="icon ion-social-whatsapp"></i></a></div>
                </div>
            <p class="copyright">UPZ | MD © 2019</p>
</div>
</div>