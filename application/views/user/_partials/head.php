<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Header-Blue.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-Clean.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-Menu.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Navigation-with-Search.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Testimonials.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Map-Clean.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Highlight-Blue.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/Registration-Form-with-Photo.css">
    <link rel="icon" href="<?php echo base_url()?>assets/img/logouin.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <!-- <link href="<?php echo base_url() ?>assets/img/upz.png" rel="icon" type="image/png">
    <link href="<?php echo base_url() ?>assets/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/vendor/morrisjs/morris.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
