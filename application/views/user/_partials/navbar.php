    <nav class="navbar navbar-light navbar-expand-md navigation-clean-search fixed">
        <div class="col-md-12 col-md-12">
            <div class="row">
                <div class="col-md-12">
                <ul>
                    
                    <a class="linkupzmd" href="<?php echo base_url()?>/overview/home"><img class="logouin" src="<?php echo base_url()?>assets/img/LogoUPZ.png">UNIT PENGUMPUL ZAKAT MANAJEMEN DAKWAH</a>
                </ul>
                </div>
            </div>
            <ul class="nav navbar header-blue">
                <li class="" role="presentation"><a class="nav-link" href="<?php echo base_url().'overview/home'; ?>">Home</a></li>
                <li class="" role="presentation"><a class="nav-link" href="<?php echo base_url().'Galeri'; ?>">Galeri</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Profil&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/visimisi'; ?>">Visi dan Misi</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/struktur'; ?>">Struktur Lembaga</a></div>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Zakat&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/panduan'; ?>">Panduan Zakat</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'kalkulator/kprofesi'; ?>">Kalkulator Zakat</a></div>
                </li>
                <li class="" role="presentation"><a class="nav-link" href="<?php echo base_url().'overview/rekening'; ?>">Rekening Donasi</a></li>
                <li class="" role="presentation"><a class="nav-link" href="<?php echo base_url().'donasi/donasi'; ?>">Donasi</a></li>
               <!--  <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Program&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/minggu'; ?>">Program Mingguan</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/bulan'; ?>">Program Bulanan</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/tahun'; ?>">Program Tahunan</a></div>
                </li> -->
                <?php if($this->session->userdata('status') == "login_user") 
                {?>
                    <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Muzzaki&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'dashboarduser/profilku'; ?>">Profilku</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'dashboarduser/tampilzakatku'; ?>">Zakatku</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'dashboarduser/saran'; ?>">Saran & Masukan</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/logout'; ?>">Logout</a></div>
                </li>
                <?php } else {?>
                    <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Muzzaki&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/login'; ?>">Login</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'overview/add'; ?>">SignUp</a></div>
                </li>
                <?php } ?>
                <form action="<?php echo site_url('overview/cari');?>" method="get">
                <input class="search" type="text" name="cari" placeholder="Cari berita" value="<?php echo (isset($_GET['cari'])) ? $_GET['cari'] : ''; ?>">
                 <button type="submit">Cari Berita</button>
                 </form>
                </ul>
        </div>
    </nav>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>