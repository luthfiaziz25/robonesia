    <nav class="navbar navbar-light navbar-expand-md navigation-clean-search fixed">
            <div class="row">
                <div class="col-md-2">
                <ul>
                    <a href="#"><img class="logouin" src="<?php echo base_url()?>assets/img/LogoUPZ.png"></a>
                </ul>
                </div>
            </div>
            <div class="col-md-4 titlenav">
                <h3 class="UPZHeader">UPZ MD</h3>
                <a class="linkupzmd" href="<?php echo base_url()?>/loged/home">UNIT PENGUMPUL ZAKAT MANAJEMEN DAKWAH</a>
            </div>
            <div class="row">
            <div class="col-md-12">
                <div class="container"><a class="navbar-brand" href="#"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
            id="navcol-1">
                <ul class="nav navbar-nav ml-auto">
                <li class="nav-item" role="presentation"><a class="nav-link" href="<?php echo base_url().'loged/home'; ?>">Home</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Profil&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/visimisi'; ?>">Visi dan Misi</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/struktur'; ?>">Struktur Lembaga</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/galeri'; ?>">Galeri</a></div>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Zakat&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/panduan'; ?>">Panduan Zakat</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/kprofesi'; ?>">Kalkulator Zakat</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'donasi/donasi'; ?>">Donasi/Zakat</a></div>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="<?php echo base_url().'loged/rekening'; ?>">Rekening Donasi</a></li>
                <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Program&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/minggu'; ?>">Program Mingguan</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/bulan'; ?>">Program Bulanan</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/tahun'; ?>">Program Tahunan</a></div>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Muzzaki&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/profilku'; ?>">Profilku</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/zakatku'; ?>">Zakatku</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/saran'; ?>">Saran & Masukan</a><a class="dropdown-item" role="presentation" href="<?php echo base_url().'loged/logout'; ?>">Logout</a></div>
                </li>
                </ul>
            </div>
        </div>
            </div>
            </div>
    </nav>
    <br>
    <br>
    <br>
    <br>
    <br>