<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
</head>
<body>
    <?php $this->load->view("user/_partials/navbar.php")?>
    <?php $this->load->view($content); ?>
    <?php $this->load->view("user/_partials/footer.php")?>
</body>
</html>