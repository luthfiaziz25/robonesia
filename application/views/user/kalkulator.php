<!DOCTYPE html>
<html>
<head>
	<title>Kalkulator</title>
<?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
</head>

<body class="imgartikel">
  <?php $this->load->view("user/_partials/navbarloged.php")?>

  <br>
  <div class="container bgartikel">
    <br>
    <div class="col-sm-12">
      <p class="borderleft0">KALKULATOR</p>
      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/kprofesi'; ?>"><button class="btnsidered">Zakat Profesi</button></a></li>
      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/kmaal'; ?>"><button class="btnsidered">Zakat Maal</button></a></li>
      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/kemas'; ?>"><button class="btnsidered">Zakat Emas</button></a></li>
      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/kperusahaan'; ?>"><button class="btnsidered">Zakat Perusahaan</button></a></li>
      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/kfitrah'; ?>"><button class="btnsidered">Zakat Fitrah</button></a></li>
    </div>
    <hr>
    <?php $this->load->view($content); ?>
  </div>
  <?php $this->load->view("user/_partials/footer.php")?>

</body>
</html>           