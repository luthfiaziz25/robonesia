
<!DOCTYPE html>
<html>
<head>
  <title>Berita</title>
  <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">
  <br>
  <div class="container bgartikel">
    <div class="article-dual-coloum">
      <div class="row">
        <div class="row alignartikel">
          
          <div class="col-md-8 col-lg-8 marginartikel">
            <div class="row">
              <div class="col-md-12 itemcenter">
                <img class="bannerartikel" src="<?php echo base_url()?>assets/img/artikel9.jpg">
              </div>
            </div>
            <hr>
            <h6>Author  : <a href="" name="author">Muhammad Luthfi Aziz</a></h6>
            <h6>Publish : <a name="date">20 Januari 2019</a></h6><br>
            <center><h3 class="judulberita" name="judul"><strong>Ini Judul Berita/Pengabdian</strong></h3></center><hr>
            <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
            <div class="text textcontent">
              <p>UPZ MD Adalah sebuah lembaga atau wadah resmi untuk mengelola mengenal mengenai Zakat, Infak dan Sedekah. UPZ Manajemen Dakwah didirikan tahun 2016 berdasarkan <strong> SK BAZNAS Nomor Un.05/III.4/PP.009/20/2016 tanggal 05 Februari 2016</strong></p>
            </div>
            <h3 class="titleprofil">Visi dan Misi</h3>
            <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
            <div class="text">
              <h5>Visi</h5>
              <p class="textcontent">Membangun kesejahteraan Umat Muslim
               Menjadi lembaga atau wadah yang amanah, jujur, Profesional dan Bertanggung Jawab
             </p>
             <h5>Misi</h5>
             <p class="textcontent">
              Membangun kesadaran umat Muslim tentang kewajiban menunaikan Zakat.
              Meningkatkan kepedulian sesama umat manusia terutama terhadap anak Yatim dan Dhuafa.
            </p>
          </div>
          <h3 class="titleprofil">Fungsi dan Peran</h3>
          <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
          <ul>
            <li>Melakukan Pengumpulan ZIS Secara maksimal</li>
            <li>Memberikan pelayanan sebaik-baiknya kepada Muzaki atau Munfik</li>
            <li>Mengelola ZIS sesuai dengan syar'i</li>
            <li>Menyampaikan laporan Keuangan secara berkala dan transparansi</li>
            <li>Menjalankan tugas selalu berkoordinasi dan komunikai dengan jurusan Manajemen Dakwah dan BAZNAS Provinsi Jawa Barat</li>
          </ul>
          <h3 class="titleprofil">Program</h3>
          <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
          <ul>
            <li>Dapat Mensosialisasikan ZIS Kepada masyarakat</li>
            <li>Menghimpun ZIS Secara optimal</li>
            <li>Mendistribusikan secara selektif kepada Mustahik, Yatim dan Dhuafa</li>
            <li>Memberikan bantuan perlengkapan dan santunan kematian secara tepat dan tanggap</li>
            <li>Memberikan besasiswa bagi mahasiswa/i tidak mampu yang memiliki prestasi</li>
            <li>Melengkapi kebutuhan administrasi dan Operasional penunjang sekretariat UPZ Manajemen Dakwah</li>
            <li>Membangun citra dan kepercayaan masyarakat, Muzaki, Munfiq</li>
          </ul>
          <hr>
          <!-- <div class="row">
            <div class="col-md-6">
              <img src="<?php echo base_url()?>assets/img/artikel9.jpg" class="imgsmall">
              
              <p class="smalldeskripsi"> Ini Deskripsi singkat tentang berita populer yang ditampilkan di suggestion berita</p>
            </div>
            <div class="col-md-6">
              <img src="<?php echo base_url()?>assets/img/artikel9.jpg" class="imgsmall">
              <a href="" class="titlesmall">
                Ini Judul Kecil 
              </a>
              <p> Ini Deskripsi singkat tentang berita populer yang ditampilkan di suggestion berita</p>
            </div>
          </div> -->
        <div class="projects-horizontal">
        <div class="container bgartikelsmall">
            <div class="intro">
                <p class="text-center"></p>
            </div>
            <div class="row projects">
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel1.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Santunan Anak Yatim</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel2.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Peduli Kesehatan</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel3.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Lomba Anak Berbakat</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel4.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Pembangunan Mesjid</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
        <div class="col-md-9 col-lg-3 marginside">
          <div class="toc borderside">
            <aside>
              <div class="cardfansartikel" style="background-color:rgba(255,255,255,0.9) ;">
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

                <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
              </div>

              <p  class="borderleftartikelred">Berita Terpopuler</p>
              <li class="borderleftgambarred">
                <a class="sidenews" href=""><img class="sideimg" src="<?php echo base_url()?>assets/img/artikel9.jpg"></a><a class="sidetitle" href="">Bantuan Untuk Kesehatan Masyarakat</a></li>
                <li class="borderleftgambarred">
                  <a class="sidenews" href=""><img class="sideimg" src="<?php echo base_url()?>assets/img/artikel9.jpg"></a><a class="sidetitle" href="">Bantuan Untuk Kesehatan Masyarakat</a></li>
                  <li class="borderleftgambarred">
                    <a class="sidenews" href=""><img class="sideimg" src="<?php echo base_url()?>assets/img/artikel9.jpg"></a><a class="sidetitle" href="">Bantuan Untuk Kesehatan Masyarakat</a></li>
                    <li class="borderleftgambarred">
                      <a class="sidenews" href=""><img class="sideimg" src="<?php echo base_url()?>assets/img/artikel9.jpg"></a><a class="sidetitle" href="">Bantuan Untuk Kesehatan Masyarakat</a></li>

                      <p class="borderleftorange">Tentang Zakat</p>                      
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>

                      <p class="borderleftgreen">Program Kerja</p>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url()?>maal"><button class="btnsidegreen">Program Mingguan</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url()?>maal"><button class="btnsidegreen">Program Bulanan</button></a></li>
                      <li class="borderleft1"><a class="aside" href="<?php echo base_url()?>maal"><button class="btnsidegreen">Program Tahunan</button></a></li>
                    </aside>
                  </div>
                  <br>
                  <br>
                  <br>
                </div>
              </div>
              <!-- end sidebar -->
            </div>
          </div>
        </div>
        <br>
        <!-- end wrapper -->

      </body>
      </html>
<!--     <div class="article-clean">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">
                    <div class="intro">
                        <h1 class="text-center">Judul Artikel</h1>
                        <p class="text-center"><span class="by">by</span> <a href="#">Author Name</a><span class="date">Sept 8th, 2016 </span></p><img class="img-fluid" src="<?php echo base_url()?>assets/img/desk.jpg"></div>
                    <div class="text">
                        <p>Sed lobortis mi. Suspendisse vel placerat ligula. <span style="text-decoration: underline;">Vivamus</span> ac sem lac. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in
                            justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
                        <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac lacus. <strong>Ut vehicula rhoncus</strong> elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit <em>pulvinar dict</em> vel in justo. Vestibulum
                            ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
                        <h2>Aliquam In Arcu </h2>
                        <p>Suspendisse vel placerat ligula. Vivamus ac sem lac. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                            posuere cubilia Curae.</p>
                        <figure><img class="figure-img" src="<?php echo base_url()?>assets/img/beach.jpg">
                            <figcaption>Caption</figcaption>
                        </figure>
                        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Suspendisse vel placerat ligula. Vivamus ac sem lac. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit
                            pulvinar dictum vel in justo.</p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->