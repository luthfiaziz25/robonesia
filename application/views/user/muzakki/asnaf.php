<!DOCTYPE html>
<html>
<head>
    <title>Asnaf Zakat</title>
    <?php $this->load->view("user/_partials/head.php")?>
    <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
    <br>
 <!--Artikel-->

    <div class="article-dual-column">
        <div class="container bgartikel">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="intro">
                        <h1 class="text-center">PANDUAN ZAKAT</h1>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 marginside">
                    <div class="toc borderside">
                        <aside>
                            <p class="borderleftorange">Tentang Zakat</p>                      
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/asnaf'; ?>"><button class="btnactiveorange">Asnaf Zakat</button></a></li>

                        </aside>
                        <aside>
                                <div class="col-md-4" >
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="text">
                        <div class="text-justify">
                            <center><h3>Asnaf Zakat</h3></center>
                            <hr>
                            <p>Sebagai instrumen yang masuk dalam salah satu Rukun Islam, zakat tentu saja memiliki aturan mengikat dari segi ilmu fiqihnya. Mulai dari akan melakukan pembayaran zakat sampai berakhir pada penyalurannya, semua diatur dengan jelas di dalam aturan Islam yang mengikat. Aturan ini serta merta bukan untuk memberatkan umat islam, namun sebagai bentuk kasih sayang Allah agar kita tidak mendzhalimi seseorang.</p>

                            <p>Selama ini kita sudah sering mendengar wajibnya membayar zakat, lalu sudah tahukah Kita dengan jelas dan rinci siapa saja golongan yang diperbolehkan menerima zakat? Yuk, kita simak ulasan mengenai 8 Asnaf yang menerima manfaat zakat berdasarkan surat At-Taubah ayat 60:</p>

                            <p>1.   Fakir; Mereka yang hampir tidak memiliki apa-apa sehingga tidak mampu memenuhi kebutuhan pokok hidup.</p>
                            <p>
                                2.  Miskin; Mereka yang memiliki harta namun tidak cukup untuk memenuhi kebutuhan dasar untuk hidup.
                            </p>
                            <p>
                                3.  Amil; Mereka yang mengumpulkan dan mendistribusikan zakat.
                            </p>
                            <p>
                                4.  Mu'allaf; Mereka yang baru masuk Islam dan membutuhkan bantuan untuk menguatkan dalam tauhid dan syariah.
                            </p>
                            <p>
                                5.  Hamba sahaya; Budak yang ingin memerdekakan dirinya.
                            </p>
                            <p>
                                6.  Gharimin; Mereka yang berhutang untuk kebutuhan hidup dalam mempertahankan jiwa dan izzahnya.
                            </p>
                            <p>
                                7.  Fisabilillah; Mereka yang berjuang di jalan Allah dalam bentuk kegiatan dakwah, jihad dan sebagainya.
                            </p>
                            <p>
                                8.  Ibnus Sabil; Mereka yang kehabisan biaya di perjalanan dalam ketaatan kepada Allah
                            </p>
                            <a href="http://baznas.go.id/asnaf">Source :BAZNAS</a>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</body>
</html>