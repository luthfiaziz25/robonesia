<!DOCTYPE html>
<html>
<head>
  <title>Zakat/Donasi</title>
    <?php $this->load->view("user/_partials/head.php")?>
    <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>

<!--Container-->
      <br>
      <main role="main" class="container">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h2 class="border-bottom border-gray pb-2 mb-0" align="center" >Bayar Zakat</h2>
          <div class="row">
            <div class="col-md-5 mb-3">
                <label for="country">Pilih Type Transaksi</label>
                <select class="custom-select d-block w-100" id="typezakat" required>
                  <option value="">Pilih Transaksi</option>
                  <optgroup label="Zakat">
                    <option value="1">Zakat Profesi</option>
                    <option value="2">Zakat Maal</option>
                    <option value="3">Zakat Surat Berharga</option>
                    <option value="4">Zakat Perusahaan</option>
                    <option value="5">Zakat Fitrah</option>
                  </optgroup>
                  <optgroup label="--------------------">
                    <option value="6">Infaq/Shodaqoh</option>
                  </optgroup>
                </select>
                <div class="invalid-feedback">
                  Mohon Pilih Salah Satu Type Zakat
                </div>
              </div>
            </div>
            <hr class="mb-4">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="Nama">Nama Donatur/Muzakki</label>
                <input type="text" class="form-control" name="nama_donatur" placeholder="Nama" required>
                <div class="invalid-feedback">
                  Penghasilan Tidak Boleh Kosong
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="uangzakat">Jumlah Uang Zakat/Shodaqoh</label>
                <input type="number" id="rupiah" class="form-control" name="jumlahuang" placeholder="Rp." required>
                <div class="invalid-feedback">
                  Penghasilan Tidak Boleh Kosong
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email" required>
                <div class="invalid-feedback">
                  Penghasilan Tidak Boleh Kosong
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="pilihbank">Metode Pembayaran</label>
                <select class="custom-select d-block w-100" id="pilihbank" required>
                  <option value="">Choose...</option>
                  <optgroup label="Transfer Bank">
                    <option value="1">Bank BRI</option>
                    <option value="2">BNI</option>
                    <option value="3">Mandiri</option>
                    <option value="4">Bank Mega</option>
                    <option value="5">Danamon</option>
                    <option value="6">BCA</option>
                    <option value="7">BTN</option>
                  </optgroup>
                </select>
                <div class="invalid-feedback">
                  Silahkan Pilih Bulan Terlebih Dahulu
                </div>
              </div>
            </div>
            <a href="verifikasibayar.php"><button class="btn btn-primary btn-lg btn-block" type="submit">Selanjutnya</button></a>
          </form>
        </div>
    </main>
</body>
</html>