<!DOCTYPE html>
<html>
<head>
    <title>Zakat Fitrah</title>
    <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
    <br>
    <div class="article-dual-column">
        <div class="container bgartikel">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="intro">
                        <h1 class="text-center">PANDUAN ZAKAT</h1>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 marginside">
                    <div class="toc borderside">
                        <aside>
                            <p class="borderleftorange">Tentang Zakat</p>                      
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/fitrah'; ?>"><button class="btnactiveorange">Zakat Fitrah</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>

                        </aside>
                        <aside>
                                <div class="col-md-4" >
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="text">
                        <div class="text-justify">
                            <center><h3>Zakat Fitrah</h3></center>
                            <hr>
                            <p>Zakat fitrah adalah zakat yang wajib ditunaikan bagi seorang muslim/ah yang sudah mampu untuk menunaikannya. Zakat fitrah harus dikeluarkan setahun sekali pada saat awal bulan Ramadhan hingga batas sebelum sholat hari raya Idul Fitri. Hal tersebut yang menjadi pembeda zakat fitrah dengan zakat lainnya.</p>

                            <p>Zakat fitrah ditunaikan dalam bentuk beras atau makanan pokok seberat 2,5 kg atau 3,5 liter per jiwa. Kualitas beras atau makanan pokok harus sesuai dengan kualitas beras atau makanan pokok yang dikonsumsi kita sehari-hari. Namun, beras atau makanan pokok tersebut dapat diganti dalam bentuk uang senilai 2,5 kg atau 3,5 liter beras.</p>
                            <p>Zakat fitrah akan menyucikan harta dan menyempurnakan puasa, karena dalam setiap harta manusia ada sebagian hak orang lain. Oleh karenanya, tidak ada suatu alasan pun bagi seorang hamba Allah yang beriman untuk tidak menunaikan zakat fitrah karena telah diwajibkan bagi setiap muslim, laki-laki maupun perempuan.</p>

                            <a href="http://baznas.go.id/zakatfitrah">Source :BAZNAS</a>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</body>
</html>