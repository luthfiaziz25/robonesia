<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
</head>
<?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>

<body>
    <!--Crausel-->
    <div class="carousel slide" data-ride="carousel" id="carousel-1 margin">
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active"><img class="slide" src="<?php echo base_url()?>assets/img/banner1.jpg" alt="Slide Image"></div>
            <div class="carousel-item"><img class="slide" src="<?php echo base_url()?>assets/img/banner2.jpg" alt="Slide Image"></div>
            <div class="carousel-item"><img class="slide" src="<?php echo base_url()?>assets/img/banner3.png" alt="Slide Image"></div>
        </div>
        <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
        <ol
        class="carousel-indicators">
        <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-1" data-slide-to="1"></li>
        <li data-target="#carousel-1" data-slide-to="2"></li>
    </ol>
</div>
<div id="map" style="width: 100%; height: 500px;"></div> 
<script type="text/javascript">
  
//              menentukan koordinat titik tengah peta
var myLatlng = new google.maps.LatLng(-6.9271274,107.7245824);

//              pengaturan zoom dan titik tengah peta
var myOptions = {
  zoom: 13,
  center: myLatlng
};

//              menampilkan output pada element
var map = new google.maps.Map(document.getElementById("map"), myOptions);

//              menambahkan marker
var marker = new google.maps.Marker({
 position: myLatlng,
 map: map,
 title:"UIN SUNAN GUNUNG DJATI"
});
</script> 
<hr>
<div class="article-list">
    <div class="container">
        <div class="intro">
            <h2 class="text-center">Berita Terbaru</h2>
        </div>
        <div class="row articles">
            <div class="col-sm-6 col-md-4 item"><a href="<?php echo base_url().'loged/detailberita'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/desk.jpg"></a>
                <h3 class="name"><a class="judulartikel judulpostkecil" href="<?php echo base_url().'loged/detailberita'; ?>">Judul Berita</a></h3>
                <p class="description">Paragraf ini berisi tentang isi berita yang akan dipulikasikan oleh admin web, yang dapat memberikan informasi uptudate kepada pengunjung web</p></div>
                <div
                class="col-sm-6 col-md-4 item"><a href="<?php echo base_url().'loged/detailberita'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/building.jpg"></a>
                <h3 class="name"><a class="judulartikel judulpostkecil" href="<?php echo base_url().'loged/detailberita'; ?>">Judul Berita</a></h3>
                <p class="description">Paragraf ini berisi tentang isi berita yang akan dipulikasikan oleh admin web, yang dapat memberikan informasi uptudate kepada pengunjung web.</p></div>
                <div
                class="col-sm-6 col-md-4 item"><a href="<?php echo base_url().'loged/detailberita'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/loft.jpg"></a>
                <h3 class="name"><a class="judulartikel judulpostkecil" href="<?php echo base_url().'loged/detailberita'; ?>">Judul Berita</a></h3>
                <p class="description">Paragraf ini berisi tentang isi berita yang akan dipulikasikan oleh admin web, yang dapat memberikan informasi uptudate kepada pengunjung web</p></div>
            </div>
        </div>
        <div class="container">
            <p class="more"><a class="more" href="<?php echo base_url().'loged/moreberita'; ?>"><strong>Tampilkan Lebih Banyak</strong></a></p>
        </div>
    </div>
    <hr>
    <div class="projects-horizontal">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Bentuk Pengabdian</h2>
                <p class="text-center"></p>
            </div>
            <div class="row projects">
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel1.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Santunan Anak Yatim</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel2.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Peduli Kesehatan</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel3.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Lomba Anak Berbakat</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a href="<?php echo base_url().'loged/detailpengabdian'; ?>"><img class="img-fluid imghoverpost" src="<?php echo base_url()?>assets/img/artikel4.jpg"></a></div>
                        <div class="col">
                            <h3 class="name"><a class="judulpostkecil" href="<?php echo base_url().'loged/detailpengabdian'; ?>">Pembangunan Mesjid</a></h3>
                            <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <p class="more"><a class="more" href="<?php echo base_url().'loged/morepengabdian'; ?>"><strong>Tampilkan Lebih Banyak</strong></a></p>
            </div>
        </div>
    </div>
    <div class="team-boxed">
        <div class="container">
            <div class="intro"></div>
            <h2 class="text-center">Struktur Lembaga</h2>
            <div class="row people">
                <div class="col-md-6 col-lg-4 item">
                    <div class="box"><img class="rounded-circle" src="<?php echo base_url()?>assets/img/user.jpg">
                        <h3 class="nama">H. ARIF RAHMAN,S.Ag. M.Pd.</h3>
                        <p class="title">Penasehat</p>
                        <p class="description"></p>
                        <div class="social">
                            <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="fa fa-facebook-official"></i></a>
                            <a href="http://www.twitter.com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="http://www.instagram.com" rel="nofollow" target="_blank"><i class="fa fa-instagram"></i></a></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 item">
                        <div class="box"><img class="rounded-circle" src="<?php echo base_url()?>assets/img/user.jpg">
                            <h3 class="nama">MUKTI AHMAD RAHARJA</h3>
                            <p class="title">Ketua</p>
                            <p class="description"></p>
                            <div class="social">
                                <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="fa fa-facebook-official"></i></a>
                                <a href="http://www.twitter.com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="http://www.instagram.com" rel="nofollow" target="_blank"><i class="fa fa-instagram"></i></a></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 item">
                            <div class="box"><img class="rounded-circle" src="<?php echo base_url()?>assets/img/user.jpg">
                                <h3 class="nama">H. ASEP IWAN SETIAWAN, S.Sos. M.Ag.</h3>
                                <p class="title">Pembina</p>
                                <p class="description"></p>
                                <div class="social">
                                    <a href="http://www.facebook.com" rel="nofollow" target="_blank"><i class="fa fa-facebook-official"></i></a>
                                    <a href="http://www.twitter.com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="http://www.instagram.com" rel="nofollow" target="_blank"><i class="fa fa-instagram"></i></a></div>
                                </div>
                            </div>
                            <div class="container">
                                <p class="morestruktur"><strong><a class="morestruktur" href="<?php echo base_url().'loged/struktur'; ?>">Struktur Lembaga</a></strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
            </html>