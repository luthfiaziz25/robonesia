<!DOCTYPE html>
<html>
<head>
    <title>Zakat Maal</title>
    <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
    <br>
    <div class="article-dual-column">
        <div class="container bgartikel">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="intro">
                        <h1 class="text-center">PANDUAN ZAKAT</h1>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 marginside">
                    <div class="toc borderside">
                        <aside>
                            <p class="borderleftorange">Tentang Zakat</p>                      
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/maal'; ?>"><button class="btnactiveorange">Zakat Maal</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>

                        </aside>
                        <aside>
                                <div class="col-md-4" >
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="text">
                        <div class="text-justify">
                            <center><h3>Harta Sebagai Obyek Zakat</h3></center>
                            <hr>
                            <p>Secara umum dan global, Alquran menyatakan bahwa zakat diambil dari setiap harta yang kita miliki, seperti dikemukakan dalam QS At-Taubah: 103 dan juga diambil dari setiap hasil usaha yang baik dan halal, seperti tergambar dalam QS Al-Baqarah: 267. Ketika menafsirkan ayat tersebut (QS At-Taubah: 103), Imam Al-Qurthubi (wafat 671 H) mengemukakan bahwa zakat diambil dari semua harta yang dimiliki, meskipun kemudian sunnah Nabi mengemukakan rincian harta yang wajib dikeluarkan zakatnya. Hal yang sama dikemukakan oleh Imam ath-Thabari (wafat 310 H) dalam kitab Jaami‟ al-Bayaan fi Ta‟wil Alquran. Sedangkan Ahmad Mustafa al-Maraghi (wafat 1495 H) ketika menjelaskan firman Allah SWT QS Al-Baqarah: 267 menyatakan bahwa ayat ini merupakan perintah Allah SWT kepada orang-orang yang beriman untuk mengeluarkan zakat (infak) dari hasil usaha yang terbaik, baik yang berupa mata uang, barang dagangan, hewan ternak, maupun yang berbentuk tanaman, buah-buahan dan biji-bijian. Sejalan dengan itu, Muhammad Sulaiman Abdullah Asqar menyatakan bahwa berzakat dan berinfak itu harus dari harta yang baik, terpilih dan halal.</p>

                            <p>Yusuf al-Qaradhawi menyatakan bahwa yang dimaksud dengan harta (al-amwaal) merupakan bentuk jamak dari kata maal. Dan maal, bagi orang Arab, yang dengan bahasanya Alquran diturunkan, adalah segala sesuatu yang diinginkan sekali oleh manusia untuk menyimpan dan memilikinya. Ibnu Asyr, sebagaimana dikutip Yusuf al-Qaradhawi, mengemukakan bahwa harta itu pada mulanya berarti emas dan perak, tetapi kemudian berubah pengertiannya menjadi segala barang yang disimpan dan dimiliki. Ulama lain, sebagaimana dikutip Zarqa dalam Fiqh Islam, menyatakan bahwa harta itu adalah segala yang diinginkan oleh manusia dan dimungkinkan menyimpannya sampai waktu yang dibutuhkan. Sebagian ulama lain menambahkan pengertian dengan menyatakan bahwa harta itu di samping diinginkan oleh manusia, juga dimungkinkan diperjualbelikan atau dimanfaatkan. Terhadap pengertian harta sebagaimana tersebut di atas, Zarqa dalam Fiqh Islam memberikan kritikannya.</p>
                            <p>Pertama, bahwa keinginan dan tabiat manusia itu berbeda-beda, bahkan kadangkala bertentangan antara yang satu dengan yang lainnya. Karena itu, pengertian demikian tidak mungkin dapat dijadikan landasan dan ukuran dalam membedakan harta dengan yang lainnya. Dan apabila dinyatakan bahwa kecenderungan dan keinginan itu bersifat luas dan umum, maka tentu tidak dapat dijadikan landasan pula, karena tidak ada batasannya yang jelas.</p>
                            <p>Kedua, bahwa dari sebagian jenis harta itu, terdapat harta yang tidak mungkin menyimpannya seperti sayur-mayur, padahal ia adalah harta yang sangat penting yang dibutuhkan manusia dalam kehidupannya. Demikian pula terdapat sebagian harta yang tidak diinginkan oleh tabiat manusia tetapi menyembuhkannya, seperti obat-obatan yang pahit. Hal-hal tersebut adalah harta yang bernilai yang tidak tercakup oleh pengertian harta sebagaimana tersebut di atas.</p>
                            <p>Ketiga, terdapat sebagian harta yang tidak ada kepemilikannya sebelum didapatkannya, termasuk pula pada harta, karena sifatnya yang masih bebas, seperti ikan di laut. Keempat, buah-buahan yang dapat dimakan tetapi belum matang, biasanya tidak diinginkan oleh tabiat manusia, dan tidak pula dapat disimpan sampai waktu yang dibutuhkannya, tetapi buah-buahan yang semacam ini tetap merupakan harta yang dapat diperjualbelikan.</p>
                            <p>Sejalan dengan hal-hal tersebut, Zarqa mengemukakan suatu definisi yang memungkinkan tercakupnya berbagai harta, sesuai dengan perkembangan keadaan dan zaman. Ia menyatakan bahwa harta itu adalah segala sesuatu yang konkret bersifat material yang mempunyai nilai dalam pandangan manusia.</p>
                            <h3>Syarat Harta Sebagai Obyek Zakat</h3>
                            <hr>
                            <p>
                                Pertama, harta tersebut harus didapatkan dengan cara yang baik dan halal. Artinya harta yang haram, baik substansi benda maupun cara mendapatkannya, jelas tidak dapat dikenakan kewajiban zakat, karena Allah SWT tidak akan menerimanya.
                            </p>
                            <p>
                                Kedua, harta tersebut berkembang atau berpotensi untuk dikembangkan, seperti melalui kegiatan usaha, perdagangan, melalui pembelian saham, atau ditabungkan, baik yang dilakukan sendiri maupun bersama orang atau pihak lain. Harta yang tidak berkembang atau tidak berpotensi untuk berkembang, maka tidak dikenakan kewajiban zakat.
                            </p>
                            <p>
                                Ketiga, milik penuh, yaitu harta tersebut berada di bawah kontrol dan dan di dalam kekuasaan pemiliknya, atau seperti menurut sebagian ulama bahwa harta itu berada di tangan pemiliknya, di dalamnya tidak tersangkut dengan hak orang lain, dan ia dapat menikmatinya.
                            </p>
                            <p>
                                Keempat, harta tersebut, menurut pendapat jumhur ulama, harus mencapai nishab, yaitu jumlah minimal yang menyebabkan harta terkena kewajiban zakat.
                            </p>
                            <p>
                                Kelima, sumber-sumber zakat tertentu, seperti perdagangan, peternakan, emas dan perak, harus sudah berada atau dimiliki ataupun diusahakan oleh muzaki dalam tenggang waktu satu tahun.
                            </p>
                            <p>
                                Keenam, sebagian ulama mazhab Hanafi mensyaratkan kewajiban zakat setelah terpenuhi kebutuhan pokok, atau dengan kata lain, zakat dikeluarkan setelah terdapat kelebihan dari kebutuhan hidup sehari-hari yang terdiri atas kebutuhan sandang, pangan, dan papan. Mereka berpendapat bahwa yang dimaksud dengan kebutuhan pokok adalah kebutuhan yang jika tidak terpenuhi, akan mengakibatkan kerusakan dan kesengsaraan dalam hidup.
                            </p>
                            <h3>Zakat Maal</h3>
                            <div class="col-md-8">
                              <table class="table">
                                <tbody>
                                    <tr>
                                        <td>a.</td>
                                        <td>Hewan Ternak</td>
                                    </tr>
                                    <tr>
                                        <td>b.</td>
                                        <td>Emas dan Perak</td>
                                    </tr>
                                    <tr>
                                        <td>c.</td>
                                        <td>Perdagangan</td>
                                    </tr>
                                    <tr>
                                        <td>d.</td>
                                        <td>Hasil Pertanian (Tanaman dan Buah-buahan)</td>
                                    </tr>
                                    <tr>
                                        <td>e.</td>
                                        <td>Barang Temuan</td>
                                    </tr>
                                    <tr>
                                        <td>f.</td>
                                        <td>Barang Tambang</td>
                                    </tr>
                                    <tr>
                                        <td>g.</td>
                                        <td>Zakat Penghasilan dari Profesi</td>
                                    </tr>
                                    <tr>
                                        <td>h.</td>
                                        <td>Zakat Perusahaan</td>
                                    </tr>
                                    <td>i.</td>
                                    <td>Zakat Surat-Surat Berharga</td>
                                </tr>
                                <tr>
                                    <td>j.</td>
                                    <td>Zakat Perdagangan Mata Uang</td>
                                </tr>
                                <tr>
                                    <td>k.</td>
                                    <td>Zakat Hewan Ternak yang Diperdagangkan</td>
                                </tr>
                                <tr>
                                    <td>l.</td>
                                    <td>Zakat Madu dan Produk Hewani</td>
                                </tr>
                                <tr>
                                    <td>m.</td>
                                    <td>Zakat Investasi Properti</td>
                                </tr>
                                <tr>
                                    <td>n.</td>
                                    <td>Zakat Asuransi Syariah</td>
                                </tr>
                                <tr>
                                    <td>o.</td>
                                    <td>Dan lain-lain</td>
                                </tr>
                                </tbody>
                            </table>
                            <a href="http://baznas.go.id/zakatmaal">Source :BAZNAS</a>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</body>
</html>