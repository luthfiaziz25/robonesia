<div class="projects-horizontal">
    <div class="container">
        <div class="intro">
            <h2 class="text-center">Kumpulan Berita</h2>
            <p class="text-center"></p>
        </div>
        <div class="row projects">
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-12 col-lg-5"><a href="<?php echo base_url()?>artikel"><img class="img-fluid" src="<?php echo base_url()?>assets/img/artikel1.jpg"></a></div>
                    <div class="col">
                        <h3 class="name"><a href="<?php echo base_url()?>artikel">Santunan Anak Yatim</a></h3>
                        <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-12 col-lg-5"><a href="<?php echo base_url()?>artikel"><img class="img-fluid" src="<?php echo base_url()?>assets/img/artikel2.jpg"></a></div>
                    <div class="col">
                        <h3 class="name"><a href="<?php echo base_url()?>artikel">Peduli Kesehatan</a></h3>
                        <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-12 col-lg-5"><a href="<?php echo base_url()?>artikel"><img class="img-fluid" src="<?php echo base_url()?>assets/img/artikel3.jpg"></a></div>
                    <div class="col">
                        <h3 class="name"><a href="<?php echo base_url()?>artikel">Lomba Anak Berbakat</a></h3>
                        <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-12 col-lg-5"><a href="<?php echo base_url()?>artikel"><img class="img-fluid" src="<?php echo base_url()?>assets/img/artikel4.jpg"></a></div>
                    <div class="col">
                        <h3 class="name"><a href="<?php echo base_url()?>artikel">Pembangunan Mesjid</a></h3>
                        <p class="description">Ini merupakan realisasi dari program fidkom zakat, untuk menumbuhkan kesadaran bagi semua orang yang memiliki hak untuk mengeluarkan zakat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>