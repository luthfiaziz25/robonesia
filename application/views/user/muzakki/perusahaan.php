<!DOCTYPE html>
<html>
<head>
    <title>Zakat Perusahaan</title>
    <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
    <br>
    <div class="article-dual-column">
        <div class="container bgartikel">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="intro">
                        <h1 class="text-center">PANDUAN ZAKAT</h1>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 marginside">
                    <div class="toc borderside">
                        <aside>
                            <p class="borderleftorange">Tentang Zakat</p>                      
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/perusahaan'; ?>"><button class="btnactiveorange">Zakat Perusahaan</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>

                        </aside>
                        <aside>
                                <div class="col-md-4" >
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="text">
                        <div class="text-justify">
                            <center><h3>Zakat Perusahaan</h3></center>
                            <hr>
                            <p>Sebagaimana dimaklumi, pada saat ini hampir sebagian besar perusahaan dikelola tidak secara individual, melainkan secara bersama-sama dalam sebuah kelembagaan dan organisasi dengan manajemen yang modern, misalnya dalam bentuk PT, CV atau koperasi.</p>

                            <p>Para ahli ekonomi menyatakan bahwa saat ini komoditas-komoditas yang dikelola perusahaan tidak terbatas hanya pada komoditas-komoditas tertentu yang sifatnya konvensional yang dilakukan dalam skala, wilayah dan level yang sempit. Bisnis yang dikelola perusahaan telah merambah berbagai bidang kehidupan, dalam skala dan wilayah yang sangat luas, bahkan antar negara dalam bentuk ekspor-impor.</p>

                            <p>Paling tidak, menurut mereka, perusahaan itu pada umumnya mencakup tiga hal yang besar. Pertama, perusahaan yang menghasilkan produk-produk tertentu. Jika dikaitkan dengan kewajiban zakat, maka produk yang dihasilkannya harus halal dan dimiliki oleh orang-orang yang beragama Islam, atau jika pemiliknya bermacam-macam agamanya, maka berdasarkan kepemilikan saham dari yang beragama Islam. Sebagai contoh, perusahaan yang memproduksi sandang dan pangan, alat-alat kosmetika dan obat-obatan, berbagai macam kendaraan dan berbagai suku cadangnya, alat-alat rumah tangga, bahan bangunan dan lain sebagainya. Kedua, perusahaan yang bergerak di bidang jasa, seperti perusahaan di bidang akuntansi, dan lain sebagainya. Ketiga, perusahaan yang bergerak di bidang keuangan, seperti lembaga keuangan, baik bank maupun non-bank (asuransi, reksadana, money changer, dan yang lainnya).</p>

                            <p>
                                Muktamar Internasional Pertama tentang Zakat di Kuwait (29 Rajab 1404 H) menyatakan bahwa kewajiban zakat sangat terkait dengan perusahaan, dengan catatan antara lain adanya kesepakatan sebelumnya antara para pemegang saham, agar terjadi keridhaan dan keikhlasan ketika mengeluarkannya. Kesepakatan tersebut seyogianya dituangkan dalam aturan perusahaan, sehingga sifatnya menjadi mengikat. Perusahaan, menurut hasil muktamar ter-sebut termasuk ke dalam syakhsan i'tibaran (badan hukum yang dianggap orang) atau syakshiyyah hukmiyyah menurut Mustafa Ahmad Zarqa. Oleh karena di antara individu itu kemudian timbul transaksi, meminjam, menjual, berhubungan dengan pihak luar, dan juga menjalin kerja sama. Segala kewajiban dan hasil akhirnya pun dinikmati secara bersama, termasuk di dalamnya kewajiban kepada Allah SWT dalam bentuk zakat. Tetapi di luar zakat perusahaan, tiap individu juga wajib mengeluarkan zakat setiap bulan (setiap menerima gaji), sesuai dengan penghasilan dan juga nishabnya.
                            </p>

                            <p>
                                Dalam kaitan dengan kewajiban zakat perusahaan ini, dalam Undang-Undang No. 23 Tahun 2011 tentang Pengelolaan Zakat, Bab I Pasal 4 ayat (2) bagian c dan ayat (3) dikemukakan bahwa di antara obyek zakat yang wajib dikeluarkan zakatnya adalah perniagaan/perdagangan dan badan usaha/perusahaan.
                            </p>
                            <h3>Nishab, Waktu, Kadar, dan Cara Mengeluarkan Zakat Perusahaan</h3>
                            <hr>
                            <p>
                                Para ulama peserta Muktamar Intemasional Pertama tentang Zakat, menganalogikan zakat perusahaan ini kepada zakat perdagangan, karena dipandang dari aspek legal dan ekonomi kegiatan sebuah perusahaan intinya berpijak pada kegiatan trading atau perdagangan. Oleh karena itu, secara umum pola pembayaran dan penghitungan zakat perusahaan adalah sama dengan zakat perdagangan. Demikian pula nishabnya adalah senilai 85 gram emas, sama dengan nishab zakat perdagangan dan sama dengan nishab zakat emas dan perak. Hal ini sejalan dengan sebuah hadis riwayat Abu Daud dari Ali bin Abi Thalib. Dan menurut pendapat yang paling mu'tabar (akurat), 20 misqal itu sama dengan 85 gram emas.
                            </p>
                            <p>
                                Sebuah perusahaan biasanya memiliki harta yang tidak akan terlepas dari tiga bentuk: Pertama, harta dalam bentuk barang, baik yang berupa sarana dan prasarana, maupun yang merupakan komoditas perdagangan.
                            </p>
                            <p>
                                Kedua, harta dalam bentuk uang tunai, yang biasanya disimpan di bank-bank.
                            </p>
                            <p>
                                Ketiga, harta dalam bentuk piutang.
                            </p>
                            <p>
                                Maka yang dimaksud dengan harta perusahaan yang harus dizakati adalah ketiga bentuk harta tersebut, dikurangi harta dalam bentuk sarana dan prasarana dan kewajiban mendesak lainnya, seperti utang yang jatuh tempo atau yang harus dibayar saat itu juga. Abu Ubaid (wafat tahun 224 H) di dalam Al-Amwaal menyatakan bahwa "Apabila engkau telah sampai batas waktu membayar zakat (yaitu usaha engkau telah berlangsung selama satu tahun, misalnya usaha dimulai pada bulan Zulhijjah 1421 H dan telah sampai pada Zulhijjah 1422 H), perhatikanlah apa yang engkau miliki, baik berupa uang (kas) ataupun barang yang siap diperdagangkan (persediaan), kemudian nilailah dengan nilai uang, dan hitunglah utang-utang engkau atas apa yang engkau miliki".
                            </p>
                            <p>
                                Dari penjelasan di atas, maka dapatlah diketahui bahwa pola perhitungan zakat perusahaan, didasarkan pada laporan keuangan (neraca) dengan mengurangkan kewajiban atas aktiva lancar. Atau seluruh harta (di luar sarana dan prasarana) ditambah keuntungan, dikurangi pembayaran utang dan kewajiban lainnya, lalu dikeluarkan 2,5 persen sebagai zakatnya. Sementara pendapat lain menyatakan bahwa yang wajib dikeluarkan zakatnya itu hanyalah keuntungannya saja.
                            </p>
                            <a href="http://baznas.go.id/zakatperusahaan">Source :BAZNAS</a>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</body>
</html>