<!DOCTYPE html>
<html>
<head>
    <title>Zakat Profesi</title>
    <?php $this->load->view("user/_partials/head.php")?>
  <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
    <br>
    <div class="article-dual-column">
        <div class="container bgartikel">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="intro">
                        <h1 class="text-center">PANDUAN ZAKAT</h1>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 marginside">
                    <div class="toc borderside">
                        <aside>
                            <p class="borderleftorange">Tentang Zakat</p>                      
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/profesi'; ?>"><button class="btnactiveorange">Zakat Profesi</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>

                        </aside>
                        <aside>
                                <div class="col-md-4" >
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="text">
                        <div class="text-justify">
                            <center><h3>Zakat Profesi</h3></center>
                            <hr>
                            <p>Yusuf al-Qaradhawi menyatakan bahwa di antara hal yang sangat penting untuk mendapatkan perhatian kaum muslimin saat ini adalah penghasilan atau pendapatan yang diusahakan melalui keahliannya, baik keahlian yang dilakukannya secara sendiri maupun secara bersama-sama. Yang dilakukan sendiri, misalnya profesi dokter, arsitek, ahli hukum, penjahit, pelukis, mungkin juga da‘i atau muballigh, dan lain sebagainya. Yang dilakukan bersama-sama, misalnya pegawai (pemerintah maupun swasta) dengan menggunakan sistem upah atau gaji.</p>

                            <p>Wahbah az-Zuhaili secara khusus mengemukakan kegiatan penghasilan atau pendapatan yang diterima seseorang melalui usaha sendiri (wirausaha) seperti dokter, insinyur, ahli hukum, penjahit dan lain sebagainya. Dan juga yang terkait dengan pemerintah (pegawai negeri) atau pegawai swasta yang mendapatkan gaji atau upah dalam waktu yang relatif tetap, misalnya sebulan sekali. Penghasilan atau pendapatan yang semacam ini dalam istilah fiqh dikatakan sebagai al-maal al-mustafaad. Sementara itu, fatwa ulama yang dihasilkan pada waktu Muktamar Internasional Pertama tentang Zakat di Kuwait pada tanggal 29 Rajab 1404 H yang bertepatan dengan tanggal 30 April 1984 M, bahwa salah satu kegiatan yang menghasilkan kekuatan bagi manusia sekarang adalah kegiatan profesi yang menghasilkan amal yang bermanfaat, baik yang dilakukan sendiri, seperti dokter, arsitek dan yang lainnya, maupun yang dilakukan secara bersama-sama, seperti para karyawan atau para pegawai. Semua itu menghasilkan pendapatan atau gaji.</p>

                            <h3>Landasan Hukum Kewajiban Zakat Profesi</h3>
                            <hr>
                            <p>
                                Semua penghasilan melalui kegiatan profesional tersebut, apabila telah mencapai nishab, maka wajib dikeluarkan zakatnya. Hal ini berdasarkan nash-nash yang bersifat umum, misalnya firman Allah SWT dalam QS At-Taubah: 103, QS Al-Baqarah: 267 dan juga firman-Nya dalam QS Adz-Dzaariyaat: 19
                            </p>
                            <p>
                                “Ambillah zakat dari sebagian harta mereka, dengan zakat itu kamu membersihkan dan mensucikan mereka, dan mendo`alah untuk mereka. Sesungguhnya do`a kamu itu (menjadi) ketenteraman jiwa bagi mereka. Dan Allah Maha Mendengar lagi Maha Mengetahui.” (QS At-Taubah: 103).
                            </p>
                            <p>
                                “Hai orang-orang yang beriman, nafkahkanlah (di jalan Allah) sebagian dari hasil usahamu yang baik-baik dan sebagian dari apa yang Kami keluarkan dari bumi untuk kamu. Dan janganlah kamu memilih yang buruk-buruk lalu kamu nafkahkan darinya, padahal kamu sendiri tidak mau mengambilnya melainkan dengan memicingkan mata terhadapnya. Dan ketahuilah, bahwa Allah Maha Kaya lagi Maha Terpuji.” (QS Al-Baqarah: 267).
                            </p>
                            <p>
                                “Dan pada harta-harta mereka ada hak untuk orang miskin yang meminta dan orang miskin yang tidak mendapat bagian” (QS Adz-Dzaariyaat: 19).
                            </p>
                            <p>
                                Sayyid Quthub (wafat 1965 M) dalam tafsirnya, Fi Zhilalil-Quran ketika menafsirkan firman Allah SWT dalam QS Al-Baqarah: 267 menyatakan bahwa nash ini mencakup seluruh hasil usaha manusia yang baik dan halal dan mencakup pula seluruh yang dikeluarkan Allah SWT dari dalam dan atas bumi, seperti hasil-hasil pertanian, maupun hasil pertambangan seperti minyak. Karena itu, nash ini mencakup semua harta, baik yang terdapat di zaman Rasulullah SAW, maupun di zaman sesudahnya. Semuanya wajib dikeluarkan zakatnya dengan ketentuan dan kadar sebagaimana diterangkan dalam Sunnah Rasulullah SAW, baik yang sudah diketahui secara langsung, maupun yang di-qiyas-kan kepadanya. Al-Qurthubi (wafat 671 H), dalam Tafsir al-Jaami‟ li Ahkaam Alquran, menyatakan bahwa yang dimaksud dengan kata-kata hakkun ma‟lum (hak yang pasti) pada QS Adz-Dzaariyaat: 19 adalah zakat yang diwajibkan, artinya semua harta yang dimiliki dan semua penghasilan yang didapatkan, jika telah memenuhi persyaratan kewajiban zakat, maka harus dikeuarkan zakatnya.
                            </p>
                            <p>
                                Sementara itu, para peserta Muktamar Internasional Pertama tentang Zakat di Kuwait (pada tanggal 29 Rajab 1404 H yang bertepatan dengan tanggal 30 April 1984 M) telah sepakat tentang wajibnya zakat profesi apabila telah mencapai nishab, meskipun mereka berbeda pendapat dalam hal cara mengeluarkannya. Dalam Pasal 4 ayat (2) Bab I Undang-undang No. 23/2011 tentang Pengelolaan Zakat dikemukakan bahwa harta yang dikenai zakat adalah: a. emas, perak, dan logam mulia lainnya; b. uang dan surat berharga lainnya; c. perniagaan; d. pertanian, perkebunan, dan kehutanan; e. peternakan dan perikanan; f. pertambangan; g. perindustrian; h. pendapatan dan jasa; dan i. rikaz.
                            </p>
                            <p>
                                Berdasarkan uraian di atas, dapat disimpulkan bahwa setiap keahlian dan pekerjaan apapun yang halal, baik yang dilakukan sendiri maupun yang terkait dengan pihak lain, seperti seorang pegawai atau karyawan, apabila penghasilan dan pendapatannya mencapai nishab, maka wajib dikeluarkan zakatnya.
                            </p>
                            <h3>Nishab, Waktu, Kadar, dan Cara Zakat Profesi</h3>
                            <hr>
                            <p>
                                Terdapat beberapa kemungkinan kesimpulan dalam menentukan nishab, kadar dan waktu mengeluarkan zakat profesi. Hal ini sangat bergantung pada qiyas (analogi) yang dilakukannya.
                            </p>
                            <p>
                                Pertama, jika dianalogikan pada zakat perdagangan, maka nishab, kadar, dan waktu mengeluarkannya sama dengannya dan sama pula dengan zakat emas dan perak. Nishabnya senilai 85 gram emas, kadar zakatnya 2,5 persen dan waktu mengeluarkannya setahun sekali.
                            </p>
                            <p>
                                Contoh: Jika si A berpenghasilan Rp 10.000.000 setiap bulan maka besar zakat yang dikeluarkannya adalah: 2,5 % x 12 x Rp 10.000.000 sebesar Rp 3.000.000 per tahun atau Rp 250.000 per bulan.
                            </p>
                            <p>
                                Kedua, jika dianalogikan pada zakat pertanian, maka nishabnya senilai 653 kg padi atau gandum, kadar zakatnya sebesar lima persen dan dikeluarkan pada setiap mendapatkan gaji atau penghasilan, misalnya sebulan sekali. Dalam contoh kasus di atas, maka kewajiban zakat si A adalah sebesar 5% x Rp 10.000.000 atau sebesar Rp 500.000 per bulan.
                            </p>
                            <p>
                                Ketiga, jika dianalogikan pada zakat rikaz, maka zakatnya sebesar 20 persen tanpa ada nishab, dan dikeluarkan pada saat menerimanya (Pada contoh di atas, maka si A mempunyai kewajiban berzakat sebesar 20 % x Rp 10.000.000 atau sebesar Rp 2.000.000 setiap bulan.)
                            </p>
                            <p>
                                Zakat profesi bisa dianalogikan pada dua hal secara sekaligus, yaitu pada zakat pertanian dan pada zakat emas dan perak. Dari sudut nishab dianalogikan pada zakat pertanian, yaitu sebesar lima ausaq atau senilai 653 kg padi / gandum dan dikeluarkan pada saat menerimanya. Misalnya setiap bulan bagi karyawan yang menerima gaji bulanan langsung dikeluarkan zakatnya, sama seperti zakat pertanian yang dikeluarkan pada saat panen, sebagaimana digambarkan Allah SWT dalam QS Al-An‘aam: 141.
                            </p>
                            <p>
                                Karena dianalogikan pada pada zakat pertanian, maka bagi zakat profesi tidak ada ketentuan haul. Ketentuan waktu menyalurkannya adalah pada saat menerima, misalnya setiap bulan, dapat didasarkan pada 'urf (tradisi) di sebuah negara. Karena itu profesi yang menghasilkan pendapatan setiap hari, misalnya dokter yang membuka praktek sendiri, atau para da'i yang setiap hari berceramah, zakatnya dapat dikeluarkan setiap hari atau sebulan sekali.
                            </p>
                            <p>
                                Penganalogian zakat profesi dengan zakat pertanian dilakukan karena ada kemiripan antara keduanya (asy-syabah). Jika hasil panen pada setiap musim berdiri sendiri tidak terkait dengan hasil sebelumnya, demikian pula gaji dan upah yang diterima, tidak terkait antara penerimaan bulan kesatu dan bulan kedua dan seterusnya. Berbeda dengan perdagangan yang selalu terkait antara bulan pertama dan bulan kedua dan seterusnya sampai dengan jangka waktu satu tahun atau tahun tutup buku.
                            </p>
                            <p>
                                Dari sudut kadar zakat, dianalogikan pada zakat uang/emas/perak, karena memang gaji, honorarium, upah dan yang lainnya, pada umumnya diterima dalam bentuk uang. Karena itu kadar zakatnya adalah sebesar rub'ul usyri atau 2,5 persen.
                            </p>
                            <p>
                                Qiyas syabah, yang penulis gunakan dalam menetapkan kadar dan nishab zakat profesi pada zakat pertanian dan zakat nuqud (emas dan perak) adalah qiyas yang 'illat hukumnya ditetapkan melalui metode syabah. Contoh qiyas syabah yang dikemukakan oleh Muhammad al-Amidi adalah hamba sahaya yang dianalogikan pada dua hal yaitu pada manusia (nafsiyyah) menyerupai orang yang merdeka (al-hur) dan dianalogikan pula pada kuda karena dimiliki dan dapat diperjualbelikan di pasar.
                            </p>
                            <a href="http://baznas.go.id/zakatprofesi">Source :BAZNAS</a>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</body>
</html>