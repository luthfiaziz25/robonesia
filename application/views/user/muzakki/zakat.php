<!DOCTYPE html>
<html>
<head>
    <title>Panduan Zakat</title>
    <?php $this->load->view("user/_partials/head.php")?>
    <?php $this->load->view("user/_partials/js.php")?>
</head>
<body>
<br>
<div class="article-dual-column">
    <div class="container bgartikel">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="intro">
                    <h1 class="text-center">PANDUAN ZAKAT</h1>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="toc bordersidepanduan">
                    <aside>
                        <p class="borderleftorange">Tentang Zakat</p>                      
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
                        <li class="borderleft1"><a class="aside" href="<?php echo base_url().'loged/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>
                    </aside>
                    <aside>
                                <div class="col-md-4" >
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
                </div>
            </div>
            <div class="col-md-9">
                <div class="text">
                    <p>Panduan zakat membantu pengguna memahami tentang macam -macam zakat,tatacara untuk melaksanakan zakat, aturan - aturan tentang zakat.</p>
                    <p>Dengan adanya panduan zakat ini diharapkan pengguna paham terhadap zakat yang akan dikeluarkan agar tepat dalam melaksanakan ibadah zakat</p>
                    <p>Panduan zakat membantu pengguna memahami tentang macam -macam zakat,tatacara untuk melaksanakan zakat, aturan - aturan tentang zakat.</p>
                    <p>Dengan adanya panduan zakat ini diharapkan pengguna paham terhadap zakat yang akan dikeluarkan agar tepat dalam melaksanakan ibadah zakat</p>
                    <p>Panduan zakat membantu pengguna memahami tentang macam -macam zakat,tatacara untuk melaksanakan zakat, aturan - aturan tentang zakat.</p>
                    <p>Dengan adanya panduan zakat ini diharapkan pengguna paham terhadap zakat yang akan dikeluarkan agar tepat dalam melaksanakan ibadah zakat</p>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>