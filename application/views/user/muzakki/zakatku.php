<!DOCTYPE html>
<html>
<head>
	<title>Zakatku</title>
	<?php $this->load->view("user/_partials/head.php")?>
    <?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">

<div class="container">
	<div class="row">
		<div class="col-md-3">
			<ul class="dashleftuser">
        <br>
      <div class="cardfb">
        <li><p class="ahrefjudul"><strong>Menu</strong></p></li>
        <hr>
        <li><a class="ahref activepage" href="<?php echo base_url()?>dashboarduser">Profil Muzzaki</a></li>
        <li><a class="ahref" href="<?php echo base_url()?>setting">Setting</a></li>
        <li><a class="ahref" href="<?php echo base_url()?>zakatku">Zakatku</a></li>
        <li><a class="ahref" href="<?php echo base_url()?>feedback">Feedback</a></li>
        <br>
      </div>
    </ul>	
    <aside>
                    <div class="col-md-4" >
                  <div class="cardfansuser" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                    </aside>
		</div>
		<div class="col-md-9">
			<h3 class="titlezakatku"><strong>TABEL ZAKATKU</strong></h3>
			<hr class="hrzakatku">
<table class="table">
	<tr class="trtitle">
		<td class="tdno">No</td>
		<td class="tdjenis">Jenis Zakat</td>
		<td class="tdnominal">Rp.</td>
		<td class="tdwaktu">Waktu 3</td>
		<td class="tdstatus">Status</td>
	</tr>
	<tr>
		<td id="no">1</td>
		<td id="jenis">Zakat Profesi</td>
		<td id="nominal">10000000</td>
		<td id="waktu">18/01/2019</td>
		<td id="status">Berhasil</td>
	</tr>
	<tr>
		<td id="no">1</td>
		<td id="jenis">Zakat Maal</td>
		<td id="nominal">10000000</td>
		<td id="waktu">18/01/2019</td>
		<td id="status">Menunggu Pembayaran</td>
	</tr>
	<tr>
		<td id="no">1</td>
		<td id="jenis">Zakat Perusahaan</td>
		<td id="nominal">50000000</td>
		<td id="waktu">15/01/2019</td>
		<td id="status">Menunggu Pembayaran</td>
	</tr>
	<tr>
		<td id="no">1</td>
		<td id="jenis">Zakat Profesi</td>
		<td id="nominal">10000000</td>
		<td id="waktu">18/01/2019</td>
		<td id="status">Berhasil</td>
	</tr>
	<tr>
		<td id="no">1</td>
		<td id="jenis">Zakat Profesi</td>
		<td id="nominal">10000000</td>
		<td id="waktu">18/01/2019</td>
		<td id="status">Berhasil</td>
	</tr>
</table>
</div>
</div>
</div>

</body>
</html>