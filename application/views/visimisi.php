
<!DOCTYPE html>
<html>
<head>
  <title>Visi dan Misi</title>
  <?php $this->load->view("user/_partials/head.php")?>
<?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">
  <br>
  <div class="container bgartikel">
    <div class="article-dual-coloum">
      <div class="row">
        <div class="row alignartikel">
          
          <div class="col-md-8 col-lg-8 marginartikel">
            <div class="row">
              <div class="col-md-12 itemcenter">
                <img class="logouin" src="<?php echo base_url()?>assets/img/LogoUPZ.png">
              </div>
            </div>
            <hr>
            <h3 class="titleprofil">Profil</h3>
            <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
            <div class="text textcontent">
              <p>UPZ MD Adalah sebuah lembaga atau wadah resmi untuk mengelola mengenal mengenai Zakat, Infak dan Sedekah. UPZ Manajemen Dakwah didirikan tahun 2016 berdasarkan <strong> SK BAZNAS Nomor Un.05/III.4/PP.009/20/2016 tanggal 05 Februari 2016</strong></p>
            </div>
            <h3 class="titleprofil">Visi dan Misi</h3>
            <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
            <div class="text">
              <h5>Visi</h5>
              <p class="textcontent">Membangun kesejahteraan Umat Muslim
               Menjadi lembaga atau wadah yang amanah, jujur, Profesional dan Bertanggung Jawab
             </p>
             <h5>Misi</h5>
             <p class="textcontent">
              Membangun kesadaran umat Muslim tentang kewajiban menunaikan Zakat.
              Meningkatkan kepedulian sesama umat manusia terutama terhadap anak Yatim dan Dhuafa.
            </p>
          </div>
          <h3 class="titleprofil">Fungsi dan Peran</h3>
          <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
          <ul>
            <li>Melakukan Pengumpulan ZIS Secara maksimal</li>
            <li>Memberikan pelayanan sebaik-baiknya kepada Muzaki atau Munfik</li>
            <li>Mengelola ZIS sesuai dengan syar'i</li>
            <li>Menyampaikan laporan Keuangan secara berkala dan transparansi</li>
            <li>Menjalankan tugas selalu berkoordinasi dan komunikai dengan jurusan Manajemen Dakwah dan BAZNAS Provinsi Jawa Barat</li>
          </ul>
          <h3 class="titleprofil">Program</h3>
          <h6 class="titleupz">UPZ Manajemen Dakwah</h6>
          <ul>
            <li>Dapat Mensosialisasikan ZIS Kepada masyarakat</li>
            <li>Menghimpun ZIS Secara optimal</li>
            <li>Mendistribusikan secara selektif kepada Mustahik, Yatim dan Dhuafa</li>
            <li>Memberikan bantuan perlengkapan dan santunan kematian secara tepat dan tanggap</li>
            <li>Memberikan besasiswa bagi mahasiswa/i tidak mampu yang memiliki prestasi</li>
            <li>Melengkapi kebutuhan administrasi dan Operasional penunjang sekretariat UPZ Manajemen Dakwah</li>
            <li>Membangun citra dan kepercayaan masyarakat, Muzaki, Munfiq</li>
          </ul>
        </div>
        <div class="col-md-9 col-lg-3 marginside">
          <div class="toc borderside">
            <aside>
                  <div class="col-md-4" >
                    <br>
                  <h5>FANSPAGE</h5>
                  <div class="cardfans" style="background-color:rgba(255,255,255,0.9) ;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
                  </div>
                  </aside>
            <aside>
              <p class="borderleftblue">Profil</p>
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/visimisi'; ?>"><button class="btnactiveblue">Visi dan Misi</button></a></li>
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/struktur'; ?>"><button class="btnsideblue">Struktur Lembaga</button></a></li>
              <p class="borderleftorange">Tentang Zakat</p>                      
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/maal'; ?>"><button class="btnsideorange">Zakat Maal</button></a></li>
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/fitrah'; ?>"><button class="btnsideorange">Zakat Fitrah</button></a></li>
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/profesi'; ?>"><button class="btnsideorange">Zakat Profesi</button></a></li>
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/surat'; ?>"><button class="btnsideorange">Zakat Surat Berharga</button></a></li>
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/perusahaan'; ?>"><button class="btnsideorange">Zakat Perusahaan</button></a></li>
              <li class="borderleft1"><a class="aside" href="<?php echo base_url().'overview/asnaf'; ?>"><button class="btnsideorange">Asnaf Zakat</button></a></li>
              
              <p class="borderleftartikel">Berita Terbaru</p>
              <?php foreach ($berita->result() as $key): ?>
              <li class="borderleftgambar">
                <a class="sidenews" href=""><img class="sideimg" src="<?php echo base_url() ?>assets/img/<?php echo $key->image; ?>"></a><a class="sidetitle" href="<?php echo base_url('overview/detailberita/'. $key->id)?>"><?php echo $key->judul;?></a></li>
              <?php endforeach ?>
                    </aside>
                  </div>
                  <br>
                  <br>
                  <br>
                  <br>
                </div>
              </div>
              <!-- end sidebar -->
            </div>
          </div>
        </div>
        <br>
        <!-- end wrapper -->

      </body>
      </html>
