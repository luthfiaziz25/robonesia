<!DOCTYPE html>
<html>
<head>
	<title>Zakatku</title>
	<?php $this->load->view("user/_partials/head.php")?>
	<?php $this->load->view("user/_partials/js.php")?>
</head>
<body class="imgartikel">

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<ul class="dashleftuser">
					<br>
					<div class="cardfb">
						<li><p class="ahrefjudul"><strong>Menu</strong></p></li>
						<hr>
						<li><a class="ahref activepage" href="<?php echo base_url()?>dashboarduser/profilku">Profil Muzzaki</a></li>
						<!-- <li><a class="ahref" href="<?php echo base_url()?>dashboarduser/setting">Setting</a></li> -->
						<li><a class="ahref" href="" data-toggle="modal" data-target='#modal-setting'>Setting</a></li>
						<li><a class="ahref" href="<?php echo base_url()?>dashboarduser/tampilzakatku">Zakatku</a></li>
						<li><a class="ahref" href="<?php echo base_url()?>dashboarduser/saran">Feedback</a></li>
						<br>
					</div>
				</ul>	
				<aside>
					<div class="col-md-4" >
						<div class="cardfansuser" style="background-color:rgba(255,255,255,0.9) ;">
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2';
								fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>

							<div class="fb-page" data-href="https://www.facebook.com/mduinsgd/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mduinsgd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mduinsgd/">Jurusan Manajemen Dakwah Uin Sgd Bandung</a></blockquote></div>
						</div>
					</div>
				</aside>
			</div>
				<div class="col-md-9">
					<h3 class="titlezakatku"><strong>TABEL ZAKATKU</strong></h3>
					<hr class="hrzakatku">

					<table class="table">
						<thead>
							<tr class="trtitle">
								<td class="tdno">No</td>
								<td class="tdjenis">Jenis Zakat</td>
								<td class="tdnominal">Jumlah Zakat</td>
								<td class="tdwaktu">Waktu</td>
								<td class="tdstatus">Status</td>
								<td class="tdbukti">Bukti Bayar</td>
								<td class="tdaction">Action</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach($tampilzakatku as $value) { ?>
								<tr class="trtitle">
									<td ><?php echo $value->id_transaksi; ?></td>
									<td ><?php echo $value->jenis_zakat; ?></td>
									<td ><?php echo "Rp. ", number_format($value->jumlah_zakat);?></td>
									<td ><?php echo $value->tanggal; ?></td>
									<td ><?php echo $value->status; ?></td>
									<td><img class="imgbukti" src="<?=base_url().'assets/img/uploadbukti/'.$value->upload_bukti; ?>"></td>
									<td ><a class="btn btn-warning btn-flat" href="<?=base_url().'Donasi/v_bukti/'.$value->id_transaksi; ?>"><span class="fa fa-plus"></span> Upload Bukti</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!-- Modal Upload-->
		<!-- <div class="modal fade" id="modal-upload" tabindex="-1" role="doalog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="head-shape bg-head">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
						<h4 class="modal-title" id="myModalLabel">Upload Bukti Pembayaran</h4>
					</div>

					<form class="form-horizontal" action="<?php echo site_url('donasi/uploadbukti'); ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="modal-body">
								<div class="form-group">
									<label></label>
									<input class="form-control" type="file" name="image">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary btn-flat" id="simpan">Upload</button>
								</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div> -->
		<!-- Modal Setting -->
	<div class="modal fade" id="modal-setting" tabindex="-1" role="doalog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="head-shape bg-head">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Ganti Password</h4>
          </div>

          <form class="form-horizontal" action="<?php echo site_url('dashboarduser/gantipass'); ?>" method="POST">
          <div class="modal-body">
            <div class="modal-body">
                <div class="form-group">
                  <input class="form-control" type="password" name="password" placeholder="password lama" required>
                </div>
                <div class="form-group">
                  <input class="form-control" type="password" name="newpassword" placeholder="New Password" required>
                </div>
                <div class="form-group">
                  <input class="form-control" type="password" name="renewpassword" placeholder="Re-New Password" required>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary btn-flat" id="simpan">Save</button>
                </div>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
</body>
</html>